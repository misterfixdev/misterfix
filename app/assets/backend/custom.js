$('#example1').DataTable();

$(function(){
    $(".select2").select2({
        matcher: matchCustom,
        templateResult: formatCustom
    });
})

function stringMatch(term, candidate) {
    return candidate && candidate.toLowerCase().indexOf(term.toLowerCase()) >= 0;
}

function matchCustom(params, data) {
    // If there are no search terms, return all of the data
    if ($.trim(params.term) === '') {
        return data;
    }
    // Do not display the item if there is no 'text' property
    if (typeof data.text === 'undefined') {
        return null;
    }
    // Match text of option
    if (stringMatch(params.term, data.text)) {
        return data;
    }
    // Match attribute "data-foo" of option
    if (stringMatch(params.term, $(data.element).attr('data-foo'))) {
        return data;
    }
    // Return `null` if the term should not be displayed
    return null;
}

function formatCustom(state) {
    return $(
        '<div>'
        +'<div>' + state.text + '</div>'
        +'<div class="foo">'
        + $(state.element).attr('data-foo')
        + '</div>'
        +'</div>'
    );
}


$('#task_select').change(function (e) {
    if ($('#task_select').val() != ''){
        $('#find_btn').attr("disabled", false);
    }
});

$('#supervisor_select').change(function (e) {
    if ($('#supervisor_select').val() != ''){
        $.ajax({
            url: site_url + "admin/supervisors/single/info",
            type: 'post',
            data: {id: $('#supervisor_select').val()},
            success: function (res) {
                console.log(JSON.parse(res));
                var data = JSON.parse(res);
                var supervisor = JSON.parse(res).supervisor[0];
                var category = JSON.parse(res).category;

                const d = new Date(supervisor.joined_date)
                const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d)
                const mo = new Intl.DateTimeFormat('en', { month: 'long' }).format(d)
                const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d)
                $('#sup-name').text(supervisor.name);
                $('#sup-mobile').text(supervisor.mobile);
                $('#sup-location').text(supervisor.location);
                $('#sup-joined_date').text(da+'-'+mo+'-'+ye);
                $('#sup-status').text((supervisor.status)?'Active': 'Inactive');

                var text = '';
                category.forEach(function(item, index) {
                    text = text + '<li style="padding-left: 15px;padding-top: 5px;">'+item.category+'</li>'
                    // document.getElementById("demo").innerHTML += index + ":" + item + "<br>";
                });

                $('#cat_div').html(text);
                $('#supervisor_details').show();
                $('#assign_btn').attr("disabled", false);
            }
        });
    }else{
        $('#supervisor_details').hide();

    }
});

