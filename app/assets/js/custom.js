$('#txtDistrict').change(function (e) {
    window.location = site_url + 'service/all?district=' + $(this).val();
});

$('#txtCity').change(function (e) {
    window.location = site_url + 'service/all?district=' + $('#txtDistrict').val() + '&&city=' + $(this).val();
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result).show();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function () {
    readURL(this);
});


function markDone(id) {
    var x = confirm("Are you sure work has done?");

    if (x) {
        $.ajax({
            url: site_url + "order/completed",
            type: 'post',
            data: {id: id},
            success: function (res) {
                if (res == 1) {
                    window.location.reload();
                }
            }
        });
    }
}

function acceptTask(id) {
    var x = confirm("Are you sure?");

    if (x) {
        $.ajax({
            url: site_url + "order/accept",
            type: 'post',
            data: {id: id},
            success: function (res) {
                if (res == 1) {
                    window.location.reload();
                }
            }
        });
    }
}

function rejectTask(id) {
    var reason = prompt("Reason to reject : ", "");
    if (reason != null || reason != "") {
        $.ajax({
            url: site_url + "order/reject",
            type: 'post',
            data: {id: id, reason : reason},
            success: function (res) {
                if (res == 1) {
                    window.location.reload();
                }
            }
        });
    }
}

function request_cancel(id) {
    $('#serv_id').val(id);
}

function accept_Cancel(id) {
    var x = confirm("Are you sure?");

    if (x) {
        $.ajax({
            url: site_url + "order/cancel_accept",
            type: 'post',
            data: {id: id},
            success: function (res) {
                if (res == 1) {
                    window.location.reload();
                }
            }
        });
    }
}