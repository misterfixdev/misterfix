<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("service_provider_model");
        $this->load->model("service_model");
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->helper('url', 'form');

        $user = $this->ion_auth->get_Logged_user();
        if ($user['user_type'] == 'service_provider' && !$user['admin_verify']) {
            redirect('welcome/unverified');
        }

    }

    public function index()
    {
        if ($this->ion_auth->logged_in()) {

            $user = $this->ion_auth->get_Logged_user();
            if ($user['user_type'] != 'service_provider') {
                redirect('welcome/index');
            }

        } else {
            redirect('welcome/index');
        }

        $this->data['cities'] = $this->db->where('status', 1)->where('main_id is NOT NULL', NULL, FALSE)->get('locations')->result_array();
        $this->data['categories'] = $this->db->where('status', 1)->get('categories')->result_array();
        $this->data['service_provider'] = $this->service_provider_model->singleServiceProviderDetailsByUserID($user['id']);
        $this->data['service_provider_categories'] = $this->service_provider_model->getServiceProviderCategoryByUserID($user['id']);
//        var_dump($this->data['service_provider']);
        $this->load->view('front/service_provider_profile', $this->data);
    }

    public function save()
    {
//        var_dump($this->input->post());
//        var_dump($this->input->post('days'));
//        die();
        $errors = '';

        if ($this->input->post('days') == NULL) {
            $errors = 'Enter working Days <br>';
        }

        if ($errors != '') {
            $this->session->set_flashdata('error', $errors);
            redirect('user/index');
        }

//        Calc DOB
        $age = $this->calcAge($this->input->post('dob'));

        $district = $this->db->where('id', $this->input->post('location'))->get('locations')->result_array();

        $data = array(
            'dob' => $this->input->post('dob'),
            'age' => $age,
            'user_id' => $this->input->post('user_id'),
            'location_id' => $district[0]['main_id'],
            'city_id' => $district[0]['id'],
            'gender' => $this->input->post('gender'),
            'category_id' => $this->input->post('main_service'),
            'service_provider_id' => $this->uniqueID(6),
            'working_days' => json_encode($this->input->post('days')),
            'time_to_call_from' => $this->input->post('time_from'),
            'time_to_call_to' => $this->input->post('time_to'),
        );


//save data
        if ($this->db->where('user_id', $this->input->post('user_id'))->get('service_providers')->num_rows()) {
            $this->db->where('user_id', $this->input->post('user_id'));
            $res = $this->db->update('service_providers', $data);
        } else {
            $this->db->insert('service_providers', $data);
        }

//        save alternative services
        $this->db->where('user_id', $this->input->post('user_id'))->delete('service_provider_categories');
        if (count($this->input->post('alternative_services'))) {
            foreach ($this->input->post('alternative_services') as $serv) {
//            var_dump($serv);
                $data = array(
                    'user_id' => $this->input->post('user_id'),
                    'category_id' => $serv,
                );
                $this->db->insert('service_provider_categories', $data);
            }
        }

//        file upload
        $image_name = time() . '-' . str_replace(' ', '-', str_replace('_', '-', $_FILES["file"]['name']));
        $config['upload_path'] = './assets/docs/';
        $config['allowed_types'] = 'jpg|png|pdf|jpeg';
        $config['max_size'] = 5000;
//        $config['max_width'] = 1500;
//        $config['max_height'] = 1500;
        $config['file_name'] = $image_name;
        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file')) {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('user/index');
        }

        $this->db->where('id', $this->input->post('user_id'))->update('users', array('step' => 3, 'doc_type' => $this->input->post('doc_type'), 'file_name' => $image_name));

        $this->session->set_flashdata('msg', 'User Details added Successfully');
        redirect('user/index');

    }


    public function editCategory()
    {
        if ($this->ion_auth->logged_in()) {

            $user = $this->ion_auth->get_Logged_user();

        } else {
            redirect('welcome/index');
        }

        $this->data['cities'] = $this->db->where('status', 1)->where('main_id is NOT NULL', NULL, FALSE)->get('locations')->result_array();
        $this->data['categories'] = $this->db->where('status', 1)->get('categories')->result_array();
        $this->data['service_provider'] = $this->service_provider_model->singleServiceProviderDetailsByUserID($user['id']);
        $this->data['service_provider_categories'] = $this->service_provider_model->getServiceProviderCategoryByUserID($user['id']);
//        die();
        $this->load->view('front/edit_category', $this->data);
    }

    public function editDetails()
    {
        if ($this->ion_auth->logged_in()) {

            $user = $this->ion_auth->get_Logged_user();

        } else {
            redirect('welcome/index');
        }

        $this->data['cities'] = $this->db->where('status', 1)->where('main_id is NOT NULL', NULL, FALSE)->get('locations')->result_array();
        $this->data['categories'] = $this->db->where('status', 1)->get('categories')->result_array();
        $this->data['service_provider'] = $this->service_provider_model->singleServiceProviderDetailsByUserID($user['id']);
        $this->data['service_provider_categories'] = $this->service_provider_model->getServiceProviderCategoryByUserID($user['id']);
        $service_provider_category_ids = $this->db->where('user_id', $user['id'])->select('category_id')->get('service_provider_categories')->result_array();
//        die();
        $itemArray = array();
        foreach ($service_provider_category_ids as $item) {
            array_push($itemArray, $item['category_id']);
        }

        $this->data['service_provider_category_ids'] = $itemArray;
        $this->data['working_days'] = json_decode($this->data['service_provider'][0]['working_days']);
        $this->load->view('front/edit_details', $this->data);
    }

    public function saveDetails()
    {
        var_dump($this->input->post());
        $district = $this->db->where('id', $this->input->post('location'))->get('locations')->result_array();
//        var_dump($district);

        $data = array(
            'location_id' => $district[0]['main_id'],
            'city_id' => $district[0]['main_id'],
            'age' => $this->calcAge($this->input->post('dob')),
            'expariance' => $this->input->post('experience'),
            'working_area' => $this->input->post('working_area'),
            'dob' => $this->input->post('dob'),
            'gender' => $this->input->post('gender'),
            'description' => $this->input->post('description'),
            'time_to_call_from' => $this->input->post('time_to_call_from'),
            'time_to_call_to' => $this->input->post('time_to_call_to'),
            'working_days' => json_encode($this->input->post('days')),
        );
        $this->db->where('user_id', $this->input->post('user_id'));
        $res = $this->db->update('service_providers', $data);

        if ($res) {
            $this->db->where('user_id', $this->input->post('user_id'))->delete('service_provider_categories');
            foreach ($this->input->post('alternative_services') as $ser) {
                $this->db->insert('service_provider_categories', array('user_id' => $this->input->post('user_id'), 'category_id' => $ser));
            }
        }
        $this->session->set_flashdata('msg', 'Details updated Successfully');
        redirect('user/index');
    }

    public function rates()
    {
        $user_id = $this->input->post('user_id');
        unset($_POST['user_id']);
//        var_dump($this->input->post());

        foreach ($this->input->post() as $key => $post) {

            if ($key == 'main_cat') {
                $this->db->where('user_id', $user_id)->update('service_providers', array('rate' => $post));
                unset($_POST['main_cat']);
            } else {
                $id = substr($key, 6);
                $this->db->where('id', $id)->update('service_provider_categories', array('rate' => $post));
            }

        }
        $this->db->where('id', $user_id)->update('users', array('step' => 4));

        $this->session->set_flashdata('msg', 'Rates added Successfully');
        redirect('user/index');
    }

    public function uniqueID($length_of_string)
    {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ' . rand(1000, 1000000);

        // Shufle the $str_result and returns substring
        // of specified length
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

    public function payment()
    {
        echo 'payment<br>';

        $amount = $this->db->where('property_name', 'SP_REG_FEE')->limit(1)->get('system_properties')->result_array();
        $data = array(
            'user_id' => $this->input->get('order_id'),
            'amount' => $amount[0]['value'],
            'reason' => 'SP_REG_FEE'
        );
        if (!$this->db->where('reason', 'SP_REG_FEE')->where('user_id', $this->input->get('order_id'))->get('payments')->num_rows()) {

            $this->db->insert('payments', $data);
        }

        $this->db->where('id', $this->input->get('order_id'))->update('users', array('step' => 4, 'is_completed' => 1));

        $this->session->set_flashdata('msg', 'Payment done successfully');
        redirect('user/index');
    }

    public function cancel()
    {
        echo 'cancel<br>';

        var_dump($this->input->post());
    }

    public function noti()
    {

    }

    public function uploadimage()
    {
        $image_name = time() . '-' . str_replace(' ', '-', str_replace('_', '-', $_FILES["profile_pic"]['name']));
        $config['upload_path'] = './assets/img/profile_images/';
        $config['allowed_types'] = 'jpg|png|pdf';
        $config['max_size'] = 2000;
//        $config['max_width'] = 1500;
//        $config['max_height'] = 1500;
        $config['file_name'] = $image_name;
        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('profile_pic')) {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('user/index');
        } else {
            $ext_img = $this->db->where('user_id', $this->input->post('user_id'))->get('service_providers')->result_array();

            if (!is_null($ext_img[0]['profile_image'])) {
                if (file_exists($config['upload_path'] . $ext_img[0]['profile_image'])) {
                    unlink($config['upload_path'] . $ext_img[0]['profile_image']);
                }
            }

            $res = $this->db->where('user_id', $this->input->post('user_id'))->update('service_providers', array('profile_image' => $image_name));
            if ($res) {
                $this->session->set_flashdata('msg', 'Image Uploaded successfully');
                redirect('user/index');
            } else {
                $this->session->set_flashdata('error', 'Image Not saved!');
                redirect('user/index');
            }

        }
    }

    public function calcAge($date)
    {
        $from = new DateTime($this->input->post('dob'));
        $to = new DateTime('today');
        return $from->diff($to)->y;
    }

    public function reviewUpdate()
    {

        $data = array(
            'value' => $this->input->post('rate'),
            'description' => $this->input->post('description'),
        );

        $res = $this->db->where('id', $this->input->post('service_provider_rates_id'))->update('service_provider_rates', $data);
        if ($res) {
            $this->session->set_flashdata('msg', 'Review updated successfully');
        } else {
            $this->session->set_flashdata('error', 'Review not updated!');
        }
        redirect('/service/view/' . $this->input->post('service_provider_id'), 'refresh');
    }

    public function reviewAdd()
    {
        var_dump($this->input->post());

        $data = array(
            'value' => $this->input->post('rate'),
            'description' => $this->input->post('description'),
            'user_id' => $this->input->post('service_provider_user_id'),
            'created_by' => $this->input->post('created_by'),

        );

        $res = $this->db->insert('service_provider_rates', $data);
        if ($res) {
            $this->session->set_flashdata('msg', 'Review updated successfully');
        } else {
            $this->session->set_flashdata('error', 'Review not updated!');
        }
        redirect('/service/view/' . $this->input->post('service_provider_id'), 'refresh');
    }

    public function my_orders()
    {
        $status = $this->input->get('state');
        if ($this->ion_auth->logged_in()) {
            $user = $this->ion_auth->get_Logged_user();
            if ($this->ion_auth->is_serviceprovider()) {
                if ($user['user_type'] == 'service_provider' && $user['is_completed'] == 0 && $user['step'] < 4) {
                    redirect('user/index');
                }
                $this->data['orders'] = $this->service_model->allMyRequestOrders($user['id'], $status);
//                var_dump(count($this->data['orders']));
                return $this->load->view('front/my_requests', $this->data);
            } elseif ($this->ion_auth->is_member()) {
                $this->data['reasons'] = $this->db->where('status', 1)->get('cancellation_reasons')->result_array();
                $this->data['orders'] = $this->service_model->allMyOrders($user['id']);
                return $this->load->view('front/my_orders', $this->data);
            }
        } else {
            redirect(base_url() . 'auth/login', 'refresh');
        }
//        var_dump($this->data['orders']);

    }

    public function mark_completed()
    {
        $this->db->where('id', $this->input->post('id'))->update('service_requests', array('status' => 2));
        echo json_encode(1);
    }

    public function mark_accept()
    {
        $rec = $this->db->where('id', $this->input->post('id'))->get('service_requests')->result_array();

        $this->db->where('id', $this->input->post('id'))->update('service_requests', array('status' => 1));
        $this->save_noti($rec[0]['service_provider_id'], $rec[0]['customer_id'], ' is accepted your request');

        echo json_encode(1);
    }

    public function mark_reject()
    {
        $this->db->where('id', $this->input->post('id'))->update('service_requests', array('status' => 3, 'remark' => $this->input->post('reason')));
        $this->refundAmount($this->input->post('id'), '');

        echo json_encode(1);
    }

    public function cancel_accept()
    {
        $this->db->where('id', $this->input->post('id'))->update('service_requests', array('status' => 5));
        $this->refundAmount($this->input->post('id'), '');
        echo json_encode(1);
    }

    public function request_reject()
    {
        $order = $this->db->where('id', $this->input->post('serv_id'))->get('service_requests')->result_array();
        $amount = $this->db->where('order_id', $this->input->post('serv_id'))->where('payment_done_by', $this->input->post('user_id'))->get('payments')->result_array();


        if (count($order)) {
//            if ($order[0]['status'] == 1) {
//                $this->db->where('id', $this->input->post('serv_id'))->update('service_requests', array('status' => 4, 'cancel_reason' => $this->input->post('reason')));
//                $this->session->set_flashdata('msg', 'Requested to service cancellation.');
//            } else {
//            }
            $this->refundAmount($this->input->post('serv_id'), $this->input->post('reason'));
        }
        redirect(base_url() . 'my-account/orders', 'refresh');

    }

    public function save_noti($sp_id, $user_id, $msg)
    {

        $user = $this->db->where('id', $sp_id)->select('first_name,last_name,phone')->get('users')->result_array();
        $msg = $user[0]['first_name'] . ' ' . $user[0]['last_name'] . ' - (' . $user[0]['phone'] . ') ' . $msg;
        $data = array(
            'user_id' => $user_id,
            'description' => $msg,
            'status' => 0
        );

        $this->db->insert('notifications', $data);


    }

    public function refundAmount($serv_id, $reason)
    {
        $user = $this->ion_auth->get_Logged_user();
        $order = $this->db->where('id', $serv_id)->get('service_requests')->result_array();
        $amount = $this->db->where('order_id', $serv_id)->where('payment_done_by', $user['id'])->get('payments')->result_array();

        $this->db->where('id', $serv_id)->update('service_requests', array('status' => 5));
        if ($reason != '') {
            $this->db->where('id', $serv_id)->update('service_requests', array('cancel_reason' => $reason));
        }
//                add refund table
        $data = array(
            'user_id' => $user['id'],
            'sp_id' => $order[0]['service_provider_id'],
            'order_id' => $order[0]['id'],
            'amount' => $amount[0]['amount']
        );
        $this->db->insert('refunds', $data);

//add refund to payment table
        $data = array(
            'payment_done_by' => $user['id'],
            'user_id' => $order[0]['service_provider_id'],
            'order_id' => $order[0]['id'],
            'amount' => -1 * $amount[0]['amount'],
            'reason' => 'REFUND'
        );
        $this->db->insert('payments', $data);
        $this->save_noti($order[0]['service_provider_id'], $user['id'], $order[0]['request_no'] . ' Service request cancelled Successfully');
        $this->save_noti($user['id'], $order[0]['service_provider_id'], $order[0]['request_no'] . ' Service request cancelled by Customer');
        $this->session->set_flashdata('msg', 'Service request cancelled Successfully');
    }

//    appointment
    public function makeAppointment()
    {
        $this->load->view('front/appointment');
    }

    public function saveAppointment()
    {
        var_dump($this->input->post());

        $data = array(
            'user_id'=>$this->input->post('user_id'),
            'subject'=>$this->input->post('subject'),
            'date'=>$this->input->post('date'),
            'time_slot'=>$this->input->post('time_slot'),
            'matter'=>$this->input->post('matter'),
        );
        $this->db->insert('appointments',$data);

        $this->session->set_flashdata('msg', 'Appointment submitted Successfully! We will get back you soon.');
        redirect(base_url() . 'appointment/create', 'refresh');

    }
}
