<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
//        $this->load->database();
        $this->load->library('pagination');
        $this->load->model("service_provider_model");
        $this->load->model("payment_model");
    }

    public function index()
    {
        $district_id = null;
        $this->data['cities'] = [];

        if (!is_null($this->input->get('district'))) {
            $this->data['cities'] = $this->db->where('status', 1)->where('main_id', $this->input->get('district'))->get('locations')->result_array();
        }

        $this->data['categories'] = $this->db->where('status', 1)->get('categories')->result_array();
        $this->data['districts'] = $this->db->where('status', 1)->where('main_id', null)->get('locations')->result_array();

        $query = array(
            'district' => $this->input->get('district'),
            'city' => $this->input->get('city'),
            'category' => $this->input->get('category'),
        );

        $config['base_url'] = base_url() . 'service/all';
        $page = ($this->input->get('page')) ? $this->input->get('page') : 0;
        $config['per_page'] = 10;
        $this->data['all_records_count'] = $this->service_provider_model->getCountOfQuery(null, null, $query)->num_rows();
        $config['total_rows'] = $this->data['all_records_count'];
        $config['enable_query_strings'] = true;
        $config['use_page_numbers'] = true;
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");

        $config["uri_segment"] = 3;
        $config["page"] = $page;
        $config["query_data"] = $this->input->get();

        $service_providers = $this->service_provider_model->getCountOfQuery($config['per_page'], $page, $query)->result_array();
        $elements = [];
        foreach ($service_providers as $service_provider) {
            $alternatives = $this->service_provider_model->getAlternativeCategories($service_provider['user_id']);

            $service_provider['human_date'] = $this->timeago($service_provider['created_date']);
            $service_provider['alternative_categories'] = $alternatives;
            $service_provider['rating'] = $this->getRatesOfServiceProvider($service_provider['user_id']);
            array_push($elements, $service_provider);
        }
        $this->data['links'] = $this->create_links($config);
        $this->data['service_providers'] = $elements;
        $this->load->view('front/services', $this->data);
    }

    public function category($id)
    {
        var_dump($id);
    }

    public function create_links($data)
    {
        $page = 0;
        $prev_btn = '';
        $next_btn = '';
        $query = '';
        $numbers = '';
//var_dump($data);
        if (array_key_exists('page', $data['query_data']) && $data['query_data']['page'] > 0) {
            $page = $data['query_data']['page'];
            $data['query_data']['page'] = $data['query_data']['page'] - 1;
        }

        if ($page > 0) {
            if (count($data['query_data'])) {
                $query = http_build_query($data['query_data']);
            }

            $prev_btn = '<li class="page-item">
                        <a class="page-link" href="' . base_url() . 'service/all?' . $query . '" aria-label="Previous">
                           <span aria-hidden="true">&laquo;</span>
                           <span class="sr-only">Previous</span>
                        </a>
                      </li>';
        } else {
            $prev_btn = '<li class="page-item">
                        <a class="page-link link-disable" href="#" aria-label="Previous" aria-disabled="true">
                           <span aria-hidden="true">&laquo;</span>
                           <span class="sr-only">Previous</span>
                        </a>
                      </li>';
        }

        for ($x = 0; $x < ($data['total_rows'] / $data['per_page']); $x++) {
            $active = '';

            if ($page == $x) {
                $active = 'active';
            }

            if (count($data['query_data'])) {
                $data['query_data']['page'] = $x;
                $query = http_build_query($data['query_data']);
            }
            $numbers = $numbers . '<li class="page-item ' . $active . '"><a class="page-link" href="' . base_url() . 'service/all?' . $query . '">' . ($x + 1) . '</a></li>';
        }
        if ($data['total_rows'] > ($page + 1) * $data['per_page']) {
            $data['query_data']['page'] = $page + 1;
            $query = http_build_query($data['query_data']);
            $next_btn = '<a class="page-link" href="' . base_url() . 'service/all?' . $query . '" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                         </a>';
        } else {
            $next_btn = '<a class="page-link link-disable" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                         </a>';
        }

        $html = '<nav aria-label="Page navigation example"><ul class="pagination">';
        $html = $html . $prev_btn;
        $html = $html . $numbers;
        $html = $html . $next_btn;
        $html = $html . '</ul></nav>';


        return $html;
    }

    public function view($id)
    {

        $service_providers = $this->service_provider_model->singleServiceProviderDetails($id);
        $elements = [];
        foreach ($service_providers as $service_provider) {
            $alternatives = $this->service_provider_model->getAlternativeCategories($service_provider['user_id']);

            $service_provider['human_date'] = $this->timeago($service_provider['created_date']);
            $service_provider['alternative_categories'] = $alternatives;
            array_push($elements, $service_provider);
        }
        $this->data['rate'] = $this->getRatesOfServiceProvider($service_provider['user_id']);
        $this->db->where('id', $id)->update('service_providers', array('rating_value' => $this->data['rate']['sum']));

        $this->data['details'] = $elements;
        $reviews_temp = $this->service_provider_model->getReviewsByUserID($service_provider['user_id']);

        $reviews = [];
        foreach ($reviews_temp as $ele) {
            $ele['human_date'] = $this->timeago($ele['created_date']);
            array_push($reviews, $ele);
        }
        $this->data['reviews'] = $reviews;
//        var_dump($this->data['details']);
        $this->load->view('front/service_single', $this->data);
    }

    public function getCityByLocationID()
    {

    }

    public function timeago($date)
    {
        $timestamp = strtotime($date);

        $strTime = array("second", "minute", "hour", "day", "month", "year");
        $length = array("60", "60", "24", "30", "12", "10");

        $currentTime = time();
        if ($currentTime >= $timestamp) {
            $diff = time() - $timestamp;
            for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
                $diff = $diff / $length[$i];
            }

            $diff = round($diff);
            return $diff . " " . $strTime[$i] . "(s) ago ";
        } else {
            return "Today";
        }
    }

    public function getRatesOfServiceProvider($serviceProviderID)
    {
        $sum = $this->db->where('user_id', $serviceProviderID)->select_sum('value')->get('service_provider_rates')->row()->value;
        $num = $this->db->where('user_id', $serviceProviderID)->get('service_provider_rates')->num_rows();
        if ($num == 0) {
            $rate = round($sum, 1);
        } else {
            $rate = round($sum / $num, 1);
        }


        return array('rate' => $rate, 'num' => $num, 'sum' => $sum);

    }

    public function book($id)
    {
        $this->data['errors'] = '';

        if (!$this->ion_auth->logged_in()) {
            redirect(base_url() . 'auth/login?return_url=' . base_url() . 'service/book/' . $id, 'refresh');
        }

        if ($this->ion_auth->is_serviceprovider()) {
            $this->data['errors'] = 'You are a Service Provider. You can not book a service';
        }

        $service_providers = $this->service_provider_model->singleServiceProviderDetails($id);
        $elements = [];
        foreach ($service_providers as $service_provider) {
            $alternatives = $this->service_provider_model->getAlternativeCategories($service_provider['user_id']);

            $service_provider['human_date'] = $this->timeago($service_provider['created_date']);
            $service_provider['alternative_categories'] = $alternatives;
            array_push($elements, $service_provider);
        }
        $this->data['rate'] = $this->getRatesOfServiceProvider($service_provider['user_id']);
        $this->data['cities'] = $this->db->where('status', 1)->where('main_id is NOT NULL', NULL, FALSE)->get('locations')->result_array();
        $this->data['details'] = $elements;

        return $this->load->view('front/service_book', $this->data);
    }

    public function bookSave()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect(base_url() . '/auth/login', 'refresh');
        }
//        var_dump($this->input->post());
        $date = new DateTime($this->input->post('required_date'));

        $data = array(
            'request_no' => $this->generateRandomString(),
            'customer_id' => $this->input->post('customer_id'),
            'service_provider_id' => $this->input->post('service_provider_id'),
            'required_date' => $date->format('Y-m-d'),
            'required_time' => $this->input->post('required_time'),
            'category' => json_encode($this->input->post('category')),
            'work_background' => $this->input->post('work_background'),
            'location' => $this->input->post('location'),
            'additional_phone' => ($this->input->post('additional_phone') != null) ? $this->input->post('additional_phone') : null,
        );

        $this->db->insert('service_requests', $data);
//        var_dump( $this->db->insert_id());
        redirect(base_url() . 'service/payment/' . $this->db->insert_id(), 'refresh');
    }

    public function payment($id)
    {
        if (!$this->ion_auth->logged_in()) {
            redirect(base_url() . '/auth/login', 'refresh');
        }
        $this->data['errors'] = '';

        if ($this->ion_auth->is_serviceprovider()) {
            $this->data['errors'] = 'You are a Service Provider. You can not book a service';
        }
        $service_details = $this->db->where('id', $id)->get('service_requests')->result_array();
        if (!count($service_details)) {
            show_404();
        }
        $this->data['service_details'] = $service_details;
        $service_providers = $this->service_provider_model->singleServiceProviderDetailsByUserID($service_details[0]['service_provider_id']);
        $elements = [];
//        var_dump($service_providers);
        foreach ($service_providers as $service_provider) {
            $alternatives = $this->service_provider_model->getAlternativeCategories($service_provider['user_id']);

            $service_provider['human_date'] = $this->timeago($service_provider['created_date']);
            $service_provider['alternative_categories'] = $alternatives;
            array_push($elements, $service_provider);
        }
//        die();
        $this->data['rate'] = $this->getRatesOfServiceProvider($service_provider['user_id']);

        $this->data['details'] = $elements;

        $this->data['charge'] = $this->db->where('property_name', 'SER_RES_FEE')->where('status', 1)->get('system_properties')->result_array();

        return $this->load->view('front/service_book_payment', $this->data);
    }

    public function payment_success()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect(base_url() . 'auth/login', 'refresh');
        }
        $this->data['error'] = '';
        $this->data['success'] = '';

        $amount = $this->db->where('property_name', 'SER_RES_FEE')->limit(1)->get('system_properties')->result_array();
        $service_details = $this->db->where('id', $this->input->get('order_id'))->get('service_requests')->result_array();
        if (!count($service_details)) {
            show_404();
        }
        $data = array(
            'user_id' => $service_details[0]['service_provider_id'],
            'payment_done_by' => $service_details[0]['customer_id'],
            'order_id' => $this->input->get('order_id'),
            'amount' => $amount[0]['value'],
            'reason' => 'SER_RES_FEE'
        );

        $payment_details = $this->payment_model->getPaymentDetails($this->input->get('order_id'), 0);
        if (count($payment_details)) {
            $this->data['error'] = 'Payment Already Done';
        } else {
            $this->db->insert('payments', $data);
            $id = $this->db->insert_id();
            $this->db->where('id', $this->input->get('order_id'))->update('service_requests', array('status' => 0));
            $this->data['success'] = 'Payment has been done successfully !';
            $payment_details = $this->payment_model->getPaymentDetails($this->input->get('order_id'), 0);
//            $this->save_noti($service_details[0]['service_provider_id'], $service_details[0]['customer_id'], ' is accepted your request');

        }
        $this->data['service_details'] = $service_details;
        $this->data['payment_details'] = $payment_details;
        return $this->load->view('front/payment_success', $this->data);
    }


    public function read_noti()
    {

        $this->db->where('user_id', $this->input->post('user_id'))->update('notifications', array('status' => 1));

    }

    public function save_noti($sp_id, $user_id, $msg)
    {

        $user = $this->db->where('id', $sp_id)->select('first_name,last_name,phone')->get('users')->result_array();
        $msg = $user[0]['first_name'] . ' ' . $user[0]['last_name'] . ' - (' . $user[0]['phone'] . ') ' . $msg;
        $data = array(
            'user_id' => $user_id,
            'description' => $msg,
            'status' => 0
        );

        $this->db->insert('notifications', $data);


    }

    function generateRandomString() {
        $length =8;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
