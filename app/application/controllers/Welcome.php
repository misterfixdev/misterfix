<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
//        $this->load->database();
    }

    public function index()
    {
        $query = $this->db->where('status', 1)->get('categories');

        $this->data['categories'] = $query->result_array();

        $this->load->view('front/index', $this->data);
    }

    public function catalog($id)
    {
        var_dump($id);
    }

    public function edit($id)
    {
        var_dump($id);
    }

    public function unverified()
    {
        $user = $this->ion_auth->get_Logged_user();
        if ($user['user_type'] == 'service_provider' && $user['admin_verify']) {
            redirect('service/index');
        }

        $this->load->view('front/unverified');
    }
}
