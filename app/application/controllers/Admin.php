<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation $form_validation The form validation library
 */
class Admin extends CI_Controller
{
    public $data = [];

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->lang->load('auth');
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        if (!$this->ion_auth->is_admin()) {
            redirect('/', 'refresh');
        }

        $this->data['user'] = $this->ion_auth->get_Logged_user();
        $this->data['main_title'] = 'Dashboard';
        $this->data['sub_title'] = 'Control Panel';
        $this->load->model("payment_model");
        $this->load->model("Supervisor_model");


    }

    /**
     * Redirect if needed, otherwise display the user list
     */
    public function index()
    {
        $this->data['members'] = $this->db->where('user_type', 'member')->get('users')->num_rows();
        $this->data['sps'] = $this->db->where('user_type', 'service_provider')->get('users')->num_rows();
        $this->data['supervisors'] = $this->db->where('status', 1)->get('supervisors')->num_rows();
        $this->data['service_requests'] = $this->db->get('service_requests')->num_rows();
        $this->data['withdrawals'] = $this->db->select_sum('amount')->where('payout', 1)->get('payments')->row()->amount;

        $s_r = $this->db->order_by('created_date', 'DESC')->get('service_requests')->result_array();
        $res = array();
        foreach ($s_r as $key => $itm) {
            $itm['created_date'] = date("Y-m-d", strtotime($itm['created_date']));
            array_push($res, $itm);
        }
        $arr = array();

        foreach ($res as $key => $item) {
            $arr[$item['created_date']][$key] = $item;
        }

        ksort($arr, SORT_NUMERIC);

        $cnt = array();
        foreach ($arr as $key => $i) {
            array_push($cnt, array('y' => $key, 'item' => count($arr[$key])));
        }
        $this->data['cnt'] = json_encode($cnt);
        $this->load->view('backend/index', $this->data);
    }

//members
    public function members_list()
    {
        $this->data['main_title'] = 'List Members';
        $this->data['sub_title'] = 'Users';
        $this->data['members'] = $this->db->where('user_type', 'member')->get('users')->result_array();
        $this->load->view('backend/list_members', $this->data);
    }

//service providers
    public function serviceproviders_list()
    {
        $this->data['main_title'] = 'List Service Providers';
        $this->data['sub_title'] = 'Users';
        $this->data['members'] = $this->db->where('user_type', 'service_provider')->get('users')->result_array();
        $this->load->view('backend/list_service_providers', $this->data);
    }

    public function serviceproviders_verify()
    {
        $this->db->where('id', $this->input->post('id'))->update('users', array('admin_verify' => $this->input->post('status')));
        $this->session->set_flashdata('msg', 'Service Provider verified Successfully');
        redirect('admin/serviceproviders_list');
    }

//    Payments
    public function payments_list()
    {
        $this->data['main_title'] = 'List Payments';
        $this->data['sub_title'] = 'Payments';
        $payments = $this->payment_model->getAllPayments();

        $result = array();
        foreach ($payments as $element) {
            $result[$element['user_id']][] = $element;
        }

        $this->data['payments'] = $result;

//        echo '<pre>' . var_export($result, true) . '</pre>';
//        die();
        $this->load->view('backend/list_payments', $this->data);
    }

    public function payments_withdraw()
    {
        $logged_user = $this->ion_auth->get_Logged_user();
//        var_dump();

        $data = array(
            'user_id' => $this->input->post('user_id'),
            'amount' => $this->input->post('amount'),
            'created_by' => $logged_user['id'],
        );

        $this->db->insert('withdrawals', $data);

        foreach (json_decode($this->input->post('ids')) as $id) {
            $this->db->where('id', $id)->update('payments', array('payout' => 1));
        }

        $this->db->insert('notifications', array('user_id' => $this->input->post('user_id'), 'description' => $this->input->post('amount') . ' LKR withdraw to your account.'));

        $this->session->set_flashdata('msg', 'Withdrawal done successfully');
        redirect('admin/payments_list');
    }

    public function payments_withdrawals()
    {
        $this->data['main_title'] = 'Withdrawals';
        $this->data['sub_title'] = 'Payments';
        $this->data['withdrawals'] = $this->payment_model->getAllWithdrawals();

        $this->load->view('backend/withdrawals', $this->data);
    }

    public function refunds()
    {
        $this->data['main_title'] = 'List Payments';
        $this->data['sub_title'] = 'Payments';
        $payments = $this->payment_model->getAllRefunds();

        $result = array();
        foreach ($payments as $element) {
            $result[$element['user_id']][] = $element;
        }

        $this->data['payments'] = $result;

//        echo '<pre>' . var_export($result, true) . '</pre>';
//        die();
        $this->load->view('backend/refunds', $this->data);
    }

    public function refund_withdraw()
    {
        $logged_user = $this->ion_auth->get_Logged_user();
//        var_dump();

        $data = array(
            'user_id' => $this->input->post('user_id'),
            'amount' => $this->input->post('amount'),
            'created_by' => $logged_user['id'],
        );

        $this->db->insert('withdrawals', $data);

        foreach (json_decode($this->input->post('ids')) as $id) {
            $this->db->where('id', $id)->update('refunds', array('payout' => 1));
        }

        $this->db->insert('notifications', array('user_id' => $this->input->post('user_id'), 'description' => $this->input->post('amount') . ' LKR refunded to your account.'));

        $this->session->set_flashdata('msg', 'Refunded done successfully');
        redirect('admin/refunds');
    }

//    supervisors

    public function supervisors_add()
    {
        $this->data['main_title'] = 'Add / List Supervisor';
        $this->data['sub_title'] = 'Supervisors';
        $this->data['categories'] = $this->db->where('status', 1)->get('categories')->result_array();
        $this->data['cities'] = $this->db->where('status', 1)->where('main_id is NOT NULL', NULL, FALSE)->get('locations')->result_array();
        $this->data['supervisors'] = $this->db->where('supervisors.status', 1)
            ->join('locations', 'supervisors.location = locations.id', 'left')
            ->select('supervisors.name as name, supervisors.mobile as mobile, supervisors.status as status, locations.name as location')
            ->get('supervisors')->result_array();

        $this->load->view('backend/add_supervisor', $this->data);

    }

    public function supervisors_save()
    {
//        var_dump($this->input->post());
        $data = array(
            'name' => $this->input->post('name'),
            'location' => $this->input->post('location'),
            'mobile' => $this->input->post('mobile'),
        );
        $this->db->insert('supervisors', $data);
        $id = $this->db->insert_id();
        foreach ($this->input->post('categories') as $cat) {
            $data = array(
                'supervisor_id' => $id,
                'category_id' => $cat,
            );
            $this->db->insert('supervisor_categories', $data);

        }

        $this->session->set_flashdata('msg', 'Details updated Successfully');
        redirect('admin/supervisors_add');
    }

    public function supervisors_assign()
    {
        $this->data['main_title'] = 'Assign Supervisor';
        $this->data['sub_title'] = 'Supervisors';
        $this->data['text'] = '';
        $this->data['categories'] = $this->db->where('status', 1)->get('categories')->result_array();
        $this->data['cities'] = $this->db->where('status', 1)->where('main_id is NOT NULL', NULL, FALSE)->get('locations')->result_array();

//        all supervisors
        $this->data['supervisors'] = '';
//all tasks
        $this->data['tasks'] = $this->db->where_in('service_requests.status', array(1, 0))
            ->join('users', 'service_requests.service_provider_id = users.id', 'left')
            ->join('locations', 'service_requests.location = locations.id', 'left')
            ->where('service_requests.supervisor_id', null)
            ->select('service_requests.*, users.first_name as first_name, users.last_name as last_name,locations.name as location,service_requests.location as location_id')
            ->get('service_requests')->result_array();

        if ($this->input->get('task') != null) {
            $single = $this->db->where_in('service_requests.status', array(1, 0))
                ->join('users', 'service_requests.service_provider_id = users.id', 'left')
                ->join('locations', 'service_requests.location = locations.id', 'left')
                ->where('service_requests.supervisor_id', null)
                ->where('service_requests.id', $this->input->get('task'))
                ->select('service_requests.*, users.first_name as first_name, users.last_name as last_name,
                locations.name as location,service_requests.location as location_id')
                ->get('service_requests')->result_array();

            $text = "";
            $text = $text . '<span class="badge badge-success">Location : ' . $single[0]['location'] . '</span>&nbsp;';
//            foreach (json_decode($single[0]['category'], true) as $cat) {
//                $text = $text . '<span class="badge badge-success">' . $cat . '</span>&nbsp;';
//            }
            $this->data['text'] = $text;

            $this->data['supervisors'] = $this->db->where('supervisors.status', 1)
                ->join('locations', 'supervisors.location = locations.id', 'left')
                ->where('supervisors.location', $single[0]['location_id'])
                ->select('supervisors.id as id,supervisors.name as name, supervisors.mobile as mobile, supervisors.status as status, locations.name as location')
                ->get('supervisors')->result_array();


        }
        $this->load->view('backend/assign_supervisor', $this->data);

    }

    public function supervisors_single()
    {
        $supervisor_data = $this->db->where('supervisors.id', $this->input->post('id'))
            ->join('locations', 'supervisors.location = locations.id', 'left')
            ->select('supervisors.id as id,supervisors.name as name, supervisors.mobile as mobile, 
            supervisors.status as status, locations.name as location, supervisors.created_date as joined_date')
            ->get('supervisors')->result_array();

        $category_data = $this->db->where('supervisor_categories.supervisor_id', $this->input->post('id'))
            ->join('categories', 'supervisor_categories.category_id = categories.id', 'left')
            ->where('supervisor_categories.status', 1)
            ->select('categories.name as category')
            ->get('supervisor_categories')->result_array();

        $data = array(
            'supervisor' => $supervisor_data,
            'category' => $category_data
        );
        echo json_encode($data);
    }

    public function supervisors_assign_save()
    {
//        var_dump($this->input->post());
        $this->db->where('id', $this->input->post('task_id'))->update('service_requests', array('supervisor_id' => $this->input->post('supervisor')));

        $this->session->set_flashdata('msg', 'Supervisor assigned Successfully');
        redirect('admin/supervisors_assign');
    }

//    service category

    public function category_list()
    {
        $this->data['main_title'] = 'List Service Category';
        $this->data['sub_title'] = 'Service Category';
        $this->data['categories'] = $this->db->get('categories')->result_array();
        $this->load->view('backend/list_service_category', $this->data);
    }

    public function service_request_list()
    {
        $this->data['main_title'] = 'List Service Requests';
        $this->data['sub_title'] = 'Service Requests';
        $this->data['requests'] = $this->payment_model->getServiceRequests();
        $this->load->view('backend/list_service_requests', $this->data);
    }

//appointments
    public function appointments()
    {
        $follow= $this->input->get('follow');
        $this->data['main_title'] = 'List Appointments';
        $this->data['sub_title'] = 'Service Requests';
        $this->data['apps'] = $this->payment_model->getAppointments($follow);
//        var_dump($this->data['apps']);
        $this->load->view('backend/appointments', $this->data);
    }
    public function markAppointments()
    {
        $this->db->where('id', $this->input->post('id'))->update('appointments', array('is_followed_up' => 1));

        $this->session->set_flashdata('msg', 'Appointment marked as Followed up');
        redirect('admin/appointments');
    }

}
