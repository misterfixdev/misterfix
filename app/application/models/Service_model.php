<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Ion_auth $ion_auth The Ion_auth library
 */
class Service_model extends CI_Model
{
    public function allMyOrders($user_id)
    {

        return $this->db->distinct()
            ->where('service_requests.customer_id', $user_id)
            ->join('users', 'service_requests.service_provider_id = users.id', 'left')
            ->join('payments', 'service_requests.id = payments.order_id AND payments.amount > 0', 'left')
            ->join('supervisors', 'service_requests.supervisor_id = supervisors.id', 'left')
            ->order_by('service_requests.created_date', 'DESC')
            ->select('service_requests.*, users.first_name as first_name, users.last_name as last_name,
            payments.*, service_requests.id as ser_id, service_requests.created_date as created_on,
           service_requests.supervisor_id as supervisor_id, supervisors.name as sup_name, supervisors.mobile as sup_mobile'
            )
            ->get('service_requests')->result_array();
    }

    public function allMyRequestOrders($user_id, $state)
    {

        $q = $this->db->distinct()
            ->where('service_requests.service_provider_id', $user_id)
            ->join('users', 'service_requests.service_provider_id = users.id', 'left')
            ->join('payments', 'service_requests.id = payments.order_id AND payments.amount > 0', 'left')
            ->join('cancellation_reasons', 'service_requests.cancel_reason = cancellation_reasons.id', 'left')
            ->join('supervisors', 'service_requests.supervisor_id = supervisors.id','left')
            ->order_by('service_requests.created_date', 'DESC');
        if ($state != '') {
            $q->where('service_requests.status', $state);
        }
        $q->where('service_requests.status >=', 0);

        return $q->select('service_requests.*, users.first_name as first_name, users.last_name as last_name, payments.*, 
            service_requests.id as ser_id, service_requests.created_date as created_on,
            service_requests.supervisor_id as supervisor_id, supervisors.name as sup_name, supervisors.mobile as sup_mobile,
            cancellation_reasons.reason as cancel_reason')
            ->get('service_requests')->result_array();
    }
}