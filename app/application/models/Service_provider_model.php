<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Ion_auth $ion_auth The Ion_auth library
 */
class Service_provider_model extends CI_Model
{
    public function getCountOfQuery($per_page = null, $page = null, $query)
    {
//        var_dump($query);

        if (!is_null($query['category'])) {
            $b = $this->db->where('service_provider_categories.category_id', $query['category'])
                ->select('service_provider_categories.user_id')
                ->get('service_provider_categories')->result_array();
            $z = [];
            foreach ($b as $c) {
                array_push($z, $c['user_id']);
            }
//            var_dump((array_values($z)));
        }

        $a = $this->db->where('service_providers.status', 1)
            ->join('users', 'service_providers.user_id = users.id', 'left')
            ->where('users.is_completed', 1)
            ->where('users.admin_verify', 1)
            ->join('categories', 'service_providers.category_id = categories.id', 'left')
            ->join('locations as dis', 'service_providers.location_id = dis.id')
            ->join('locations as city', 'service_providers.city_id = city.id')
            ->order_by('rating_value', 'DESC');;

        if (!is_null($query['category'])) {
            $a->where('service_providers.category_id', $query['category']);
            if (count($z)) {
                $a->or_where_in('service_providers.user_id', $z);
            }
        }

        if (!is_null($query['district']) && $query['district'] != 'srilanka') {
            $a->where('service_providers.location_id', $query['district']);
        }

        if (!is_null($query['city'])) {
            $a->where('service_providers.city_id', $query['city']);
        }
//        var_dump($page);
        if ($page == 0) {
            $a->limit($per_page);
        } else {
            $a->limit($per_page, $per_page * $page);
        }

        $q = $a->select('service_providers.user_id as user_id, service_providers.id as id, service_providers.profile_image as profile_image, dis.name as district,city.name as city,users.first_name as first_name,users.last_name,service_providers.created_date as created_date,service_providers.expariance as expariance, service_providers.time_to_call_from as time_to_call_from,service_providers.time_to_call_to as time_to_call_to, categories.name as main_category_name, categories.icon as main_category_icon')
            ->get('service_providers');


        return $q;
    }

    public function getAlternativeCategories($id)
    {
        return $this->db->where('service_provider_categories.status', 1)
            ->where('service_provider_categories.user_id', $id)
            ->join('categories', 'service_provider_categories.category_id = categories.id', 'left')
            ->select('service_provider_categories.id as service_provider_categories, categories.name as category_name, categories.icon as category_icon, service_provider_categories.rate as rates')
            ->get('service_provider_categories')
            ->result_array();
    }

    public function singleServiceProviderDetails($id)
    {
        return $this->db->where('service_providers.status', 1)
            ->where('service_providers.id', $id)
            ->join('users', 'service_providers.user_id = users.id', 'left')
            ->join('categories', 'service_providers.category_id = categories.id', 'left')
            ->join('locations as dis', 'service_providers.location_id = dis.id')
            ->join('locations as city', 'service_providers.city_id = city.id')
            ->select('service_providers.user_id as user_id, service_providers.id as id, 
            service_providers.profile_image as profile_image, dis.name as district,
            city.name as city,users.first_name as first_name,users.last_name,
            service_providers.created_date as created_date,service_providers.expariance as expariance, 
            service_providers.time_to_call_from as time_to_call_from, service_providers.time_to_call_to as time_to_call_to,
            service_providers.rate as main_rate,
            categories.name as main_category_name, categories.icon as main_category_icon,
            service_providers.dob as dob, service_providers.gender as gender, service_providers.description as description,
            service_providers.category_id as category_id, service_providers.working_days as working_days, 
            service_providers.age as age, users.phone as phone, service_providers.working_area as working_area')
            ->limit(1)
            ->get('service_providers')->result_array();
    }

    public function singleServiceProviderDetailsByUserID($id)
    {
        return $this->db->where('service_providers.status', 1)
            ->where('service_providers.user_id', $id)
            ->join('users', 'service_providers.user_id = users.id', 'left')
            ->join('categories', 'service_providers.category_id = categories.id', 'left')
            ->join('locations as dis', 'service_providers.location_id = dis.id')
            ->join('locations as city', 'service_providers.city_id = city.id')
            ->select('service_providers.user_id as user_id, service_providers.id as id, 
            service_providers.profile_image as profile_image, dis.name as district,
            city.name as city,users.first_name as first_name,users.last_name,service_providers.created_date as created_date,
            service_providers.expariance as expariance, service_providers.time_to_call_from as time_to_call_from,
            service_providers.time_to_call_to as time_to_call_to,service_providers.rate as rate, 
            categories.name as main_category_name, categories.icon as main_category_icon, service_providers.city_id as city_id,
            service_providers.dob as dob, service_providers.gender as gender, service_providers.description as description,
            service_providers.category_id as category_id, service_providers.working_days as working_days, 
            service_providers.age as age,users.phone as phone, service_providers.working_area as working_area')
            ->limit(1)
            ->get('service_providers')->result_array();
    }

    public function getServiceProviderCategoryByUserID($id)
    {
        return $this->db->where('service_provider_categories.status', 1)
            ->where('service_provider_categories.user_id', $id)
            ->join('users', 'service_provider_categories.user_id = users.id', 'left')
            ->join('categories', 'service_provider_categories.category_id = categories.id', 'left')
            ->select('service_provider_categories.user_id as user_id, service_provider_categories.id as id, 
            users.first_name as first_name,users.last_name,service_provider_categories.created_date as created_date,
            categories.name as main_category_name, categories.icon as main_category_icon, service_provider_categories.rate as rate')
            ->get('service_provider_categories')->result_array();
    }


    public function getReviewsByUserID($id)
    {
        return $this->db->where('service_provider_rates.user_id', $id)
            ->join('users', 'service_provider_rates.created_by = users.id', 'left')
            ->select('service_provider_rates.*, users.first_name as first_name,users.last_name')
            ->order_by('service_provider_rates.created_date', 'DESC')
            ->get('service_provider_rates')->result_array();


    }
}