<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Ion_auth $ion_auth The Ion_auth library
 */
class Supervisor_model extends CI_Model
{
    public function getSupervisors(){

        return $this->db->where('supervisors.status',1)
            ->join('supervisor_categories', 'service_requests.service_provider_id = supervisor_categories.supervisor_id', 'left')
            ->join('payments', 'service_requests.id = payments.order_id', 'left')
            ->order_by('service_requests.created_date', 'DESC')
            ->select('service_requests.*, users.first_name as first_name, users.last_name as last_name,
            payments.*, service_requests.id as ser_id, service_requests.created_date as created_on')
            ->get('service_requests')->result_array();
    }

}