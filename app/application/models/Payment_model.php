<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Ion_auth $ion_auth The Ion_auth library
 */
class Payment_model extends CI_Model
{
    public function getPaymentDetails($order_id, $payout)
    {
        return $this->db->where('payments.order_id', $order_id)
            ->where('payments.payout', $payout)
            ->join('users', 'payments.payment_done_by = users.id', 'left')
            ->select('payments.*, users.*')
            ->get('payments')->result_array();
    }

    public function getAllPayments()
    {
        return $this->db->where('payout', 0)
            ->join('users as usr', 'payments.user_id = usr.id', 'left')
            ->join('users as pd', 'payments.payment_done_by = pd.id', 'left')
            ->select('usr.first_name as user_f_name, usr.last_name as user_l_name,
            pd.first_name as pd_f_name,pd.last_name as pd_l_name,payments.reason as reason, payments.amount as amount,
            payments.created_date as create_date, payments.user_id as user_id, payments.id as payment_ids')
            ->get('payments')->result_array();
    }

    public function getAllRefunds()
    {
        return $this->db->where('payout', 0)
            ->join('users as usr', 'refunds.user_id = usr.id', 'left')
            ->select('usr.first_name as user_f_name, usr.last_name as user_l_name,refunds.amount as amount,
            refunds.created_date as create_date, refunds.user_id as user_id, refunds.id as payment_ids')
            ->get('refunds')->result_array();
    }

    public function getAllWithdrawals()
    {
        return $this->db->join('users as usr', 'withdrawals.user_id = usr.id', 'left')
            ->select('usr.first_name as user_f_name, usr.last_name as user_l_name,
            withdrawals.amount as amount,withdrawals.create_date as create_date, withdrawals.user_id as user_id,
            withdrawals.id as id, withdrawals.created_by as created_by')
            ->get('withdrawals')->result_array();
    }


    public function getServiceRequests()
    {
        return $this->db->join('users as usr', 'service_requests.customer_id = usr.id', 'left')
            ->join('users as sp', 'service_requests.service_provider_id = sp.id', 'left')
            ->join('locations as loc', 'service_requests.location = loc.id', 'left')
            ->join('supervisors as sup', 'service_requests.supervisor_id = sup.id', 'left')
            ->select('usr.first_name as user_f_name, usr.last_name as user_l_name,usr.phone as user_phone,
            sp.first_name as pd_f_name,sp.last_name as pd_l_name,
            service_requests.required_date as required_date, service_requests.required_time as required_time,
            service_requests.category as category, service_requests.work_background as work_background,
            service_requests.additional_phone as additional_phone, service_requests.status as status,
            service_requests.remark as remark, service_requests.created_date as created_date,
            loc.name as location, sup.name as sup_name,sup.mobile as sup_mobile
            ')
            ->get('service_requests')->result_array();
    }

    public function getAppointments($follow =null)
    {
        $q=$this->db->join('users as usr', 'appointments.user_id = usr.id', 'left');

            if($follow != null){
                $q->where('is_followed_up', $follow);
            }

        return $q->select('usr.first_name as user_f_name, usr.last_name as user_l_name,usr.phone as phone,
        appointments.*')
            ->get('appointments')->result_array();
    }
}