<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['service/all'] = 'service/index';
$route['view/:id'] = 'service/view';
$route['book/:id'] = 'service/book';
$route['payment/:id'] = 'service/payment';
$route['pay/success'] = 'service/payment_success';
$route['my-account/orders'] = 'user/my_orders';
$route['my-account/orders-reject'] = 'user/request_reject';
$route['order/completed'] = 'user/mark_completed';
$route['order/accept'] = 'user/mark_accept';
$route['order/reject'] = 'user/mark_reject';
$route['order/cancel_accept'] = 'user/cancel_accept';
$route['service/save/book'] = 'service/bookSave';
$route['category/:id'] = 'service/category';
$route['serviceprovider/account'] = 'user/index';
$route['serviceprovider/details/save'] = 'user/save';
$route['serviceprovider/rates/save'] = 'user/rates';
$route['serviceprovider/category/edit'] = 'user/editCategory';
$route['serviceprovider/details/edit'] = 'user/editDetails';
$route['serviceprovider/details/edit-save'] = 'user/saveDetails';
$route['review/update'] = 'user/reviewUpdate';
$route['review/add'] = 'user/reviewAdd';
$route['appointment/create'] = 'user/makeAppointment';
$route['appointment/save'] = 'user/saveAppointment';

$route['noti/read'] = 'service/read_noti';

//------------------------admin---------------
$route['admin/dashboard'] = 'admin/index';

//Supervisors
$route['admin/supervisors/add-list'] = 'admin/supervisors_add';
$route['admin/supervisors/save'] = 'admin/supervisors_save';
$route['admin/supervisors/assign'] = 'admin/supervisors_assign';
$route['admin/supervisors/single/info'] = 'admin/supervisors_single';
$route['admin/supervisors/assign/save'] = 'admin/supervisors_assign_save';

//members
$route['admin/members/list'] = 'admin/members_list';

//service requests
$route['admin/service-requests/list'] = 'admin/service_request_list';

//service Providers
$route['admin/service-providers/list'] = 'admin/serviceproviders_list';
$route['admin/service-providers/verify'] = 'admin/serviceproviders_verify';

//payments
$route['admin/payments/list'] = 'admin/payments_list';
$route['admin/payments/refunds'] = 'admin/refunds';
$route['admin/payments/refund/withdraw'] = 'admin/refund_withdraw';
$route['admin/payments/withdraw'] = 'admin/payments_withdraw';
$route['admin/payments/withdrawals'] = 'admin/payments_withdrawals';

//service categories
$route['admin/category/list'] = 'admin/category_list';
$route['admin/category/add'] = 'admin/category_add';

//appointments
$route['admin/appointments/list'] = 'admin/appointments';
$route['admin/appointments/mark'] = 'admin/markAppointments';

$route['send-email'] = 'emailcontroller';
$route['email'] = 'emailcontroller/send';
