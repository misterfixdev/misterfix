<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav') ?>

<div id="yellow_flame"></div>
<div class="container">
    <!-- <div class="space-3 lo"></div> -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-pg">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pleft">
                                <center>
                                    <h4><?php echo lang('reset_password_heading'); ?></h4>
                                    <p>To view your account details, please login to your Mister Fix account.</p>
                                    <a href="<?php echo base_url(); ?>"><img class="mt-5"
                                                                             src="<?php echo base_url(); ?>assets/img/logo-bl-theme.png"></a>
                                </center>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pright">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login_form_outer">
                                    <p>Please enter your Mobile Number. <br>So we can send you an OTP to reset your
                                        password.</p>
                                    <div id="infoMessage"><?php echo $message; ?></div>
                                    <?php echo form_open('auth/reset_password/'); ?>


                                    <fieldset>
                                        <br>
                                        <div class="form-group">
                                            <?php echo form_input($phone); ?>

                                        </div>
                                        <div class="form-group">
                                            <?php echo form_input($otp); ?>

                                        </div>
                                        <br>
                                        <hr>
                                        <br>
                                        <div class="form-group">
                                            <?php echo form_input($new_password); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_input($new_password_confirm); ?>
                                        </div>

                                        <?php echo form_input($user_id); ?>
                                        <?php echo form_hidden($csrf); ?>
                                        <br>
                                        <?php echo form_submit('submit', lang('reset_password_submit_btn'), ['class' => 'btn btn-warning btn-block login-but', 'name' => "btnLogin"]); ?>

                                        <br>
                                        <div class="text-center">
                                            <!--                                            <a href="forgot_password">Forgot Password?</a>-->
                                            <br>
                                        </div>
                                    </fieldset>
                                    <hr>
                                    <fieldset>
                                        <div class="text-center">
                                            <?php echo anchor('auth/login', 'Login', ['class' => "btn btn-warning sign-but"]) ?>
                                        </div>
                                    </fieldset>

                                    <?php echo form_close(); ?>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<?php $this->view('front/includes/footer') ?>
