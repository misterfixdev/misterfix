<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav') ?>

<div id="yellow_flame"></div>
<div class="container">
    <!-- <div class="space-3 lo"></div> -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-pg">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pleft">
                                <center>
                                    <h4>Verify Phone Number</h4>
                                    <p>Please check mobile and type the code to verify your mobile number</p>
                                    <a href="<?php echo base_url(); ?>"><img class="mt-5"
                                                                             src="<?php echo base_url(); ?>assets/img/logo-bl-theme.png"></a>
                                </center>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pright">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login_form_outer">
                                    <p>Please check mobile and type the code to verify your mobile number</p>
                                    <?php $this->view('front/includes/message') ?>
                                    <?php echo form_open("auth/otp_verify_save"); ?>

                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" class="form-control login-txt" name="phone" readonly value="<?php echo $phone ?>">
                                            <input type="hidden" class="form-control login-txt" name="id" readonly value="<?php echo $id ?>">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control login-txt" name="otp" placeholder="OTP Code Here">
                                        </div>

                                        <input type="submit" class="btn btn-warning btn-block login-but">
                                        <br>
<!--                                        <div class="text-center">-->
<!--                                            <a href="forgot_password">Forgot Password?</a>-->
<!--                                        </div>-->
                                    </fieldset>
                                    <hr>
                                    <fieldset>
                                        <div class="text-center">
                                            <?php echo anchor('auth/login', 'Login', ['class' => "btn btn-warning sign-but"]) ?>
                                        </div>
                                    </fieldset>

                                    <?php echo form_close(); ?>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<?php $this->view('front/includes/footer') ?>
