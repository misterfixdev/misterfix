<?php $this->view('front/includes/header') ?>
    <body>
<?php $this->view('front/includes/nav') ?>

    <div id="yellow_flame"></div>
<?php $this->view('front/includes/message') ?>
    <div class="container">
        <!-- <div class="space-3 lo"></div> -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-pg">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pleft">
                                    <center>
                                        <h4>Register in Mister Fix</h4>
                                        <p>To view your account details, please register to your Mister Fix.</p>
                                        <a href="<?php echo base_url(); ?>"><img class="mt-5"
                                                                                 src="<?php echo base_url(); ?>assets/img/logo-bl-theme.png"></a>
                                    </center>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pright">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login_form_outer">
                                        <p><?php echo lang('create_user_subheading'); ?></p>
                                        <div id="infoMessage"><?php echo $message; ?></div>
                                        <?php echo form_open("auth/create_user"); ?>

                                        <fieldset>
                                            <div class="form-group">
                                                <?php echo form_input($first_name); ?>
                                            </div>
                                            <div class="form-group">
                                                <?php echo form_input($last_name); ?>
                                            </div>
                                            <!--                                            <div class="form-group">-->
                                            <!--                                                --><?php //echo form_input($email); ?>
                                            <!--                                            </div>-->
                                            <!--                                            <div class="form-group">-->
                                            <!--                                                --><?php //echo form_input($company); ?>
                                            <!--                                            </div>-->
<!--                                            <div class="form-group">-->
<!--                                                --><?php
//                                                if ($identity_column !== 'email') {
//                                                    echo form_error('identity');
//                                                    echo form_input($identity);
//                                                }
//                                                ?>
<!--                                            </div>-->
                                            <div class="form-group">
                                                <small class="text-warning text-left">Type Phone Number <b>71xxxxxxx</b></small>
                                                <?php echo form_input($phone); ?>
                                            </div>
                                            <div class="form-group">
                                                <?php echo form_input($password); ?>
                                            </div>
                                            <div class="form-group">
                                                <?php echo form_input($password_confirm); ?>
                                            </div>
                                            <div class="form-group">
                                                <small style="text-align: left !important;" class="text-warning"><b>User Type</b></small>
                                                <?php echo form_input($user_type); ?>
                                            </div>
                                            <?php echo form_submit('submit', lang('create_user_submit_btn'), ['class' => 'btn btn-warning btn-block login-but', 'name' => "btnLogin"]); ?>

                                        </fieldset>
                                        <hr>
                                        <fieldset>
                                            <div class="text-center">
                                                <label>You have an account?</label><br>
                                                <?php echo anchor('auth/login', 'Login', ['class' => "btn btn-warning sign-but"]) ?>
                                            </div>
                                        </fieldset>
                                        <?php echo form_close(); ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
<?php $this->view('front/includes/footer') ?>