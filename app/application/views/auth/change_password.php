<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav') ?>
<div id="yellow_flame"></div>
<div class="container">
    <!-- <div class="space-3 lo"></div> -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-pg">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pleft">
                                <center>
                                    <h4>Change Password</h4>
                                    <p>To view your account details, please login to your Mister Fix account.</p>
                                    <a href="<?php echo base_url(); ?>"><img class="mt-5"
                                                                             src="<?php echo base_url(); ?>assets/img/logo-bl-theme.png"></a>
                                </center>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pright">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login_form_outer">
                                    <p><?php echo lang('change_password_heading'); ?></p>
                                    <div id="infoMessage"><?php echo $message; ?></div>
                                    <?php echo form_open("auth/change_password");?>

                                    <fieldset>
                                        <div class="form-group">
                                            <?php echo form_input($old_password);?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_input($new_password);?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_input($new_password_confirm);?>
                                        </div>
                                        <?php echo form_input($user_id);?>
                                        <?php echo form_submit('submit', lang('change_password_submit_btn'),['class'=>'btn btn-warning btn-block login-but', 'name'=>"btnLogin"]); ?>

                                        <br>
                                        <div class="text-center">
                                            <a href="<?php echo base_url()?>serviceprovider/account">Back to Account</a>
                                        </div>
                                    </fieldset>

                                    <?php echo form_close(); ?>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->view('front/includes/footer') ?>
