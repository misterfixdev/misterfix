<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav') ?>

<div id="yellow_flame"></div>
<?php $this->view('front/includes/message') ?>
<div class="container">
    <!-- <div class="space-3 lo"></div> -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-pg">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pleft">
                                <center>
                                    <h4><?php echo lang('deactivate_heading'); ?></h4>
                                    <h3 ><?php echo sprintf(lang('deactivate_subheading'), $user->username); ?></h3>
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pright">

                                <?php echo form_open("auth/deactivate/" . $user->id); ?>

                                <p>
                                    <?php echo lang('deactivate_confirm_y_label', 'confirm'); ?>
                                    <input type="radio" name="confirm" value="yes" checked="checked"/>
                                </p>
                                <p>
                                    <?php echo lang('deactivate_confirm_n_label', 'confirm'); ?>
                                    <input type="radio" name="confirm" value="no"/>
                                </p>

                                <?php echo form_hidden($csrf); ?>
                                <?php echo form_hidden(['id' => $user->id]); ?>
                                <?php echo form_hidden(['type' => $user->user_type]); ?>

                                <p><?php echo form_submit('submit', lang('deactivate_submit_btn'),['class'=>'btn btn-success']); ?></p>

                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<?php $this->view('front/includes/footer') ?>



