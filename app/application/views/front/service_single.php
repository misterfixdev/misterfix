<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav');
$user_type = 'member';
if ($this->ion_auth->logged_in()) {
    $user = $this->ion_auth->get_Logged_user();
    $user_type = $user['user_type'];
}
?>


<div id="yellow_flame"></div>
<section class="outer main_view">
    <div class="container">
        <div class="space-3"></div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="profile-sidebar">
                        <div class="card-body text-center border mb-4">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                <img src="<?php echo base_url() ?>assets/img/profile_images/<?php echo ($details[0]['profile_image'] != null) ? $details[0]['profile_image'] : 'default.png' ?>"
                                     class="img-responsive"
                                     alt="">
                            </div>
                            <br>
                            <?php $experiance = floor($details[0]['expariance'] / 12); ?>
                            <?php if ($experiance > 1) { ?>
                                <div class="col ext-prof-view-exp text-center mb-1">
                                    <h5 class="m-0 text-success"><i class="fa fa-briefcase"></i>&nbsp;<span><b>Experience</b></span>
                                    </h5>
                                    <span class="ty"><?php echo $experiance ?> Year(s) Experience</span>
                                </div>
                            <?php } ?>

                        </div>
                        <!-- END SIDEBAR BUTTONS -->
                        <div class="card-body text-center border">
                            <h1 class="text-danger pb-1"><?php echo number_format($rate['rate'], 1, '.', ','); ?></h1>
                            <div class="sub-row text-warning pb-3" style="font-size: 25px">
                                <div class="star-ratings-sprite" title="<?php echo $rate['rate']; ?> Star Rating">
                                    <span style="width:<?php echo ($rate['rate'] / 5) * 100; ?>%"
                                          class="star-ratings-sprite-rating"></span>
                                </div>
                            </div>

                            <p class="text-muted"><?php echo $rate['num']; ?> Rating(s)</p>
                        </div>

                    </div>
                </div>

                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12">

                    <div class="row">
                        <div class="col-md-5">
                            <div class="ext-prof-name-city">
                                <h3><?php echo $details[0]['first_name'] ?><?php echo $details[0]['last_name'] ?></h3>
                                <p class="mb-1"><?php echo $details[0]['city'] ?>
                                    , <?php echo $details[0]['district'] ?></p>
                                <small><?php echo $details[0]['human_date'] ?></small>
                            </div>

                            <div class="ext-prof-view-working-area">
                                <h5 class="mt-2 mb-2"><span>Working Area:</span><span
                                            class="ty"><?php echo ucwords(str_replace('_', ' ', $details[0]['working_area'])); ?></span>
                                </h5>
                                <h5 class="mt-2 mb-2"><span>Gender:</span><span
                                            class="ty"><?php echo ucwords(str_replace('_', ' ', $details[0]['gender'])); ?></span>
                                </h5>
                                <h5 class="mt-2 mb-2"><span>Age:</span><span
                                            class="ty"><?php echo $details[0]['age']; ?> Years</span>
                                </h5>
                                <h5 class="mt-2 mb-2"><span>Experience:</span><span
                                            class="ty"><?php echo floor($details[0]['expariance'] / 12); ?>
                                        Year(s)</span>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <a class="btn btn-success btn-block <?php echo ($user_type == 'member') ? '' : 'disabled' ?>"
                               href="<?php echo base_url() ?>service/book/<?php echo $details[0]['id'] ?>">Book Service
                                Now</a>

                            <?php echo ($user_type == 'member') ? '' : '<p class="text-center text-danger" style="font-size: 90%;padding-top: 5px;">You can not this service. Please Login as member</p>' ?>

                            <div class="ext-prof-view-contact border p-1 mt-4 mb-4">
                                <div class="mt-0">
                                    <div class="ul">
                                        <div class="li ext-prof-view-num-box">
                                    <span class="ext-prof-view-coimg"><img
                                                src="<?php echo base_url() ?>assets/img/icons/profile/call.png" alt=""></span>
                                            <a id="modal_contactno_btn" href="tel:<?php echo $details[0]['phone'] ?>">
                                                <span> <?php echo $details[0]['phone'] ?></span>
                                            </a>
                                        </div>
                                        <small class="pull-right p-2 text-muted">Call Now</small>
                                    </div>
                                </div>
                            </div>
                            <div class="ext-prof-view-days-hours">
                                <!-- working days -->
                                <div class="mb-4">
                                    <div class="d-flex align-items-center mb-2">
                                        <img src="<?php echo base_url() ?>assets/img/icons/profile/calendar.png" alt="">
                                        <h3>Working Days</h3>
                                    </div>

                                    <div>
								<span class="ty">
                                    <?php foreach (json_decode($details[0]['working_days']) as $day) { ?>
                                        <span class="badge-primary badge font-15"><?php echo ucwords($day); ?></span>
                                    <?php } ?>
                                </span>
                                    </div>
                                </div>

                                <!-- working hours -->
                                <?php if ($details[0]['time_to_call_from'] != '') { ?>
                                    <?php
                                    $date = new DateTime($details[0]['time_to_call_from']);
                                    $time_to_call_from = $date->format('h:i A');

                                    $to_date = new DateTime($details[0]['time_to_call_to']);
                                    ?>
                                    <div class="mb-4">
                                        <div class="d-flex align-items-center mb-2">
                                            <img src="<?php echo base_url() ?>assets/img/icons/profile/24-hours.png"
                                                 alt="">
                                            <h3>Best Time to Call</h3>
                                        </div>
                                        <div>
								<span class="ty">
											<?php echo $time_to_call_from ?><?php echo ($details[0]['time_to_call_to']) ? '- ' . $to_date->format('h:i A') : '' ?>
										</span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <!-- contact hours -->

                                <!-- Qualification and description for mobile devices -->
                                <div class="ext-prof-view-qual-desc-mobile">

                                </div>

                            </div>

                        </div>
                    </div>
                    <hr>
                    <!-- main skill -->
                    <div class="main_cat ext-prof-view-main-cat d-flex align-items-center">
                        <div class="cat_img">
                            <img src="<?php echo base_url() ?>assets/icons/<?php echo $details[0]['main_category_icon'] ?>"
                                 alt="<?php echo $details[0]['main_category_name'] ?>">
                        </div>
                        <div class="cat_spec">
                            <h6><?php echo $details[0]['main_category_name'] ?></h6>
                            <span><b><?php echo $details[0]['main_rate'] ?> LKR/hr</b></span>
                        </div>
                    </div>
                    <!-- other skills -->
                    <div class="ext-prof-view-sub-cat">
                        <?php if (count($details[0]['alternative_categories'])) { ?>
                            <small><b><u>Sub Category</u></b></small>
                            <div class="main_cat ext-prof-view-main-cat d-flex align-items-center">
                                <?php foreach ($details[0]['alternative_categories'] as $alt) { ?>
                                    <div class="cat_img_sub">
                                        <img src="<?php echo base_url() ?>assets/icons/<?php echo $alt['category_icon'] ?>"
                                             alt="<?php echo $alt['category_name'] ?>"
                                             title="<?php echo $alt['category_name'] ?>">
                                    </div>
                                    <div class="cat_spec_sub pr-3">
                                        <h6><?php echo $alt['category_name'] ?></h6>
                                        <span><b><?php echo $alt['rates'] ?> LKR/hr</b></span>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <hr>
                </div>

            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-md-7 border review-view">
                    <?php
                    if ($reviews) {

                        foreach ($reviews as $review) { ?>
                            <div class="one-review pt-2">
                                <div class="row" style="margin-bottom: 10px !important;">
                                    <div class="col-md-6">
                                        <p><?php echo $review['first_name'] ?><?php echo $review['last_name'] ?></p>
                                    </div>
                                    <div class="col-md-6 text-muted text-right">
                                        <small><?php echo $review['human_date'] ?></small>
                                    </div>
                                </div>
                                <div class="row text-success" title="<?php echo $review['value'] ?> Star Rating">
                                    <div class="col-md-12">
                                        <?php for ($x = 0; $x < $review['value']; $x++) { ?>
                                            <i class="fa fa-star"></i>
                                        <?php } ?>
                                        <?php for ($x = 0; $x < (5 - $review['value']); $x++) { ?>
                                            <i class="fa fa-star-o"></i>
                                        <?php } ?>

                                    </div>
                                </div>
                                <div class="row pt-2">
                                    <div class="col-md-12">
                                        <p><?php echo $review['description'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        <?php }
                    } else { ?>
                        <div class="one-review pt-2">
                            <div class="row" style="margin-bottom: 10px !important;">
                                <div class="col-md-12">
                                    <p>No reviews available</p>
                                </div>
                            </div>
                        </div>
                        <hr>
                    <?php } ?>
                </div>
                <div class="col-md-5">
                    <div class="card-body border text-center">
                        <i class="fa fa-pencil-square fa-3x text-success"></i>
                        <a href="#"><h4>Write Your Views</h4></a>
                        <small>share your experience/views about this Service</small>
                        <hr>
                        <div class="col-md-12">
                            <?php
                            $this->view('front/includes/message');
                            if ($this->ion_auth->logged_in()) {
                                $user = $this->ion_auth->get_Logged_user();
                                ?>
                                <?php if ($this->ion_auth->is_serviceprovider()) { ?>
                                    You are a Service Provider, You can not Rate Someone.
                                <?php }
                                if ($this->ion_auth->is_member()) {
                                    $rate = $this->ion_auth->getProvidedRate($details[0]['user_id'], $user['id']);
                                    if ($rate->num_rows()) {
                                        $rate_details = $rate->result_array();
                                        ?>
                                        <p class="text-muted">
                                            <small>
                                                Logged As
                                                : <?php echo $user['first_name'] ?> <?php echo $user['last_name'] ?>
                                                <a href="<?php echo base_url() ?>auth/logout">Logout</a>
                                            </small>
                                        </p>
                                        <form accept-charset="UTF-8" action="<?php echo base_url() ?>review/update"
                                              method="post">
                                            <input name="service_provider_rates_id" type="hidden"
                                                   value="<?php echo $rate_details[0]['id'] ?>">
                                            <input name="service_provider_id" type="hidden"
                                                   value="<?php echo $details[0]['id'] ?>">
                                            <textarea class="form-control animated" cols="50"
                                                      name="description" required
                                                      placeholder="Enter your review here..."
                                                      rows="5"><?php echo $rate_details[0]['description'] ?></textarea>

                                            <div class="text-right">
                                                <div class="rate mt-4">
                                                    <input type="radio" id="star5" name="rate"
                                                           value="5" <?php echo ($rate_details[0]['value'] == 5) ? 'checked' : '' ?>/>
                                                    <label for="star5" title="text">5 stars</label>
                                                    <input type="radio" id="star4" name="rate"
                                                           value="4" <?php echo ($rate_details[0]['value'] == 4) ? 'checked' : '' ?>/>
                                                    <label for="star4" title="text">4 stars</label>
                                                    <input type="radio" id="star3" name="rate"
                                                           value="3" <?php echo ($rate_details[0]['value'] == 3) ? 'checked' : '' ?>/>
                                                    <label for="star3" title="text">3 stars</label>
                                                    <input type="radio" id="star2" name="rate"
                                                           value="2" <?php echo ($rate_details[0]['value'] == 2) ? 'checked' : '' ?>/>
                                                    <label for="star2" title="text">2 stars</label>
                                                    <input type="radio" id="star1" name="rate"
                                                           value="1" <?php echo ($rate_details[0]['value'] == 1) ? 'checked' : '' ?>/>
                                                    <label for="star1" title="text">1 star</label>
                                                </div>
                                                <!--                                    <br>-->
                                                <div class="clearfix"></div>
                                                <button class="btn btn-success btn-sm" type="submit">Update</button>
                                            </div>
                                        </form>
                                    <?php } else { ?>
                                        <p class="text-muted">
                                            <small>
                                                Logged As
                                                : <?php echo $user['first_name'] ?> <?php echo $user['last_name'] ?>
                                                <a href="<?php echo base_url() ?>auth/logout">Logout</a>
                                            </small>
                                        </p>
                                        <form accept-charset="UTF-8" action="<?php echo base_url() ?>review/add"
                                              method="post">
                                            <input name="service_provider_id" type="hidden"
                                                   value="<?php echo $details[0]['id'] ?>">
                                            <input name="service_provider_user_id" type="hidden"
                                                   value="<?php echo $details[0]['user_id'] ?>">
                                            <input name="created_by" type="hidden" value="<?php echo $user['id'] ?>">
                                            <textarea class="form-control animated" cols="50" id="new-review"
                                                      name="description" required
                                                      placeholder="Enter your review here..." rows="5"></textarea>

                                            <div class="text-right">
                                                <div class="rate mt-4">
                                                    <input type="radio" id="star5" name="rate" value="5"/>
                                                    <label for="star5" title="text">5 stars</label>
                                                    <input type="radio" id="star4" name="rate" value="4"/>
                                                    <label for="star4" title="text">4 stars</label>
                                                    <input type="radio" id="star3" name="rate" value="3"/>
                                                    <label for="star3" title="text">3 stars</label>
                                                    <input type="radio" id="star2" name="rate" value="2"/>
                                                    <label for="star2" title="text">2 stars</label>
                                                    <input type="radio" id="star1" name="rate" value="1" checked/>
                                                    <label for="star1" title="text">1 star</label>
                                                </div>
                                                <!--                                    <br>-->
                                                <div class="clearfix"></div>
                                                <button class="btn btn-success btn-sm" type="submit">Save</button>
                                            </div>
                                        </form>
                                    <?php } ?>
                                <?php } ?>
                            <?php } else { ?>
                                <a href="<?php echo base_url() ?>auth/login?return_url=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>"
                                   class="btn btn-sm btn-block btn-info">Login</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

</section>
<br>
<br>
<br>

<?php $this->view('front/includes/footer') ?>
