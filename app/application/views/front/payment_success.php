<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav');

if ($this->ion_auth->logged_in()) {
    $user = $this->ion_auth->get_Logged_user();
}

?>


<div id="yellow_flame"></div>
<section class="outer main_view" style="min-height: 450px">
    <div class="container">
        <div class="space-3"></div>
        <?php if ($success != '') { ?>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 border-success border rounded pt-4 pb-4">
                <h3 class="text-success">
                    <i class="fa fa-check-circle"></i>
                   <?php echo $success; ?>
                </h3>

            </div>
        <?php }
        if ($error != '') {
            ?>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 border-danger border rounded pt-4 pb-4">
                <h3 class="text-danger">
                    <i class="fa fa-info-circle"></i>
                    <?php echo $error; ?>

                </h3>

            </div>
        <?php } ?>

        <div class="space-3"></div>

        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="border-info border rounded pl-4 pr-4 pb-2 pt-2">
                <p class="pt-2"><b><u> Order Summery:</u></b></p>
                <div class="ext-prof-view-working-area">
                    <h5 class="mt-2 mb-2"><span>Required Date : </span><span
                                class="ty"><?php echo $service_details[0]['required_date']; ?></span>
                    </h5>
                </div>
                <div class="ext-prof-view-working-area">
                    <h5 class="mt-2 mb-2"><span>Required Time : </span><span
                                class="ty"><?php echo $service_details[0]['required_time']; ?></span>
                    </h5>
                </div>
                <div class="ext-prof-view-working-area">
                    <h5 class="mt-2 mb-2"><span>Required Category : </span>
                        <?php foreach (json_decode($service_details[0]['category']) as $cat){ ?>
                            <span class="ty"><?php echo $cat ?> / </span>
                        <?php } ?>
                    </h5>
                </div>
                <div class="ext-prof-view-working-area">
                    <h5 class="mt-2 mb-2"><span>Work Background : </span><span
                                class="ty"><?php echo $service_details[0]['work_background']; ?></span>
                    </h5>
                </div>
                <div class="ext-prof-view-working-area">
                    <h5 class="mt-2 mb-2"><span>Requested Date : </span><span
                                class="ty"><?php echo $service_details[0]['created_date']; ?></span>
                    </h5>
                </div>
                <div class="ext-prof-view-working-area">
                    <h5 class="mt-2 mb-2"><span>Requested By : </span><span
                                class="ty"><?php echo $user['first_name']; ?> <?php echo $user['last_name']; ?></span>
                    </h5>
                </div>
                <div class="ext-prof-view-working-area">
                    <h5 class="mt-2 mb-2"><span>Phone number : </span><span
                                class="ty"><?php echo $user['phone']; ?></span>
                    </h5>
                </div>
                <div class="ext-prof-view-working-area">
                    <h5 class="mt-2 mb-2"><span>Phone number : </span><span
                                class="ty"><?php echo $service_details[0]['additional_phone']; ?></span>
                    </h5>
                </div>
            </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                <div class="border-warning border rounded pl-4 pr-4 pb-2 pt-2">
                    <p class="pt-2"><b><u> Payment Summery:</u></b></p>
                    <div class="ext-prof-view-working-area">
                        <h5 class="mt-2 mb-2"><span>Payment Done by : </span><span
                                    class="ty"><?php echo $payment_details[0]['first_name']; ?> <?php echo $payment_details[0]['last_name']; ?></span>
                        </h5>
                    </div>
                    <div class="ext-prof-view-working-area">
                        <h5 class="mt-2 mb-2"><span>Amount : </span><span
                                    class="ty"><?php echo $payment_details[0]['amount']; ?> LKR</span>
                        </h5>
                    </div>

                    <div class="ext-prof-view-working-area">
                        <h5 class="mt-2 mb-2"><span>Reason : </span><span
                                    class="ty"><?php echo $payment_details[0]['reason']; ?></span>
                        </h5>
                    </div>
                    <div class="ext-prof-view-working-area">
                        <h5 class="mt-2 mb-2"><span>Payment done on : </span><span
                                    class="ty"><?php echo $payment_details[0]['created_date']; ?></span>
                        </h5>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>
<br>
<br>
<br>
<?php $this->view('front/includes/footer') ?>
