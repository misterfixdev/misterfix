<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav');
$user_type = 'member';
if ($this->ion_auth->logged_in()) {
    $user = $this->ion_auth->get_Logged_user();
    $user_type = $user['user_type'];
}
?>

<div id="yellow_flame"></div>
<section class="outer main_view">
    <div class="container">
        <div class="space-3"></div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-12 text-left left_filters ext-main_view-left-filters">
                    <!--  //category part start -->
                    <div class="all_cat">
                        <?php
                        $district = '';
                        $city = '';

                        if (!is_null($this->input->get('district'))) {
                            $district = '?district=' . $this->input->get('district');
                        }

                        if (!is_null($this->input->get('city'))) {
                            $city = '&&city=' . $this->input->get('city');
                        }
                        $all_url = base_url() . 'service/all' . $district . $city;

                        ?>
                        <a href="<?php echo $all_url; ?>"><h5>All Categories</h5></a>


                        <div class="all_cat_inr">
                            <ul>
                            </ul>


                            <ul class="expandible">
                                <?php
                                $cate = [];
                                foreach ($categories as $category) { ?>
                                    <?php
                                    $url = base_url() . 'service/all';
                                    $district = '';
                                    $city = '';
                                    $page = '';
                                    $page = ($this->input->get('page')) ? $this->input->get('page') : 0;

                                    if (!is_null($this->input->get('district'))) {
                                        $district = '?district=' . $this->input->get('district');
                                    }

                                    if (!is_null($this->input->get('city'))) {
                                        $city = '&&city=' . $this->input->get('city');
                                    }

                                    if (!is_null($this->input->get('district')) || !is_null($this->input->get('city'))) {
                                        $cat = '&&category=' . $category['id'];
                                    } else {
                                        $cat = '?category=' . $category['id'];
                                    }


                                    $url = $url . $district . $city . $cat;

                                    if (!is_null($this->input->get('category'))) {
                                        if ($category['id'] == $this->input->get('category')) {
                                            $cate = $category;
//                                            var_dump($cate);
                                        }
                                    }
                                    ?>
                                    <li class="sin_cat <?php echo ($this->input->get('category') == $category['id']) ? 'activeurl' : '' ?>">
                                        <a href="<?php echo $url; ?>"
                                           class="all_cat_list lode_goox">
                                            <div class="cat_img"><img
                                                        src="<?php echo base_url() ?>assets/icons/<?php echo $category['icon'] ?>"
                                                        alt="<?php echo $category['name'] ?>"></div>

                                            <p class="cat_name"><?php echo $category['name'] ?></p>
                                            <div class="circle"></div>
                                        </a>
                                    </li>
                                <?php } ?>

                            </ul>

                        </div>

                    </div>
                    <!--  //category part end -->
                </div>

                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12 cbn_item_outer ext-main-ads-mobile-res">
                    <div class="row border pb-4 pt-4 rounded">
                        <div class="col-md-5">
                            <div class="place_drops">

                                <div class="dis">
                                    <!--                                    <label>District</label>-->
                                    <select class="ui search dropdown" id="txtDistrict">
                                        <option value="" selected disabled>Select District</option>
                                        <option label="All Sri Lanka" value="srilanka">All Sri Lanka</option>
                                        <?php
                                        $dis = [];
                                        foreach ($districts as $district) {

                                            if (!is_null($this->input->get('district'))) {
                                                if ($district['id'] == $this->input->get('district')) {
                                                    $dis = $district;
                                                }
                                            }

                                            ?>
                                            <option value="<?php echo $district['id'] ?>"
                                                    label="<?php echo $district['name'] ?>" <?php echo ($this->input->get('district') == $district['id']) ? 'selected' : '' ?>><?php echo $district['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="place_drops">

                                <div class="cit">
                                    <!--                                    <label>City</label>-->
                                    <select class="ui search dropdown" id="txtCity">
                                        <option value="" selected disabled>Select City</option>
                                        <?php
                                        $cit = [];
                                        if (count($cities)) {
                                            foreach ($cities as $city) {

                                                if (!is_null($this->input->get('city'))) {
                                                    if ($city['id'] == $this->input->get('city')) {
                                                        $cit = $city;
                                                    }
                                                }

                                                ?>
                                                <option value="<?php echo $city['id'] ?>"
                                                        label="<?php echo $city['name'] ?>" <?php echo ($this->input->get('city') == $city['id']) ? 'selected' : '' ?>><?php echo $city['name'] ?></option>
                                            <?php }
                                        } ?>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2" style="padding-top: 10px;">
                            <a href="<?php echo base_url() ?>service/all"><i class="fa fa-history"></i> Reset
                                Filters</a>
                        </div>
                    </div>

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 search_resu">

                        <!-- <p class="se">Your Search Returned <span></span> Results</p> -->
                        <p class="co_cbn">
                            Showing <?php echo count($service_providers) ?> of
                            <span><?php echo $all_records_count ?></span> Services </p>

                    </div>
                    <hr>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 bread lode_goox" id="crumbs">
                        <ul>
                            <li><a href="<?php echo base_url() ?>service/all"><i class="fa fa-home"
                                                                                 aria-hidden="true"></i></a>
                            </li>
                            <?php if (!is_null($this->input->get('district')) && $this->input->get('district') != 'srilanka') {
//                                var_dump('adss');
                                echo '<li><a href="' . base_url() . 'service/all?district=' . $dis['id'] . '">' . $dis['name'] . '</a></li>';
                            } else {
                                echo '<li><a href="' . base_url() . 'service/all">All Sri Lanka</a></li>';
                            }

                            if (!is_null($this->input->get('city'))) {
                                echo '<li><a href="' . base_url() . 'service/all?district=' . $dis['id'] . '&city=' . $cit['id'] . '">' . $cit['name'] . '</a></li>';
                            }

                            if (!is_null($this->input->get('category'))) {
                                echo '<li><a href="' . base_url() . 'service/all?category=' . $cate['id'] . '">' . $cate['name'] . '</a></li>';
                            }
                            ?>
                            <li><a href="<?php echo base_url() ?>service/all">Services</a></li>
                        </ul>
                    </div>

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 cbn_item_inr mt-3">
                        <div class="row">


                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 qwert_block">
                                <?php if (count($service_providers) > 0) {

                                    foreach ($service_providers as $service_provider) {
                                        ?>

                                        <div class="sin_block mt-2 mb-2">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="prof_img"
                                                         style="background-image: url(<?php echo base_url() ?>assets/img/profile_images/<?php echo ($service_provider['profile_image'] != null) ? $service_provider['profile_image'] : 'default.png' ?>)"></div>

                                                    <div class="sub-row text-warning" style="font-size: 15px">
                                                        <div class="star-ratings-sprite"
                                                             title="<?php echo $service_provider['rating']['rate']; ?> Star Rating">
                                                            <span style="width:<?php echo ($service_provider['rating']['rate'] / 5) * 100; ?>%"
                                                                  class="star-ratings-sprite-rating"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="detail">
                                                        <p class="nm"><?php echo $service_provider['first_name'] ?><?php echo $service_provider['last_name'] ?></p>
                                                        <div class="pl">
                                                            <div class="ic wk-map-marker"><i
                                                                        class="fa fa-map-marker"></i>
                                                            </div>
                                                            <p><?php echo $service_provider['city'] ?>
                                                                , <?php echo $service_provider['district'] ?></p>
                                                        </div>
                                                        <small class="pull-left text-muted"><?php echo $service_provider['human_date'] ?></small>
                                                        <br>
                                                        <?php $experiance = floor($service_provider['expariance'] / 12); ?>
                                                        <?php if ($experiance > 1) { ?>
                                                            <span class="pull-left text-success"><i
                                                                        class="fa fa-briefcase"></i>&nbsp;&nbsp;<?php echo $experiance ?>
                                                                year(s) experience</span>
                                                            <br>
                                                        <?php } ?>
                                                        <?php if ($service_provider['time_to_call_from'] != '') { ?>
                                                            <?php
                                                            $date = new DateTime($service_provider['time_to_call_from']);
                                                            $time_to_call_from = $date->format('h:i A');

                                                            $to_date = new DateTime($service_provider['time_to_call_to']);
                                                            ?>
                                                            <span class="pull-left text-info mt-2"
                                                                  title="Best time to call : <?php echo $time_to_call_from ?> <?php echo ($service_provider['time_to_call_to']) ? '- ' . $to_date->format('h:i A') : '' ?>"><i
                                                                        class="fa fa-phone"></i>&nbsp;&nbsp;<?php echo $time_to_call_from ?> <?php echo ($service_provider['time_to_call_to']) ? '- ' . $to_date->format('h:i A') : '' ?></span>
                                                        <?php } ?>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <fieldset class="scheduler-border p-0">
                                                        <legend class="scheduler-border">Category</legend>
                                                        <div class="detail">
                                                            <div class="main_cat">
                                                                <div class="cat_img">
                                                                    <img src="<?php echo base_url() ?>assets/icons/<?php echo $service_provider['main_category_icon'] ?>"
                                                                         alt="<?php echo $service_provider['main_category_name'] ?>">
                                                                </div>
                                                                <p><?php echo $service_provider['main_category_name'] ?></p>
                                                            </div>
                                                            <?php if (count($service_provider['alternative_categories'])) { ?>
                                                                <div class="o_wrk">
                                                                    <ul>
                                                                        <?php foreach ($service_provider['alternative_categories'] as $alt) { ?>
                                                                            <li class="border p-1 text-center">
                                                                                <img src="<?php echo base_url() ?>assets/icons/<?php echo $alt['category_icon'] ?>"
                                                                                     alt="<?php echo $alt['category_name'] ?>"
                                                                                     title="<?php echo $alt['category_name'] ?>">
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                            <?php } ?>

                                                        </div>
                                                    </fieldset>

                                                </div>
                                                <div class="col-md-2">
                                                    <a href="<?php echo base_url() ?>service/view/<?php echo $service_provider['id'] ?>"
                                                       class="btn-info btn btn-block btn-sm">View</a>

                                                    <a href="<?php echo base_url() ?>service/book/<?php echo $service_provider['id'] ?>"
                                                       class="btn-primary btn btn-block btn-sm <?php echo ($user_type == 'member') ? '' : 'disabled' ?>">Book
                                                        Now</a>

                                                    <?php echo ($user_type == 'member') ? '' : '<p class="text-center text-danger" style="font-size: 66%;padding-top: 5px;">Please Login as member</p>' ?>

                                                </div>
                                            </div>

                                        </div>

                                    <?php }
                                } else { ?>
                                    <div class="sin_block mt-2 mb-2">
                                        <div class="row">
                                            <div class="col-md-12">
                                                No records Found
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>


                        </div>
                    </div>

                    <br>
                    <!--                    pagination-->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php echo $links; ?>
                    </div>
                    <br>


                </div>

            </div>
        </div>


    </div>

</section>


<?php $this->view('front/includes/footer') ?>
