<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav');

if ($this->ion_auth->logged_in()) {
    $user = $this->ion_auth->get_Logged_user();
}

?>


<div id="yellow_flame"></div>
<section class="outer main_view" style="min-height: 450px">
    <div class="container">
        <div class="space-3"></div>
        <h1 class="text-center">
            Admin approval pending.

        </h1>
        <p class="text-center">
            <br>
            <br>
            <?php echo anchor("welcome/unverified", 'Check Now', ['class' => 'btn btn-success btn-lg']) ?>

        </p>

    </div>

</section>
<br>
<br>
<br>
<?php $this->view('front/includes/footer') ?>
