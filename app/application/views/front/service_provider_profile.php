<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav') ?>
<style>
    .user-row {
        margin-bottom: 14px;
    }

    .user-row:last-child {
        margin-bottom: 0;
    }

    .dropdown-user {
        margin: 13px 0;
        padding: 5px;
        height: 100%;
    }

    .dropdown-user:hover {
        cursor: pointer;
    }

    .table-user-information > tbody > tr {
        border-top: 1px solid rgb(221, 221, 221);
    }

    .table-user-information > tbody > tr:first-child {
        border-top: 0;
    }

    .table-user-information > tbody > tr > td {
        border-top: 0;
    }

    .toppad {
        margin-top: 20px;
    }

    #overlay {
        position: fixed;
        display: block;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.8);
        z-index: 2;
        cursor: pointer;
    }

    #text{
        position: absolute;
        top: 50%;
        left: 50%;
        font-size: 50px;
        color: white;
        transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
    }
</style>
<div id="yellow_flame"></div>
<?php
if ($this->ion_auth->logged_in()) {

    $user = $this->ion_auth->get_Logged_user();
    $step = $user['step'];
} else {
    header('location:' . base_url());
}

?>

<?php
if ($user['is_completed'] == 0) {
    $this->view('front/includes/service_provider_step', array('step' => $step));
}
$this->view('front/includes/message');
?>
<br>
<div class="container border">
    <!-- <div class="space-3 lo"></div> -->
    <?php if ($user['is_completed'] == 0) { ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-pg">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php if ($step == 2) { ?>
                        <!-- Details add View-->
                        <div class="row">
                            <form action="<?php echo base_url() ?>serviceprovider/details/save" class="col-md-12"
                                  method="post" enctype="multipart/form-data">
                                <input type="hidden" value="<?php echo $user['id'] ?>" name="user_id">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" class="form-control" id=""
                                                   value="<?php echo $user['first_name'] ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" class="form-control" id=""
                                                   value="<?php echo $user['last_name'] ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Date of Birth</label>
                                            <input type="date" class="form-control" id=""
                                                   value="" name="dob">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Location</label>
                                            <select class="form-control select2select" name="location">
                                                <option value="" selected disabled>Select Location</option>
                                                <?php
                                                if (count($cities)) {
                                                    foreach ($cities as $city) { ?>
                                                        <option value="<?php echo $city['id'] ?>"
                                                                label="<?php echo $city['name'] ?>" <?php echo ($this->input->get('city') == $city['id']) ? 'selected' : '' ?>><?php echo $city['name'] ?></option>
                                                    <?php }
                                                } ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Gender</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gender"
                                                       id="exampleRadios1"
                                                       value="male" checked>
                                                <label class="form-check-label" for="exampleRadios1">
                                                    Male
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gender"
                                                       id="exampleRadios2"
                                                       value="female">
                                                <label class="form-check-label" for="exampleRadios2">
                                                    Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">Experience</label>
                                        <div class="input-group mb-3">
                                            <input type="number" class="form-control" id=""
                                                   value="0" name="experience" max="200" min="0"
                                                   aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">Months</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Best Time To call</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">From</span>
                                            </div>
                                            <input type="time" class="form-control" id=""
                                                   name="time_from">
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">To</span>
                                            </div>
                                            <input type="time" class="form-control" id=""
                                                   name="time_to">
                                        </div>
                                        <div class="input-group mb-3">

                                        </div>
                                    </div>
                                </div>

                                <div class="row ">
                                    <div class="col-md-6 border pt-2">
                                        <div class="form-group">
                                            <label for="">Main Service Type</label><br>
                                            <select type="text" class="form-control multiselect multiselect-icon"
                                                    role="multiselect" name="main_service" id="main_service">
                                                <option value="0">Select Main Service type</option>
                                                <?php
                                                if (count($categories)) {
                                                    foreach ($categories as $category) { ?>
                                                        <option value="<?php echo $category['id'] ?>"
                                                                data-icon="<?php echo base_url() ?>assets/icons/<?php echo $category['icon'] ?>"
                                                                label="<?php echo $category['name'] ?>"><?php echo $category['name'] ?></option>
                                                    <?php }
                                                } ?>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-6 border pt-2">
                                        <div class="form-group">
                                            <label for="">Alternative Service Type</label><br>
                                            <select type="text" class="form-control multiselect multiselect-icon"
                                                    multiple="multiple" role="multiselect" name="alternative_services[]"
                                                    id="alternative_service">
                                                <?php
                                                if (count($categories)) {
                                                    foreach ($categories as $category) { ?>
                                                        <option value="<?php echo $category['id'] ?>"
                                                                data-icon="<?php echo base_url() ?>assets/icons/<?php echo $category['icon'] ?>"
                                                                label="<?php echo $category['name'] ?>"><?php echo $category['name'] ?></option>
                                                    <?php }
                                                } ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <?php $days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'); ?>
                                <div class="row">
                                    <div class="col-md-12 border pt-2 pb-3">
                                        <label for="">Working Days</label>
                                        <div class="form-group">
                                            <?php foreach ($days as $day) { ?>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="days[]"
                                                           id="exampleRadios1" value="<?php echo strtolower($day); ?>">
                                                    <label class="form-check-label" for="exampleRadios1">
                                                        <?php echo $day; ?>
                                                    </label>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 border pt-2 pb-3">
                                        <label for=""><b>Attach NIC copy / Police Report</b></label>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Document Type</label>
                                                <div class="form-group">
                                                    <select name="doc_type" id="" class="form-control" required>
                                                        <option value="nic">NIC</option>
                                                        <option value="police_report">Police Report</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="">Attach Document <small class="text-info">jpg, png, pdf only | Max size 5MB</small></label>
                                                <div class="input-group mb-3">
                                                    <input type="file" class="form-control" name="file" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <button type="submit" class="btn btn-success pull-right">Next >></button>
                            </form>
                        </div>
                        <!-- END Details add View-->

                    <?php } elseif ($step == 3) { ?>
                        <!-- Rates add View-->

                        <div class="row">
                            <form action="<?php echo base_url(); ?>serviceprovider/rates/save" class="col-md-12"
                                  method="post">
                                <input type="hidden" name="user_id" value="<?php echo $user['id'] ?>">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">
                                                <small>Main Category</small>
                                            </label>
                                            <h4 class="m-0">
                                                <img src="<?php echo base_url() ?>assets/icons/<?php echo $service_provider[0]['main_category_icon'] ?>"
                                                     alt="" style="width: 40px">
                                                <?php echo $service_provider[0]['main_category_name'] ?></h4>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">
                                                <small>Rate</small>
                                            </label>
                                            <input type="number" class="form-control" id="" name="main_cat"
                                                   value="<?php echo $service_provider[0]['rate'] ?>">
                                        </div>
                                    </div>

                                </div>
                                <?php foreach ($service_provider_categories as $service_provider_category) { ?>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">
                                                    <small>Sub Category</small>
                                                </label>
                                                <h4 class="m-0">
                                                    <img src="<?php echo base_url() ?>assets/icons/<?php echo $service_provider_category['main_category_icon'] ?>"
                                                         alt="" style="width: 40px">
                                                    <?php echo $service_provider_category['main_category_name'] ?></h4>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">
                                                    <small>Rate</small>
                                                </label>
                                                <input type="number" class="form-control" id="" required
                                                       name="input_<?php echo $service_provider_category['id'] ?>"
                                                       value="<?php echo $service_provider_category['rate'] ?>">
                                            </div>
                                        </div>

                                    </div>
                                <?php } ?>
                                <button type="submit" class="btn btn-success pull-right">Next >></button>
                            </form>
                        </div>
                        <!-- END Rates add View-->

                    <?php } elseif ($step == 4) { ?>
                        <!-- Payment View-->

                        <div class="row">
                            <form action="https://sandbox.payhere.lk/pay/checkout" class="col-md-12"
                                  method="post">

                                <input type="hidden" name="merchant_id" value="1213792">
                                <!-- Replace your Merchant ID -->
                                <input type="hidden" name="return_url"
                                       value="<?php echo base_url() ?>user/payment">
                                <input type="hidden" name="cancel_url"
                                       value="<?php echo base_url() ?>user/cancel">
                                <input type="hidden" name="notify_url"
                                       value="<?php echo base_url() ?>user/noti">
                                <input type="hidden" name="order_id" value="<?php echo $user['id'] ?>">
                                <input type="hidden" name="country" value="Sri Lanka">
                                <input type="hidden" name="address"
                                       value="<?php echo $user['phone'] ?>">
                                <br><br>
                                <div class="row">
                                    <div class="col-md">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" class="form-control" id="" name="first_name"
                                                   value="<?php echo $user['first_name'] ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" class="form-control" id="" name="last_name"
                                                   value="<?php echo $user['last_name'] ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="text" class="form-control" id="" name="email"
                                                   value="<?php echo $user['email'] ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md">
                                        <div class="form-group">
                                            <label for="">Phone</label>
                                            <input type="text" class="form-control" id="" name="phone"
                                                   value="<?php echo $user['phone'] ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md" style="display:none;">
                                        <div class="form-group">
                                            <label for="">City</label>
                                            <input type="text" class="form-control" id="" name="city"
                                                   value="sri lanka" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-6 border pt-2">
                                        <div class="form-group">
                                            <label for="">Description</label><br>
                                            <input type="text" class="form-control" name="items" readonly
                                                   value="SP Reg Fee"><br>

                                        </div>
                                    </div>
                                    <div class="col-md-6 border pt-2">
                                        <div class="form-group">
                                            <label for="">Amount</label><br>
                                            <input type="hidden" name="currency" value="LKR">
                                            <input type="text" class="form-control" id="" name="amount"
                                                   value="1000" readonly>
                                        </div>
                                    </div>

                                </div>

                                <button type="submit" class="btn btn-success pull-right">Next >></button>
                            </form>

                        </div>
                        <!-- END Payment View-->

                    <?php } ?>

                </div>
            </div>
        </div>
    <?php } else { ?>
        <!--        user account view-->
        <div class="row <?php echo (!$user['admin_verify'])? 'unverified':'' ?>">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 toppad ">
                <?php if (!$user['admin_verify']){ ?>
                <div id="overlay">
                    <div id="text">Pending Admin Approval</div>
                </div>
                <?php } ?>
                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center">
                                <img alt="User Pic"
                                     src="<?php echo base_url() ?>assets/img/profile_images/<?php echo ($service_provider[0]['profile_image'] != null) ? $service_provider[0]['profile_image'] : 'default.png' ?>"
                                     class="img-circle img-responsive">
                                <button class="btn btn-info btn-block btn-sm" data-toggle="modal"
                                        data-target="#exampleModal">Change Profile Image
                                </button>
                            </div>
                            <div class=" col-md-9 col-lg-9 ">
                                <h3 class="panel-title"><?php echo $user['first_name'] ?><?php echo $user['last_name'] ?></h3>
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td class="text-right">
                                            <a href="<?php echo base_url() ?>serviceprovider/details/edit">Edit</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Location:</td>
                                        <td><?php echo $service_provider[0]['city'] ?>
                                            - <?php echo $service_provider[0]['district'] ?></td>
                                    </tr>
                                    <tr>
                                        <?php $experiance = floor($service_provider[0]['expariance'] / 12); ?>
                                        <td>Experience:</td>
                                        <td><?php echo $experiance ?> Year(s)</td>
                                    </tr>
                                    <?php if ($service_provider[0]['time_to_call_from'] != '') { ?>
                                        <?php
                                        $date = new DateTime($service_provider[0]['time_to_call_from']);
                                        $time_to_call_from = $date->format('h:i A');

                                        $to_date = new DateTime($service_provider[0]['time_to_call_to']);
                                    }
                                    ?>
                                    <tr>
                                        <td>Date Of Birth:</td>
                                        <td><?php echo $service_provider[0]['dob'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Age:</td>
                                        <td><?php echo $service_provider[0]['age'] ?> Year(s)</td>
                                    </tr>
                                    <tr>
                                        <td>Gender:</td>
                                        <td><?php echo ucfirst($service_provider[0]['gender']) ?></td>
                                    </tr>
                                    <tr>
                                        <td>Description:</td>
                                        <td><?php echo ($service_provider[0]['description'] != '')?$service_provider[0]['description']:'---' ?></td>
                                    </tr>

                                </table>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 border pt-4 pb-1">
                                        <b><u>Category & Rate</u></b>
                                        <a href="<?php echo base_url() ?>serviceprovider/category/edit">Edit</a>
                                        <br>
                                        <br>
                                        <table class="table table-user-information">
                                            <tr class="bg-light">
                                                <td class=""><?php echo $service_provider[0]['main_category_name'] ?> <span class="badge badge-primary"><Small>Main Category</Small></span></td>
                                                <td>LKR. <?php echo $service_provider[0]['rate'] ?></td>
                                            </tr>
                                            <?php foreach ($service_provider_categories as $item) { ?>
                                                <tr>
                                                    <td class=""><?php echo $item['main_category_name'] ?></td>
                                                    <td>LKR. <?php echo $item['rate'] ?></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                    <div class="col-md-6 border pt-4 pb-1">
                                        <b><u>Working Days</u></b>
                                        <br>
                                        <br>
                                        <table class="table table-user-information">
                                            <?php foreach (json_decode($service_provider[0]['working_days']) as $item) { ?>
                                                <tr>
                                                    <td class=""><?php echo ucwords($item) ?></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>

                                <a href="<?php echo base_url() ?>auth/change_password" class="btn btn-warning">Change
                                    Password</a>
<!--                                <a href="#" class="btn btn-primary">Team Sales Performance</a>-->
                            </div>
                        </div>
                    </div>
<!--                    <div class="panel-footer">-->
<!--                        <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button"-->
<!--                           class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>-->
<!--                        <span class="pull-right">-->
<!--                            <a href="edit.html" data-original-title="Edit this user" data-toggle="tooltip" type="button"-->
<!--                               class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>-->
<!--                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button"-->
<!--                               class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>-->
<!--                        </span>-->
<!--                    </div>-->

                </div>
            </div>
        </div>
        <!--       END user account view-->

    <?php } ?>
</div>
<br>
<br>

<!--profile image change Modal-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Profile Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo base_url() ?>user/uploadimage" enctype="multipart/form-data">
                    <p class="text-danger">
                        <small>Max File Size - 2MB | Allowed only JPG and PNG</small>
                    </p>
                    <input type="hidden" name="user_id" value="<?php echo $user['id'] ?>">
                    <input type='file' id="imgInp" required name="profile_pic"/>
                    <img id="blah" src="#" alt="your image" style="display: none" class="p-3"/>
                    <button type="submit" class="btn btn-sm btn-success pull-right">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!--END profile image change Modal-->
<?php $this->view('front/includes/footer') ?>
