<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav') ?>

<section id="slider_cat" class="home_page">
    <div class="overlay"></div>
    <div class="container-fluid">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 horiz_cat">
            <ul>
                <?php foreach ($categories as $category) { ?>
                    <li class="sin_cat">
                        <a class="lode_goox" href="<?php echo base_url() ?>service/all?category=<?php echo $category['id'] ?>">
                            <div class="cat_img"><img
                                        src="<?php echo base_url() ?>assets/icons/<?php echo $category['icon'] ?>"
                                        alt="<?php echo $category['name'] ?>">
                            </div>

                            <p class="cat_name"><?php echo $category['name'] ?></p>
                            <!-- <span class="num_ads">(12,800)</span> -->
                        </a>
                    </li>
                <?php } ?>

            </ul>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 quick_link text-center">
                <a href="<?php base_url() ?>service/all" class="">All Services</a>
            </div>
        </div>
        <div class="space-1"></div>
    </div>
</section>

<section id="mobile_info">
    <div class="container">
        <!-- <div class="space-5"></div> -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
            <h4>Be A Service Provider</h4>
            <h1>Largest Workers Collection In Sri Lanka</h1>
            <a href="<?php base_url() ?>auth/create_user?user_type=service_provider" class="btn btn-primary btn-lg">Join With Us</a>
            <br><br>

            <br><br>
        </div>
        <!-- <div class="space-3"></div> -->
    </div>
</section>

<section id="add_service">
    <div class="container">
        <!-- <div class="space-5"></div> -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h1>Largest Workers Collection In<br> <span>Sri Lanka</span></h1>
            <a href="<?php base_url() ?>service/all" class="">All Services</a>
        </div>
        <!-- <div class="space-3"></div> -->
    </div>
</section>
<?php $this->view('front/includes/footer') ?>
