<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container">
        <a class="navbar-brand lode_goox" href="<?php echo base_url(); ?>"><img
                    src="<?php echo base_url(); ?>assets/img/logo-bl-theme.png"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item  ">
                    <a class="nav-link all lode_goox" href="<?php echo base_url() ?>">Home</a>
                </li>
                <li class="nav-item  ">
                    <a class="nav-link all lode_goox" href="<?php echo base_url() ?>service/all">All
                        Services</a>
                </li>
                <?php
                if (!$this->ion_auth->logged_in()) { ?>
                    <li class="nav-item">
                        <a class="nav-link book lode_goox" href="<?php echo base_url() ?>auth"><i
                                    class="fas fa-sign-in-alt"></i>&nbsp;Login</a>
                    </li>
                <?php } else {
                    $user = $this->ion_auth->get_Logged_user();
                    ?>
                    <li class="nav-item">
                        <div class="dropdown">
                            <button class="btn btn-info dropdown-toggle ml-3 mr-1 btn-sm " type="button"
                                    id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo $user['first_name'] . ' ' . $user['last_name'] ?>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                <?php if ($this->ion_auth->is_serviceprovider()) { ?>
                                    <a class="dropdown-item" href="<?php echo base_url() ?>serviceprovider/account">View
                                        Account</a>
                                    <a class="dropdown-item" href="<?php echo base_url() ?>my-account/orders">View
                                        Requests</a>
                                <?php } elseif ($this->ion_auth->is_member()) { ?>
                                    <a class="dropdown-item" href="<?php echo base_url() ?>my-account/orders">My
                                        Orders</a>

                                <?php } ?>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo base_url() ?>auth/logout">Logout</a>
                            </div>
                        </div>
                    </li>

                    <li class="nav-item">

                        <?php if ($user['user_type'] == 'service_provider') { ?>
                            <div class="collapse navbar-collapse" id="">

                                <ul class="navbar-nav ml-auto nav-flex-icons">
                                    <li class="nav-item avatar">
                                        <?php
                                        $count = $this->ion_auth->get_pending_requests();
                                        ?>
                                        <a class="nav-link waves-effect waves-light"
                                           href="<?php echo base_url() ?>my-account/orders?state=0"
                                           style="position: relative;" title="<?php echo $count; ?> Pending request(s)">
                                            <i class="fa fa-history text-primary" style="font-size: 22px;"></i>
                                            <?php
                                            if ($count) {
                                                ?>
                                                <span class="badge badge-danger"
                                                      style="position: absolute; left: 27px; top: 1px;">
                                            <?php echo $count ?>
                                        </span>
                                            <?php }
                                            ?>

                                        </a>
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>
                    </li>

                    <li class="nav-item">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown navbar-nav">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false" style="padding: 10px;" onclick="readNoti(<?php echo $this->ion_auth->get_user_id() ?>)">
                                <?php if (count($this->ion_auth->get_notifications())){ ?>
                                    <span class="badge badge-danger ml-2"
                                      style="position: absolute; left: 27px; top: 1px;"><?php echo count($this->ion_auth->get_notifications()); ?></span>
                                      <?php } ?>
                                    <i class="fa fa-bell text-info" style="font-size: 22px;"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-cart" role="menu">
                                    <?php if (count($this->ion_auth->get_notifications())) { ?>
                                        <?php foreach ($this->ion_auth->get_notifications() as $item) { ?>
                                            <li>
                                                <div class="item" style="border-top: 1px solid #eee;">
                                                    <div class="item-left">
                                                        <div class="item-info">
                                                            <span><?php echo $this->ion_auth->limit_text($item['description'], 50); ?></span>
                                                            <p class="text-right text-muted"><?php echo $this->ion_auth->timeAgo($item['created_at']); ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    <?php }else{ ?>
                                        <li>
                                            <div class="item">
                                                <div class="item-left">
                                                    <div class="item-info">
                                                        <span>No New Notifications</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </li>
                        </ul>
                    </li>
                <?php } ?>


            </ul>


        </div>
    </div>
</nav>

<?php
//var_dump($this->ion_auth->logged_in());
//var_dump($this->ion_auth->is_admin());
//var_dump($this->ion_auth->is_member());
//var_dump($this->ion_auth->is_serviceprovider());
//$ss = $this->ion_auth->get_Logged_user();
//
//var_dump($ss);
//var_dump($ss['first_name'] . ' ' . $ss['last_name']);
////var_dump();
////var_dump($this->ion_auth->is_admin());
//die();

//if (!$this->ion_auth->logged_in() || !)
//		{
//			redirect('auth', 'refresh');
//		}
?>