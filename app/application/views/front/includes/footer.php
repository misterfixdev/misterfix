<section id="footer">
    <div class="container">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 ftr_link">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-6 text-left">
                    <p>Links</p>
                    <div class="hr"></div>
                    <ul>
                        <li>
                            <a href="<?php echo base_url() ?>auth/create_user">Register Member</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>auth/create_user?user_type=service_provider">Register
                                Service Provider</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>service/all">All Services</a>
                        </li>
                        <li>
                            <a href="#">How to sell in MISTER FIX</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-6 text-left">
                    <p>Links</p>
                    <div class="hr"></div>
                    <ul>
                        <li>
                            <a href="<?php echo base_url() ?>appointment/create">Make an appointment with Mr.FIX</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-6 text-left">
                    <p>Links</p>
                    <div class="hr"></div>
                    <ul>

                    </ul>
                </div>

            </div>
        </div>
    </div>

    <!-- <div class="all_page_loder" > -->
    <!-- <img class="all_page_loder_img" src="../assets/img/main_loder.gif"> -->
    <!-- </div> -->

    <div class="footer-lower text-center">
        <div class="container">
            <div class="media-container-row mbr-white">
                <div class="copyright">
                    <p class="">
                        © Copyright 2020 Mister Fix - All Rights Reserved </a>
                    </p>
                </div>

            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<!-- Jquery -->
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>assets/semantic/semantic.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.twbsPagination.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<script src='https://github.com/davidstutz/bootstrap-multiselect'></script>
<script src='<?php echo base_url() ?>assets/mulitpleselect/script.js'></script>


<script>
    $('#txtDistrict, #txtCity, .select2select').select2();

    var site_url = "<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>


<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
    var dates = ["11/03/2020", "12/03/2020", "13/03/2020", "14/03/2020"];

    function DisableDates(date) {
        var string = jQuery.datepicker.formatDate('dd/mm/yy', date);
        return [dates.indexOf(string) == -1];
    }

    $(function () {
        // console.log(alldaynums);
        $(".date").datepicker({
            beforeShowDay: function (day) {
                var day = day.getDay();
                if (alldaynums.includes(day.toString())) {
                    return [false, "somecssclass"];
                } else {
                    return [true, "someothercssclass"];
                }

            },
            minDate: 0
        });
    });

    function dis(monday) {
        var cal = monday.getDay();
        return [(cal > 1)]
    }

    // console.log(alldaynums.includes('1'));

    $('.timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 30,
        minTime: '4',
        maxTime: '9:00pm',
        defaultTime: '9',
        startTime: '04:00am',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    function readNoti(user_id) {
        // alert(user_id);
        $.ajax({
            url: site_url + "noti/read",
            type: 'post',
            data: {user_id: user_id},
            success: function (res) {
                if (res == 1) {
                    window.location.reload();
                }
            }
        });
    }
</script>
</body>

</html>