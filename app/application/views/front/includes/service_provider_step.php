<section class="outer main_view">
    <div class="container">
        <div class="mt-5 mb-5 text-center">
            <h2>Service Provider Registration</h2>
        </div>
        <ul class="step d-flex flex-nowrap">
            <li class="step-item">
                <a href="#!" class="">User Registration
                    <br>
                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                </a>
            </li>
            <li class="step-item <?php echo ($step == 2) ? 'active' : '' ?>">
                <a href="#!" class="<?php echo ($step == 2) ? 'activetab' : '' ?>">Fill Details
                    <br>
                    <?php if ($step > 2) { ?>
                        <span class="text-success"><i class="fa fa-check-circle"></i></span>
                    <?php } ?>
                </a>
            </li>
            <li class="step-item <?php echo ($step == 3) ? 'active' : '' ?>">
                <a href="#!" class="<?php echo ($step == 3) ? 'activetab' : '' ?>">Add Rates
                    <br>
                    <?php if ($step > 3) { ?>
                        <span class="text-success"><i class="fa fa-check-circle"></i></span>
                    <?php } ?>
                </a>
            </li>
            <li class="step-item <?php echo ($step == 4) ? 'active' : '' ?>">
                <a href="#!" class="<?php echo ($step == 4) ? 'activetab' : '' ?>">Registration Payment
                    <br>
                    <?php if ($step > 4) { ?>
                        <span class="text-success"><i class="fa fa-check-circle"></i></span>
                    <?php } ?>
                </a>
            </li>
            <li class="step-item">
                <a href="#!" class="">Finish

                </a>
            </li>
        </ul>
    </div>
</section>