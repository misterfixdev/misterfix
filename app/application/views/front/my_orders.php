<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav');

if ($this->ion_auth->logged_in()) {
    $user = $this->ion_auth->get_Logged_user();
}

?>


<div id="yellow_flame"></div>
<section class="outer main_view" style="min-height: 450px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>My Orders</h3>
            </div>
            <?php if (count($orders)) { ?>

                <?php foreach (array_chunk($orders, 3) as $chunk) { ?>
                    <div class="col-md-12">
                        <div class="row">
                            <?php foreach ($chunk as $order) { ?>
                                <div class="col-md-4 mt-4">

                                    <div class="card profile-card-5" <?php echo ($order['status'] == 4 || $order['status'] == 5) ? 'style="border:4px solid red"' : ''; ?>>
                                        <div class="card-body pt-3">
                                            <h5 class="card-title">On <?php echo $order['created_on'] ?></h5>
                                            <p class="card-text">
                                            <ul class="p-0" style="list-style-type: none;">
                                                <li class="p-1">Service Provider:
                                                    <span><b><?php echo $order['first_name'] ?><?php echo $order['last_name'] ?></b></span>
                                                </li>
                                                <li class="p-1">Required Date:
                                                    <span><b><?php echo $order['required_date'] ?></b></span>
                                                </li>
                                                <li class="p-1">Required Time:
                                                    <span><b><?php echo $order['required_time'] ?></b></span>
                                                </li>
                                                <li class="p-1">Required Category:
                                                    <span><b><?php foreach (json_decode($order['category']) as $cat) {
                                                                echo $cat . ' / ';
                                                            } ?></b></span></li>
                                                <li class="p-1">Work Background:
                                                    <span><b><?php echo substr($order['work_background'], 0, 25) ?>
                                                            ...</b></span></li>
                                                <li class="p-1">Additional Phone:
                                                    <span><b><?php echo $order['additional_phone'] ?></b></span>
                                                </li>
                                                <?php if ($order['supervisor_id']) { ?>
                                                    <li class="p-1 border border-info"><b>Supervisor</b> <br>
                                                        <ul>
                                                            <li>Supervisor Name: <b><?php echo $order['sup_name'] ?></b></li>
                                                            <li>Supervisor
                                                                Mobile: <b><?php echo $order['sup_mobile'] ?></b></li>
                                                        </ul>
                                                    </li>
                                                <?php } ?>
                                                <?php if ($order['status'] == -1) { ?>
                                                    <li class="p-1 bg-warning"><span><b>Payment Pending</b>
                                            <a href="<?php echo base_url() ?>service/payment/<?php echo $order['ser_id'] ?>"
                                               class="pull-right"><b>Make Payment</b></a></span>
                                                    </li>
                                                <?php } else { ?>
                                                    <li class="p-1">Payment: <span><b><?php echo $order['amount'] ?> LKR <small
                                                                        class="text-muted">(<?php echo $order['reason'] ?>
                                                                    )</small></b></span>
                                                    </li>
                                                <?php } ?>

                                            </ul>
                                            </p>
                                            <?php if ($order['status'] == -1) { ?>
                                                <button class="btn-sm btn-danger btn disabled">Payment Pending</button>
                                            <?php } elseif ($order['status'] == 0) { ?>
                                                <button class="btn-sm btn-warning btn disabled">Pending Acceptance
                                                </button>
                                                <button class="btn-sm btn btn-dark " data-toggle="modal"
                                                        data-target="#exampleModal"
                                                        onclick="request_cancel(<?php echo $order['ser_id'] ?>)">Request
                                                    Cancellation
                                                </button>
                                            <?php } elseif ($order['status'] == 1) { ?>
                                                <button class="btn-sm btn-success btn disabled">Accepted</button>
                                                <button class="btn-sm btn btn-primary "
                                                        onclick="markDone(<?php echo $order['ser_id'] ?>)">Mark Task
                                                    Done
                                                </button>
                                                <button class="btn-sm btn btn-dark " data-toggle="modal"
                                                        data-target="#exampleModal"
                                                        onclick="request_cancel(<?php echo $order['ser_id'] ?>)">Request
                                                    Cancellation
                                                </button>
                                            <?php } elseif ($order['status'] == 2) { ?>
                                                <button class="btn-sm btn-primary btn disabled">Task Completed</button>
                                            <?php } elseif ($order['status'] == 3) { ?>
                                                <button class="btn-sm btn-danger btn disabled">Rejected</button>
                                                <span class="text-danger"><small><b>Reason :</b> <?php echo $order['remark'] ?></small></span>
                                            <?php } elseif ($order['status'] == 4) { ?>
                                                <button class="btn-sm btn-warning btn disabled">Cancellation Requested
                                                </button>
                                            <?php } else { ?>
                                                <button class="btn-sm btn-danger btn disabled">Cancelled
                                                </button><?php } ?>
                                            <!--                        <a href="#" class="btn btn-primary pull-right btn-sm">View</a>-->
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

            <?php } else { ?>
                <div class="col-md-12 border-warning border rounded p-5">
                    No tasks available
                </div>
            <?php } ?>

        </div>
    </div>

</section>
<br>
<br>
<br>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title text-light" id="exampleModalLabel">Cancellation Reasons</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-light">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url() ?>my-account/orders-reject" method="post">
                    <input type="hidden" name="serv_id" id="serv_id">
                    <input type="hidden" name="user_id" value="<?php echo $user['id'] ?>">
                    <ul style="list-style-type: none;">
                        <?php
                        $x = 1;
                        foreach ($reasons as $reason) { ?>
                            <li>
                                <input type="radio" name="reason" <?php echo ($x == 1) ? 'checked' : '' ?>
                                       value="<?php echo $reason['id'] ?>" id="id-<?php echo $reason['id'] ?>">&nbsp;&nbsp;&nbsp;
                                <label for="id-<?php echo $reason['id'] ?>"><?php echo $reason['reason'] ?></label>
                            </li>
                            <?php
                            $x++;
                        } ?>
                    </ul>
                    &nbsp;&nbsp;
                    <button type="button" class="btn btn-secondary pull-right ml-2" data-dismiss="modal">Close</button>&nbsp;&nbsp;
                    <button type="submit" class="btn btn-primary pull-right">Proceed Cancellation Request</button>&nbsp;&nbsp;
                </form>

            </div>
        </div>
    </div>
</div>
<?php $this->view('front/includes/footer') ?>
