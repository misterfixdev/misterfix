<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav');

if ($this->ion_auth->logged_in()) {
    $user = $this->ion_auth->get_Logged_user();
}

?>


<div id="yellow_flame"></div>
<section class="outer main_view" style="min-height: 450px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>My Orders</h3>
            </div>
            <div class="col-md-12">
                <br>
                <div class="row">
                    <div class="col-md">
                        <a href="<?php echo base_url() ?>my-account/orders"
                           class="btn btn-sm btn-primary btn-block">All</a>
                    </div>
                    <div class="col-md">
                        <a href="<?php echo base_url() ?>my-account/orders?state=0"
                           class="btn btn-sm btn-info btn-block">Pending Acceptance</a>
                    </div>
                    <div class="col-md">
                        <a href="<?php echo base_url() ?>my-account/orders?state=1"
                           class="btn btn-sm btn-success btn-block">Ongoing Tasks</a>
                    </div>
                    <div class="col-md">
                        <a href="<?php echo base_url() ?>my-account/orders?state=2"
                           class="btn btn-sm btn-warning btn-block">Competed Tasks</a>
                    </div>
                    <div class="col-md">
                        <a href="<?php echo base_url() ?>my-account/orders?state=3"
                           class="btn btn-sm btn-danger btn-block">Rejected Tasks</a>
                    </div>
                    <div class="col-md">
                        <a href="<?php echo base_url() ?>my-account/orders?state=4"
                           class="btn btn-sm btn-dark btn-block">Request Cancellation</a>
                    </div>
                    <div class="col-md">
                        <a href="<?php echo base_url() ?>my-account/orders?state=5"
                           class="btn btn-sm btn-light btn-block">Cancelled Tasks</a>
                    </div>
                </div>
            </div>
            <?php if (count($orders)) { ?>

                <?php foreach (array_chunk($orders, 3) as $chunk) { ?>
                    <div class="col-md-12">
                        <div class="row">
                            <?php foreach ($chunk as $order) { ?>
                                <div class="col-md-4 mt-4">

                                    <div class="card profile-card-5" <?php echo ($order['status'] == 4) ? 'style="border:4px solid red"' : ''; ?>>
                                        <div class="card-body pt-3">
                                            <h5 class="card-title">
                                                On <?php echo $order['created_on'] ?> <?php echo $order['ser_id'] ?></h5>
                                            <p class="card-text">
                                            <ul class="p-0" style="list-style-type: none;">
                                                <li class="p-1">Service Provider:
                                                    <span><b><?php echo $order['first_name'] ?><?php echo $order['last_name'] ?></b></span>
                                                </li>
                                                <li class="p-1">Required Date:
                                                    <span><b><?php echo $order['required_date'] ?></b></span>
                                                </li>
                                                <li class="p-1">Required Time:
                                                    <span><b><?php echo $order['required_time'] ?></b></span>
                                                </li>
                                                <li class="p-1">Required Category:
                                                    <span><b><?php foreach (json_decode($order['category']) as $cat) {
                                                                echo $cat . ' / ';
                                                            } ?></b></span></li>
                                                <li class="p-1">Work Background:
                                                    <span><b><?php echo substr($order['work_background'], 0, 25) ?>
                                                            ...</b></span>
                                                </li>
                                                <li class="p-1">Additional Phone:
                                                    <span><b><?php echo $order['additional_phone'] ?></b></span>
                                                </li>
                                                <?php if ($order['supervisor_id']) { ?>
                                                    <li class="p-1 border border-info"><b>Supervisor</b> <br>
                                                        <ul>
                                                            <li>Supervisor Name: <?php echo $order['sup_name'] ?></li>
                                                            <li>Supervisor
                                                                Mobile: <?php echo $order['sup_mobile'] ?></li>
                                                        </ul>
                                                    </li>
                                                <?php } ?>
                                                <?php if ($order['status'] == -1) { ?>
                                                    <li class="p-1 bg-warning"><span><b>Payment Pending</b>
                                            <a href="<?php echo base_url() ?>service/payment/<?php echo $order['ser_id'] ?>"
                                               class="pull-right"><b>Make Payment</b></a></span>
                                                    </li>
                                                <?php } else { ?>
                                                    <li class="p-1">Payment: <span><b><?php echo $order['amount'] ?> LKR <small
                                                                        class="text-muted">(<?php echo $order['reason'] ?>
                                                                    )</small></b></span>
                                                    </li>
                                                <?php } ?>

                                            </ul>
                                            </p>
                                            <?php if ($order['status'] == -1) { ?>
                                                <button class="btn-sm btn-danger btn disabled">Payment Pending</button>
                                            <?php } elseif ($order['status'] == 0) { ?>
                                                <button class="btn-sm btn-success btn"
                                                        onclick="acceptTask(<?php echo $order['ser_id'] ?>)">Accept
                                                </button>
                                                <button class="btn-sm btn-danger btn"
                                                        onclick="rejectTask(<?php echo $order['ser_id'] ?>)">Reject
                                                </button>
                                            <?php } elseif ($order['status'] == 1) { ?>
                                                <button class="btn-sm btn-success btn disabled">Accepted</button>
                                                <button class="btn-sm btn btn-primary "
                                                        onclick="markDone(<?php echo $order['ser_id'] ?>)">Mark Task
                                                    Done
                                                </button>
                                            <?php } elseif ($order['status'] == 2) { ?>
                                                <button class="btn-sm btn-primary btn disabled">Task Completed</button>
                                            <?php } elseif ($order['status'] == 3) { ?>
                                                <button class="btn-sm btn-danger btn disabled">Rejected</button>
                                            <?php } elseif ($order['status'] == 4) { ?>
                                                <small class="text-danger">Cancellation Requested</small>
                                                <button class="btn-sm btn-warning btn" onclick="accept_Cancel(<?php echo $order['ser_id'] ?>)">Accept Cancellation</button>
                                            <?php } else { ?>
                                                <button class="btn-sm btn-warning btn disabled">Cancellation Accepted
                                                </button>
                                            <?php } ?>
                                            <?php if ($order['cancel_reason'] != '') { ?>
                                                <br>
                                                <small>Reason:</small> <small class="badge badge-danger mt-3"><?php echo $order['cancel_reason'] ?></small>
                                            <?php } ?>
                                            <!--                        <a href="#" class="btn btn-primary pull-right btn-sm">View</a>-->
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

            <?php } else { ?>
                <div class="col-md-12 border-warning border rounded p-5">
                    No tasks available
                </div>
            <?php } ?>
        </div>
    </div>

</section>
<br>
<br>
<br>

<!--reject reason modal-->
<div class="modal fade" id="rejectReason" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!--reject reason modal-->
<?php $this->view('front/includes/footer') ?>
