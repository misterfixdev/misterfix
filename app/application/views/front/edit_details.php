<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav') ?>

<div id="yellow_flame"></div>
<?php
if ($this->ion_auth->logged_in()) {

    $user = $this->ion_auth->get_Logged_user();
    $step = $user['step'];
} else {
    header('location:' . base_url());
}

?>

<?php
if ($user['is_completed'] == 0) {
    $this->view('front/includes/service_provider_step', array('step' => $step));
}
$this->view('front/includes/message');
?>
<br>
<div class="container border">
    <!-- <div class="space-3 lo"></div> -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-pg">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <h3>Edit Details </h3>
                    </div>
                    <div class="col-md-4">
                        <span class="pull-right"><a href="<?php echo base_url() ?>serviceprovider/account">Back to Account</a></span>
                    </div>

                    <form action="<?php echo base_url(); ?>serviceprovider/details/edit-save" class="col-md-12"
                          method="post">
                        <br>
                        <input type="hidden" name="user_id" value="<?php echo $user['id'] ?>">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Location</label>
                                    <select class="form-control select2select" name="location">
                                        <option value="" selected disabled>Select Location</option>
                                        <?php
                                        if (count($cities)) {
                                            foreach ($cities as $city) { ?>
                                                <option value="<?php echo $city['id'] ?>"
                                                        label="<?php echo $city['name'] ?>" <?php echo ($service_provider[0]['city_id'] == $city['id']) ? 'selected' : '' ?>><?php echo $city['name'] ?></option>
                                            <?php }
                                        } ?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="">Experience</label>
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" id=""
                                           value="<?php echo $service_provider[0]['expariance'] ?>" name="experience"
                                           max="200" min="0"
                                           aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">Months</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Working Area</label>
                                    <select class="form-control" name="working_area">
                                        <option value="island_wide">Island Wide</option>
                                        <option value="district_area">District Area</option>
                                        <option value="city_area">City Area</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Date Of Birth</label>
                                    <input type="date" class="form-control"
                                           value="<?php echo $service_provider[0]['dob'] ?>" name="dob">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Gender</label><br>
                                    <input type="radio" name="gender"
                                           value="male" <?php echo ($service_provider[0]['gender'] == 'male') ? 'checked' : '' ?>>&nbsp;Male
                                    <input type="radio" name="gender"
                                           value="female" <?php echo ($service_provider[0]['gender'] == 'female') ? 'checked' : '' ?>>&nbsp;Female
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">description</label>
                                    <textarea name="description" id="" class="form-control"
                                              rows="2"><?php echo $service_provider[0]['description'] ?></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 border">
                                <div class="form-group">
                                    <label for="">Best time to call</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">From</label>
                                                <input type="time"
                                                       value="<?php echo $service_provider[0]['time_to_call_from'] ?>"
                                                       class="form-control" name="time_to_call_from">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">To</label>
                                                <input type="time"
                                                       value="<?php echo $service_provider[0]['time_to_call_to'] ?>"
                                                       class="form-control" name="time_to_call_to">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>

                        </div>
                        <div class="row ">
                            <div class="col-md-6 border pt-2">
                                <div class="form-group">
                                    <label for="">Main Service Type</label><br>
                                    <select type="text" class="form-control multiselect multiselect-icon"
                                            role="multiselect" name="main_service" id="main_service">
                                        <option value="0">Select Main Service type</option>
                                        <?php
                                        if (count($categories)) {
                                            foreach ($categories as $category) { ?>
                                                <option value="<?php echo $category['id'] ?>" <?php echo ($service_provider[0]['category_id'] == $category['id'])?'selected':'' ?>
                                                        data-icon="<?php echo base_url() ?>assets/icons/<?php echo $category['icon'] ?>"
                                                        label="<?php echo $category['name'] ?>"><?php echo $category['name'] ?></option>
                                            <?php }
                                        } ?>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-6 border pt-2">
                                <div class="form-group">
                                    <label for="">Alternative Service Type</label><br>
                                    <select type="text" class="form-control multiselect multiselect-icon"
                                            multiple="multiple" role="multiselect" name="alternative_services[]"
                                            id="alternative_service">
                                        <?php
                                        if (count($categories)) {
                                            foreach ($categories as $category) { ?>
                                                <option value="<?php echo $category['id'] ?>" <?php echo (in_array($category['id'], $service_provider_category_ids))?'selected':'' ?>
                                                        data-icon="<?php echo base_url() ?>assets/icons/<?php echo $category['icon'] ?>"
                                                        label="<?php echo $category['name'] ?>"><?php echo $category['name'] ?></option>
                                            <?php }
                                        } ?>
                                    </select>

                                </div>
                            </div>
                        </div>

                        <?php $days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'); ?>
                        <div class="row">
                            <div class="col-md-12 border pt-2 pb-3">
                                <label for="">Working Days</label>
                                <div class="form-group">
                                    <?php foreach ($days as $day) { ?>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="days[]" <?php echo (in_array(strtolower($day), $working_days))?'checked':'' ?>
                                                   id="exampleRadios1" value="<?php echo strtolower($day); ?>">
                                            <label class="form-check-label" for="exampleRadios1">
                                                <?php echo $day; ?>
                                            </label>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success pull-right">Save</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<br>
<br>
<?php $this->view('front/includes/footer') ?>
