<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav');

if ($this->ion_auth->logged_in()) {
    $user = $this->ion_auth->get_Logged_user();
}else{
    redirect('user/index');
}

?>


<div id="yellow_flame"></div>
<?php
$this->view('front/includes/message');
?>
<section class="outer main_view" style="min-height: 450px">
    <div class="container">
        <div class="space-3"></div>
        <div class="row">
            <form action="<?php echo base_url() ?>appointment/save" class="col-md-12"
                  method="post" enctype="multipart/form-data">
                <h2>Make an appointment with Mr.FIX</h2>
                <input type="hidden" value="<?php echo $user['id'] ?>" name="user_id">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Subject</label>
                            <input type="text" class="form-control" id="" placeholder="Subject"
                                   value="" name="subject" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Date</label>
                            <input type="date" class="form-control" id=""
                                   value="" name="date" min="<?php echo date('Y-m-d') ?>" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Time Slot</label>
                            <?php $slots = array('8.30 am to 9.00 am', '9.00 am to 9.30 am', '9.30 am to 10.00 am', '10.00 am to 10.30 am', '10.30 am to 11.00 am', '11.00 am to 11.30 am', '11.30 am to 12.00 noon', '12.00 noon to 12.30 pm', '12.30 pm to 1.00 pm', '1.00 pm to 1.30 pm', '1.30 pm to 2.00 pm', '2.00 pm to 2.30 pm', '2.30 pm to 3.00 pm', '3.00 pm to 3.30 pm', '3.30 pm to 4.00 pm', '4.00 pm to 4.30 pm', '4.30 pm to 5.00 pm') ?>
                            <select name="time_slot" id="" class="form-control" required>
                                <?php foreach ($slots as $slot) { ?>
                                    <option value="<?php echo $slot ?>"><?php echo $slot ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Matter</label>
                            <textarea type="date" class="form-control" id="" required placeholder="Matter"
                                      name="matter" rows="4"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="reset" class="btn btn-lg btn-danger">Reset</button>
                            <button type="submit" class="btn btn-lg btn-success">Submit Appointment</button>
                        </div>
                    </div>


                </div>
            </form>
        </div>

    </div>

</section>
<br>
<br>
<br>
<?php $this->view('front/includes/footer') ?>
