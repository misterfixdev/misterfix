<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav');

if ($this->ion_auth->logged_in()) {
    $user = $this->ion_auth->get_Logged_user();
}

?>


<div id="yellow_flame"></div>
<section class="outer main_view">
    <div class="container">
        <div class="space-3"></div>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="profile-sidebar">
                        <div class="card-body border mb-4">
                            <!-- SIDEBAR USERPIC -->
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="<?php echo base_url() ?>assets/img/profile_images/<?php echo ($details[0]['profile_image'] != null) ? $details[0]['profile_image'] : 'default.png' ?>"
                                         class="img-responsive"
                                         alt="">
                                </div>
                                <div class="col-md-9">
                                    <p>
                                        <b><?php echo $details[0]['first_name'] ?><?php echo $details[0]['last_name'] ?></b>
                                    </p>
                                    <p class="mb-1"><?php echo $details[0]['city'] ?>
                                        , <?php echo $details[0]['district'] ?></p>

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sub-row text-warning pb-3 text-center" style="font-size: 15px">
                                        <div class="star-ratings-sprite"
                                             title="<?php echo $rate['rate']; ?> Star Rating">
                                    <span style="width:<?php echo ($rate['rate'] / 5) * 100; ?>%"
                                          class="star-ratings-sprite-rating"></span>
                                        </div>
                                        <p class="text-muted" style="font-size: 14px;"><?php echo $rate['num']; ?>
                                            Rating(s)</p>
                                    </div>
                                    <hr>
                                    <div class="ext-prof-view-working-area">
                                        <h5 class="mt-2 mb-2"><span>Working Area:</span><span
                                                    class="ty"><?php echo ucwords(str_replace('_', ' ', $details[0]['working_area'])); ?></span>
                                        </h5>
                                        <h5 class="mt-2 mb-2"><span>Gender:</span><span
                                                    class="ty"><?php echo ucwords(str_replace('_', ' ', $details[0]['gender'])); ?></span>
                                        </h5>
                                        <h5 class="mt-2 mb-2"><span>Age:</span><span
                                                    class="ty"><?php echo $details[0]['age']; ?> Years</span>
                                        </h5>
                                        <h5 class="mt-2 mb-2"><span>Experience:</span><span
                                                    class="ty"><?php echo floor($details[0]['expariance'] / 12); ?>
                                                Year(s)</span>
                                        </h5>
                                        <h5 class="mt-2 mb-2"><span>Experience:</span>
                                            <span class="ty">
                                            <?php foreach (json_decode($details[0]['working_days']) as $day) { ?>
                                                <span class=""><?php echo ucwords($day); ?> / </span>
                                            <?php } ?>
                                            </span>
                                        </h5>
                                        <?php if ($details[0]['time_to_call_from'] != '') { ?>
                                            <?php
                                            $date = new DateTime($details[0]['time_to_call_from']);
                                            $time_to_call_from = $date->format('h:i A');

                                            $to_date = new DateTime($details[0]['time_to_call_to']);
                                            ?>
                                            <h5 class="mt-2 mb-2"><span>Best Time to Call:</span>
                                                <span class="ty">
                                            <?php echo $time_to_call_from ?><?php echo ($details[0]['time_to_call_to']) ? '- ' . $to_date->format('h:i A') : '' ?>
                                            </span>
                                            </h5>
                                        <?php } ?>
                                        <h5 class="mt-2 mb-2"><span>Main Category:</span><span
                                                    class="ty"><?php echo $details[0]['main_category_name'] ?></span>
                                        </h5>
                                        <h5 class="mt-2 mb-2"><span>Sub Category:</span>
                                            <?php foreach ($details[0]['alternative_categories'] as $alt) { ?>
                                                <span class="ty"><?php echo $alt['category_name'] ?> / </span>
                                            <?php } ?>

                                        </h5>
                                        <h5 class="mt-2 mb-2"><span>Phone:</span>
                                            <b><?php echo $details[0]['phone'] ?></b>
                                        </h5>
                                        <a href="<?php echo base_url() ?>service/view/<?php echo $details[0]['id'] ?>"
                                           class="btn-info btn btn-block mt-5">View Account</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <br>
                    <form action="<?php echo base_url() ?>service/save/book" class="border p-3 border-info rounded"
                          method="post">
                        <input type="hidden" name="customer_id" value="<?php echo $user['id'] ?>">
                        <input type="hidden" name="service_provider_id" value="<?php echo $details[0]['user_id'] ?>">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Required Date</label>
                                    <input type="text" class="form-control date" placeholder="Required Date" required
                                           name="required_date">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Required Time</label>
                                    <input type="text" class="form-control timepicker" placeholder="" required
                                           name="required_time">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Select Category</label><br>
                                    <input type="checkbox" class="" placeholder=""
                                           name="category[]" checked
                                           value="<?php echo $details[0]['main_category_name'] ?>"> <?php echo $details[0]['main_category_name'] ?>
                                    <br>
                                    <?php foreach ($details[0]['alternative_categories'] as $alt) { ?>
                                        <input type="checkbox" class="" placeholder=""
                                               name="category[]"
                                               value="<?php echo $alt['category_name'] ?>"> <?php echo $alt['category_name'] ?>
                                        <br>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Location</label>
                                    <select class="form-control select2select" name="location">
                                        <option value="" selected disabled>Select Location</option>
                                        <?php
                                        if (count($cities)) {
                                            foreach ($cities as $city) { ?>
                                                <option value="<?php echo $city['id'] ?>"
                                                        label="<?php echo $city['name'] ?>" <?php echo ($this->input->get('city') == $city['id']) ? 'selected' : '' ?>><?php echo $city['name'] ?></option>
                                            <?php }
                                        } ?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Explain the work background</label>
                                    <textarea type="text" class="form-control" placeholder="Explain the work background"
                                              required name="work_background"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row border-top border-bottom">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Phone number</label>
                                            <input type="number" class="form-control" placeholder="71xxxxxxx" required
                                                   value="<?php echo $user['phone'] ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Additional Phone number</label>
                                            <input type="number" class="form-control" placeholder="71xxxxxxx"
                                                   name="additional_phone">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" value="Request" class="btn btn-success pull-right">

                            </div>

                        </div>

                    </form>
                </div>

            </div>
            <br>

        </div>


    </div>

</section>
<br>
<br>
<br>

<?php
$allDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
$days = json_decode($details[0]['working_days']);
foreach ($days as $day) {
    if (($key = array_search(ucwords($day), $allDays)) !== false) {
        unset($allDays[$key]);
    }
}
$dayNum = [];
foreach ($allDays as $allDay) {
    if ($allDay == 'Sunday') {
        array_push($dayNum, 0);
    }
    if ($allDay == 'Monday') {
        array_push($dayNum, 1);
    }
    if ($allDay == 'Tuesday') {
        array_push($dayNum, 2);
    }
    if ($allDay == 'Wednesday') {
        array_push($dayNum, 3);
    }
    if ($allDay == 'Thursday') {
        array_push($dayNum, 4);
    }
    if ($allDay == 'Friday') {
        array_push($dayNum, 5);
    }
    if ($allDay == 'Saturday') {
        array_push($dayNum, 6);
    }
}
function js_str($s)
{
    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
}

function js_array($array)
{
    $temp = array_map('js_str', $array);
    return '[' . implode(',', $temp) . ']';
}

?>
<script>
    <?php echo 'var alldaynums = ', js_array($dayNum), ';';    ?>
</script>
<?php $this->view('front/includes/footer') ?>
