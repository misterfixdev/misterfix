<?php $this->view('front/includes/header') ?>
<body>
<?php $this->view('front/includes/nav') ?>

<div id="yellow_flame"></div>
<?php
if ($this->ion_auth->logged_in()) {

    $user = $this->ion_auth->get_Logged_user();
    $step = $user['step'];
} else {
    header('location:' . base_url());
}

?>

<?php
if ($user['is_completed'] == 0) {
    $this->view('front/includes/service_provider_step', array('step' => $step));
}
$this->view('front/includes/message');
?>
<br>
<div class="container border">
    <!-- <div class="space-3 lo"></div> -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-pg">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <h3>Edit Categories and Rates </h3>
                    </div>
                    <div class="col-md-4">
                        <span class="pull-right"><a href="<?php echo base_url() ?>serviceprovider/account">Back to Account</a></span>
                    </div>
                    <form action="<?php echo base_url(); ?>serviceprovider/rates/save" class="col-md-12"
                          method="post">
                        <input type="hidden" name="user_id" value="<?php echo $user['id'] ?>">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">
                                        <small>Main Category</small>
                                    </label>
                                    <h4 class="m-0">
                                        <img src="<?php echo base_url() ?>assets/icons/<?php echo $service_provider[0]['main_category_icon'] ?>"
                                             alt="" style="width: 40px">
                                        <?php echo $service_provider[0]['main_category_name'] ?></h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">
                                        <small>Rate</small>
                                    </label>
                                    <input type="number" class="form-control" id="" name="main_cat"
                                           value="<?php echo $service_provider[0]['rate'] ?>">
                                </div>
                            </div>

                        </div>
                        <?php foreach ($service_provider_categories as $service_provider_category) { ?>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                            <small>Sub Category</small>
                                        </label>
                                        <h4 class="m-0">
                                            <img src="<?php echo base_url() ?>assets/icons/<?php echo $service_provider_category['main_category_icon'] ?>"
                                                 alt="" style="width: 40px">
                                            <?php echo $service_provider_category['main_category_name'] ?></h4>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">
                                            <small>Rate</small>
                                        </label>
                                        <input type="number" class="form-control" id=""
                                               name="input_<?php echo $service_provider_category['id'] ?>"
                                               value="<?php echo $service_provider_category['rate'] ?>">
                                    </div>
                                </div>

                            </div>
                        <?php } ?>
                        <button type="submit" class="btn btn-success pull-right">Save</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<br>
<br>
<?php $this->view('front/includes/footer') ?>
