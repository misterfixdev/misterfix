<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>jQuery.post demo</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
</head>
<body>


    <input type="submit" onclick="send()" value="Search">

<!-- the result of the search will be rendered inside this div -->
<div id="result"></div>

<script>
    function send() {

        // $.ajax({
        //     beforeSend: function(xhrObj){
        //         xhrObj.setRequestHeader("Access-Control-Allow-Origin", "*");
        //         xhrObj.setRequestHeader("Content-Type","application/json");
        //         xhrObj.setRequestHeader("Accept","application/json");
        //         xhrObj.setRequestHeader("Authorization","Apikey eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2NTNlMWRlMC02MzczLTExZWEtOWM1Mi0yMWE5MmNkM2M2OWIiLCJzdWIiOiJTSE9VVE9VVF9BUElfVVNFUiIsImlhdCI6MTU4MzkxNTc4NSwiZXhwIjoxODk5NDQ4NTg1LCJzY29wZXMiOnsiYWN0aXZpdGllcyI6WyJyZWFkIiwid3JpdGUiXSwibWVzc2FnZXMiOlsicmVhZCIsIndyaXRlIl0sImNvbnRhY3RzIjpbInJlYWQiLCJ3cml0ZSJdfSwic29fdXNlcl9pZCI6IjQyNzEiLCJzb191c2VyX3JvbGUiOiJ1c2VyIiwic29fcHJvZmlsZSI6ImFsbCIsInNvX3VzZXJfbmFtZSI6IiIsInNvX2FwaWtleSI6Im5vbmUifQ.h-_R6YmAIP_SAcvjnu14pmv1H_GNyrbUtWsu9zBO5FA");
        //     },
        //     type: "post",
        //     url: "https://api.getshoutout.com/otpservice/send",
        //     data:{
        //         "source": "ShoutDEMO",
        //         "transport": "sms",
        //         "content": {
        //             "sms":"Your code is 1111"
        //         },
        //         "destination":"94713882815"
        //     },
        //     success: function(data){
        //         console.log(data);
        //     }
        // });

        // }
        $.ajaxSetup({
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Apikey eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2NTNlMWRlMC02MzczLTExZWEtOWM1Mi0yMWE5MmNkM2M2OWIiLCJzdWIiOiJTSE9VVE9VVF9BUElfVVNFUiIsImlhdCI6MTU4MzkxNTc4NSwiZXhwIjoxODk5NDQ4NTg1LCJzY29wZXMiOnsiYWN0aXZpdGllcyI6WyJyZWFkIiwid3JpdGUiXSwibWVzc2FnZXMiOlsicmVhZCIsIndyaXRlIl0sImNvbnRhY3RzIjpbInJlYWQiLCJ3cml0ZSJdfSwic29fdXNlcl9pZCI6IjQyNzEiLCJzb191c2VyX3JvbGUiOiJ1c2VyIiwic29fcHJvZmlsZSI6ImFsbCIsInNvX3VzZXJfbmFtZSI6IiIsInNvX2FwaWtleSI6Im5vbmUifQ.h-_R6YmAIP_SAcvjnu14pmv1H_GNyrbUtWsu9zBO5FA',

            }
        });

        $.post("https://api.getshoutout.com/otpservice/send", {
            "source": "ShoutDEMO",
            "transport": "sms",
            "content": {
                "sms": "Your code is {{code}}"
            },
            "destination": "94713882815"
        }).done(function( data ) {
            alert( "Data Loaded: " + data );
        });

    }
</script>

</body>
</html>
