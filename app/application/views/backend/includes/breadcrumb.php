<section class="content-header">
    <h1>
        <?php echo $main_title ?>
        <small><?php echo $sub_title ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><?php echo $sub_title ?></li>
        <li class="active"><?php echo $main_title ?></li>
    </ol>
</section>

<section class="">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-xs-12">
    <?php $this->view('backend/includes/message') ?>
        </div>
    </div>
</section>