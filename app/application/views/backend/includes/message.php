<?php if ($this->session->flashdata('msg')) { ?>
    <div class="container mb-0 mt-2">
        <div class="row" style="margin-bottom: 0px !important;">
            <div class="col-md-12 mt-3">
                <br>
                <div class="alert alert-success alert-dismissible mb-0" style="margin-bottom: 0px !important;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check-circle"></i> Success!</h4>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<?php if ($this->session->flashdata('error')) { ?>
    <div class="container">
        <div class="row" style="margin-bottom: 0px !important;">
            <div class="col-md-12 mt-3">
                <br>
                <div class="alert alert-success alert-dismissible" style="margin-bottom: 0px !important;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
