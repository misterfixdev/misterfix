<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url() ?>assets/img/profile_images/admin.png" class="img-circle"
                     alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $user['first_name'] ?><?php echo $user['last_name'] ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Active</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="<?php echo base_url() ?>admin/dashboard">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-star-o"></i>
                    <span>Service Requests</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url() ?>admin/service-requests/list"><i class="fa fa-circle-o="></i>
                            List Service Requests</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-plus"></i>
                    <span>Supervisors</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url() ?>admin/supervisors/add-list"><i class="fa fa-circle-o"></i>
                            Add/List Supervisors</a></li>
                    <li><a href="<?php echo base_url() ?>admin/supervisors/assign"><i class="fa fa-circle-o"></i> Assign
                            Supervisor</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-circle"></i>
                    <span>Members</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url() ?>admin/members/list"><i class="fa fa-circle-o"></i> List Members</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-circle-o"></i>
                    <span>Service Providers</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url() ?>admin/service-providers/list"><i class="fa fa-circle-o"></i>
                            List Service Providers</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-money"></i>
                    <span>Payments</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url() ?>admin/payments/list"><i class="fa fa-circle-o"></i> List
                            Payments</a></li>
                    <li><a href="<?php echo base_url() ?>admin/payments/withdrawals"><i class="fa fa-circle-o"></i>
                            Withdrawals</a></li>
                    <li><a href="<?php echo base_url() ?>admin/payments/refunds"><i class="fa fa-circle-o"></i>
                            Refunds</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cab"></i>
                    <span>Service Categories</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url() ?>admin/category/list"><i class="fa fa-circle-o"></i> List
                            Service Categories</a></li>
                    <li><a href="<?php echo base_url() ?>admin/category/add"><i class="fa fa-circle-o"></i> Add Service
                            Category</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-envelope"></i>
                    <span>Appointments</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url() ?>admin/appointments/list"><i class="fa fa-circle-o"></i> List
                            Appointments</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>