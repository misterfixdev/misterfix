<?php $this->view('backend/includes/header') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->view('backend/includes/top_nav') ?>
    <?php $this->view('backend/includes/nav') ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua-gradient">
                        <div class="inner">
                            <h3><?php echo $members ?></h3>
                            <p>Members</p>
                        </div>
                        <div class="icon">
                            <!--                            <i class="fa fa user"></i>-->
                        </div>
                        <a href="<?php echo base_url() ?>admin/members/list" class="small-box-footer">More info <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green-gradient">
                        <div class="inner">
                            <h3><?php echo $sps; ?></h3>
                            <p>Service Providers</p>
                        </div>
                        <div class="icon">
                        </div>
                        <a href="<?php echo base_url() ?>admin/service-providers/list" class="small-box-footer">More
                            info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?php echo $supervisors ?></h3>
                            <p>Supervisors</p>
                        </div>
                        <div class="icon">
                        </div>
                        <a href="<?php echo base_url() ?>admin/supervisors/add-list" class="small-box-footer">More info
                            <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red-gradient">
                        <div class="inner">
                            <h3><?php echo $withdrawals; ?> LKR</h3>

                            <p>Total Withdrawals</p>
                        </div>
                        <div class="icon">
                        </div>
                        <a href="<?php echo base_url() ?>admin/payments/withdrawals" class="small-box-footer">More info
                            <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow-gradient">
                        <div class="inner">
                            <h3><?php echo $service_requests; ?></h3>

                            <p>All Service Requests</p>
                        </div>
                        <div class="icon">
                        </div>
                        <a href="<?php echo base_url() ?>admin/service-requests/list" class="small-box-footer">More info
                            <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <section class="col-lg-12 connectedSortable">
                    <div class="nav-tabs-custom">
                        <!-- Tabs within a box -->
                        <ul class="nav nav-tabs pull-right">
                            <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
                            <li class="pull-left header"><i class="fa fa-inbox"></i> Service Request</li>
                        </ul>
                        <div class="tab-content no-padding">
                            <!-- Morris chart - Sales -->
                            <div class="chart tab-pane active" id="revenue-chart"
                                 style="position: relative; height: 300px;"></div>
                        </div>
                    </div>
                </section>
                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

    <?php $this->view('backend/includes/footer') ?>

    <script>
        var area = new Morris.Area({
            element: 'revenue-chart',
            resize: true,
            data: <?php echo $cnt; ?>,
            xkey: 'y',
            ykeys: ['item'],
            labels: ['Service Requests'],
            lineColors: ['#a0d0e0', '#3c8dbc'],
            hideHover: 'auto'
        });
    </script>