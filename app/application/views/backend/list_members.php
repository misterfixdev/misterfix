<?php $this->view('backend/includes/header') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->view('backend/includes/top_nav') ?>
    <?php $this->view('backend/includes/nav') ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <?php $this->view('backend/includes/breadcrumb') ?>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">

                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Joined Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tboady>
                                    <?php
                                    $x = 1;
                                    foreach ($members as $member) { ?>
                                        <tr>
                                            <td><?php echo $x; ?></td>
                                            <td><?php echo $member['first_name'] ?> <?php echo $member['last_name'] ?>
                                                <br>
                                                User Name : <?php echo $member['username'] ?></td>
                                            <td>
                                                <?php echo $member['phone'] ?>
                                                &nbsp;&nbsp;&nbsp;<?php echo ($member['phone_verified']) ? '<i class="fa fa-check-circle text-success" title="Verified"></i>' : '<i class="fa fa-info-circle text-danger" title="Unverified"></i>' ?>
                                                <br>

                                            </td>
                                            <td>
                                                <?php echo ($member['active']) ? '<i class="fa fa-check-circle text-success" title="Active"></i> Active' : '<i class="fa fa-info-circle text-danger" title="Inactive"></i> InActive' ?>
                                                <br>
                                            </td>
                                            <td><?php
                                                $ts = $member['created_on'];
                                                $date = new DateTime("@$ts");
                                                echo $date->format('Y-m-d H:i:s');
                                                ?></td>
                                            <td><?php echo ($member['active']) ? anchor("auth/deactivate/".$member['id'], 'Deactivate',['class'=>'btn btn-danger btn-sm']) : anchor("auth/activate/". $member['id'],'Activate',['class'=>'btn btn-success btn-sm']);?></td>
                                        </tr>
                                        <?php $x++;
                                    } ?>
                                </tboady>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <?php $this->view('backend/includes/footer') ?>

