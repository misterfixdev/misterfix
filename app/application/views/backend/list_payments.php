<?php $this->view('backend/includes/header') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->view('backend/includes/top_nav') ?>
    <?php $this->view('backend/includes/nav') ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <?php $this->view('backend/includes/breadcrumb') ?>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">

                        <div class="box-body">
                            <table id="example1" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th width="1%">#</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tboady>
                                    <?php
                                    $x = 1;
                                    foreach ($payments as $key => $member) { ?>
                                        <tr>
                                            <td><?php echo $x; ?></td>
                                            <td>
                                                <table class="table table-bordered table-striped">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Service Provider</th>
                                                        <th>Customer</th>
                                                        <th>Reason</th>
                                                        <th>Amount</th>
                                                        <th>Created At</th>
                                                    </tr>

                                                    <?php
                                                    $y = 1;
                                                    $total = 0;
                                                    $ids = array();
                                                    foreach ($member as $mem) {
                                                        $total = $total + $mem['amount'];

                                                        array_push($ids, $mem['payment_ids']);
                                                        ?>
                                                        <tr>
                                                            <!--                                                            --><?php //var_dump($mem) ?>
                                                            <td><?php echo $y; ?></td>
                                                            <td><?php echo $mem['user_f_name'] ?><?php echo $mem['user_l_name'] ?></td>
                                                            <td><?php echo $mem['pd_f_name'] ?><?php echo $mem['pd_l_name'] ?></td>
                                                            <td><?php echo $mem['reason'] ?></td>
                                                            <td>LKR. <?php echo $mem['amount'] ?></td>
                                                            <td><?php echo $mem['create_date'] ?></td>
                                                        </tr>

                                                        <?php
                                                        $y++;
                                                    }
                                                    $ids_json = json_encode($ids);
                                                    ?>
                                                    <tr>
                                                        <td colspan="3"></td>
                                                        <td class="text-right"><b>Total</b></td>
                                                        <td>
                                                            <b>LKR. <?php echo $total; ?></b>
                                                        </td>
                                                        <td>
                                                            <form action="<?php echo base_url() ?>admin/payments/withdraw"
                                                                  method="post">
                                                                <input type="hidden"
                                                                       value='<?php echo($ids_json)  ?>'
                                                                       name="ids">
                                                                <input type="hidden"
                                                                       value="<?php echo $mem['user_id']; ?>"
                                                                       name="user_id">
                                                                <input type="hidden" value="<?php echo $total; ?>"
                                                                       name="amount">
                                                                <button type="submit" class="btn btn-sm btn-primary">
                                                                    Withdraw LKR. <?php echo $total; ?></button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <!--                                            <td>X</td>-->
                                        </tr>
                                        <?php $x++;
                                    } ?>
                                </tboady>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <?php $this->view('backend/includes/footer') ?>

