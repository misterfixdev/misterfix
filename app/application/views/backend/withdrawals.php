<?php $this->view('backend/includes/header') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->view('backend/includes/top_nav') ?>
    <?php $this->view('backend/includes/nav') ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <?php $this->view('backend/includes/breadcrumb') ?>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">

                        <div class="box-body">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>#</th>
                                    <th>Service Provider</th>
                                    <th>Amount</th>
                                    <th>Withdraw Date</th>
                                </tr>
                                <tboady>
                                    <?php
                                    $x = 1;
                                    foreach ($withdrawals as $key => $withd) { ?>
                                        <tr>
                                            <td><?php echo $x; ?></td>
                                            <td><?php echo $withd['user_f_name'] . ' ' . $withd['user_l_name'] ?></td>
                                            <td><?php echo $withd['amount'] .' LKR' ?></td>
                                            <td><?php echo $withd['create_date'] ?></td>
                                        </tr>
                                        <?php $x++;
                                    } ?>
                                </tboady>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <?php $this->view('backend/includes/footer') ?>

