<?php $this->view('backend/includes/header') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->view('backend/includes/top_nav') ?>
    <?php $this->view('backend/includes/nav') ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <?php $this->view('backend/includes/breadcrumb') ?>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">

                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Customer</th>
                                    <th>Service Provider</th>
                                    <th>Require Date/Time</th>
                                    <th>Category</th>
                                    <th>Work Background</th>
                                    <th>Status</th>
                                    <th>Remark</th>
                                    <th>Locations</th>
                                    <th>Supervisor</th>
                                    <th>created Date</th>
                                </tr>
                                </thead>
                                <tboady>
                                    <?php
                                    $x = 1;
                                    foreach ($requests as $category) { ?>
                                        <tr>
                                            <td><?php echo $x; ?></td>
                                            <td>
                                                <?php echo $category['user_f_name'] ?> <?php echo $category['user_l_name'] ?>
                                                <br>
                                                <?php echo $category['user_phone'] ?>

                                                <?php if ($category['additional_phone']) { ?>
                                                    <br>
                                                    <?php echo $category['additional_phone'] ?>
                                                <?php } ?>

                                            </td>
                                            <td>
                                                <?php echo $category['pd_f_name'] ?><?php echo $category['pd_l_name'] ?>
                                            </td>
                                            <td>
                                                <?php echo $category['required_date'] ?><br>
                                                <?php echo $category['required_time'] ?>
                                            </td>
                                            <td>
                                                <?php echo $category['category'] ?>
                                            </td>
                                            <td>
                                                <?php echo $category['work_background'] ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($category['status'] == -1) {
                                                    echo 'Payment Pending';
                                                } elseif ($category['status'] == 0) {
                                                    echo 'Payment Done. Pending Accept';
                                                } elseif ($category['status'] == 1) {
                                                    echo 'Accepted';
                                                }elseif($category['status'] == 2){
                                                    echo 'Work Completed';
                                                }else{
                                                    echo 'Rejected';
                                                }
                                                ?>

                                            </td>
                                            <td>
                                                <?php echo ($category['remark']) ? $category['remark'] : '-' ?>
                                            </td>
                                            <td>
                                                <?php echo $category['location'] ?>
                                            </td>
                                            <td>
                                                <?php echo ($category['sup_name']) ? $category['sup_name'] : '-' ?><br>
                                                <?php echo $category['sup_mobile'] ?>
                                            </td>
                                            <td>
                                                <?php echo $category['created_date'] ?>
                                            </td>

                                        </tr>
                                        <?php $x++;
                                    } ?>
                                </tboady>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <?php $this->view('backend/includes/footer') ?>

