<?php $this->view('backend/includes/header') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->view('backend/includes/top_nav') ?>
    <?php $this->view('backend/includes/nav') ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <?php $this->view('backend/includes/breadcrumb') ?>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">

                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Icon</th>
                                    <th>Status</th>
                                    <th>created Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tboady>
                                    <?php
                                    $x = 1;
                                    foreach ($categories as $category) { ?>
                                        <tr>
                                            <td><?php echo $x; ?></td>
                                            <td>
                                                <?php echo $category['name'] ?>
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url() ?>assets/icons/<?php echo $category['icon'] ?>" width="50px" alt="">
                                            </td>
                                            <td>
                                                Active :
                                                &nbsp;&nbsp;<?php echo ($category['status']) ? '<i class="fa fa-check-circle text-success" title="Active"></i>' : '<i class="fa fa-info-circle text-danger" title="Inactive"></i>' ?>
                                                <br>
                                            </td>
                                            <td><?php
                                                echo $category['created_date'];
                                                ?></td>
                                            <td>X</td>
                                        </tr>
                                        <?php $x++;
                                    } ?>
                                </tboady>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <?php $this->view('backend/includes/footer') ?>

