<?php $this->view('backend/includes/header') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->view('backend/includes/top_nav') ?>
    <?php $this->view('backend/includes/nav') ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <?php $this->view('backend/includes/breadcrumb') ?>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    if ($this->session->flashdata('msg') != '') {
                        ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('msg') ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-4">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="">Task</h4>
                            <hr class="">
                        </div>
                        <div class="box-body">
                            <form action="" method="get">
                                <div class="row">

                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <select class="form-control select2" style="width: 100%;" name="task" id="task_select">
                                                <option value="" data-foo=""> -- Select Task -- </option>

                                                <?php if (count($tasks)) {
                                                    foreach ($tasks as $key => $task) { ?>
                                                        <option value="<?php echo $task['id'] ?>" <?php echo ($task['id'] == $this->input->get('task'))?'selected':'' ?>
                                                                data-foo="<?php echo preg_replace('/[^A-Za-z0-9\-,]/', ' ', $task['category']) ?>"><?php echo $task['first_name'] . ' ' . $task['last_name'] . ' - ' . $task['location'] ?></option>

                                                        <?php
                                                    }
                                                } ?>

                                            </select>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button class="btn btn-sm btn-primary btn-block" type="submit" id="find_btn" disabled>Find
                                                Supervisor
                                            </button>

                                        </div>
                                        <!-- /input-group -->
                                    </div>

                                </div>
                            </form>

                        </div>

                    </div>
                    <!-- /.box -->
                </div>

                <div class="col-md-8">
                    <form action="<?php echo base_url() ?>admin/supervisors/assign/save" method="post">
                        <input type="hidden" name="task_id" value="<?php echo $this->input->get('task')?>">

                        <div class="box">
                            <div class="box-header">
                                <h4 class="">Supervisor <?php echo $text ?></h4>
                                <hr class="">
                            </div>
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <select class="form-control select2" style="width: 100%;"
                                                    id="supervisor_select" name="supervisor">
                                                <option value="" data-foo=""> -- Select Supervisor -- </option>
                                                <?php if (count($supervisors)) {
                                                    foreach ($supervisors as $key => $supervisor) {
//                                                        $categories =
                                                        ?>
                                                        <option data-foo="Mobile : <?php echo preg_replace('/[^A-Za-z0-9\-,]/', ' ', $supervisor['mobile']) ?>"
                                                                value="<?php echo $supervisor['id'] ?>"><?php echo $supervisor['name'] . ' - ' . $supervisor['location'] ?></option>

                                                        <?php
                                                    }
                                                } ?>

                                            </select>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <div class="col-md-7 border pt-2">
                                        <div class="box box-primary " id="supervisor_details" style="display: none;">
                                            <div class="box-header with-border">
                                                <h3 class="box-title" id="sup-name">Recently Added Products</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body no-padding">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <ul class="nav nav-pills nav-stacked">
                                                            <li><a href="#">Mobile : <span id="sup-mobile"></span></a></li>
                                                            <li><a href="#">Location : <span id="sup-location"></span></a></li>
                                                            <li><a href="#">Status : <span id="sup-status"></span></a></li>
                                                            <li><a href="#">Joined Date : <span id="sup-joined_date"></span></a></li>

                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <b>Category</b>
                                                        <ul class="nav nav-pills nav-stacked" id="cat_div">


                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix">
                                <button type="submit" class="pull-right btn btn-success btn-sm" id="assign_btn" disabled> Assign Supervisor
                                    <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>
                    </form>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>

    <?php $this->view('backend/includes/footer') ?>

