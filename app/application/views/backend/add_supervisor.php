<?php $this->view('backend/includes/header') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->view('backend/includes/top_nav') ?>
    <?php $this->view('backend/includes/nav') ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <?php $this->view('backend/includes/breadcrumb') ?>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    if ($this->session->flashdata('msg') != '') {
                        ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('msg') ?>
                        </div>
                    <?php } ?>
                    <form action="<?php echo base_url() ?>admin/supervisors/save" method="post">
                        <div class="box">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <lable>Name</lable>
                                            <input type="text" name="name" class="form-control" placeholder="Name"
                                                   required>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                    <div class="col-md-3 border pt-2">
                                        <div class="form-group">
                                            <label for="">Categories</label><br>
                                            <select type="text" class="form-control multiselect multiselect-icon"
                                                    multiple="multiple" role="multiselect" name="categories[]"
                                                    id="alternative_service" required>
                                                <?php
                                                if (count($categories)) {
                                                    foreach ($categories as $category) { ?>
                                                        <option value="<?php echo $category['id'] ?>"
                                                                data-icon="<?php echo base_url() ?>assets/icons/<?php echo $category['icon'] ?>"
                                                                label="<?php echo $category['name'] ?>"><?php echo $category['name'] ?></option>
                                                    <?php }
                                                } ?>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Location</label>
                                            <select class="form-control select2select" name="location" required>
                                                <option value="" selected disabled>Select Location</option>
                                                <?php
                                                if (count($cities)) {
                                                    foreach ($cities as $city) { ?>
                                                        <option value="<?php echo $city['id'] ?>"
                                                                label="<?php echo $city['name'] ?>" <?php echo ($service_provider[0]['city_id'] == $city['id']) ? 'selected' : '' ?>><?php echo $city['name'] ?></option>
                                                    <?php }
                                                } ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <lable>Mobile</lable>
                                            <input type="number" class="form-control" name="mobile"
                                                   placeholder="9471xxxxxx" required>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix">
                                <button type="submit" class="pull-right btn btn-success btn-sm" id="sendEmail">Save
                                    <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>
                    </form>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Location</th>
                                    <th>Mobile</th>
                                    <th>Status</th>
                                    <!--                                    <th>Action</th>-->
                                </tr>
                                </thead>
                                <tboady>
                                    <?php
                                    $x = 1;
                                    foreach ($supervisors as $supervisor) { ?>
                                        <tr>
                                            <td><?php echo $x; ?></td>
                                            <td><?php echo $supervisor['name'] ?> </td>
                                            <td><?php echo $supervisor['location'] ?> </td>
                                            <td><?php echo $supervisor['mobile'] ?> </td>
                                            <td><?php echo ($supervisor['status']) ? 'Active' : 'Inactivated' ?> </td>
                                        </tr>
                                        <?php $x++;
                                    } ?>
                                </tboady>
                            </table>

                        </div>
                    </div>

                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <?php $this->view('backend/includes/footer') ?>

