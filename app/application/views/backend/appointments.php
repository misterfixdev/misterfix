<?php $this->view('backend/includes/header') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php $this->view('backend/includes/top_nav') ?>
    <?php $this->view('backend/includes/nav') ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <?php $this->view('backend/includes/breadcrumb') ?>
        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">

                        <div class="box-body">
                            <div class="mb-2 text-right">
                                <a href="<?php echo base_url() ?>admin/appointments" class="btn btn-sm btn-info">Show all appointment</a>
                                <a href="<?php echo base_url() ?>admin/appointments?follow=1" class="btn btn-sm btn-success">Show Followed Up</a>
                                <a href="<?php echo base_url() ?>admin/appointments?follow=0" class="btn btn-sm btn-warning">Show pending Followed Up</a>
                            </div>
                            <br>
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>#</th>
                                    <th>User Details</th>
                                    <th>Subject</th>
                                    <th>Date</th>
                                    <th>Time Slot</th>
                                    <th>Matter</th>
                                    <th>Followed up</th>
                                    <th>Action</th>
                                </tr>
                                <tboady>
                                    <?php if (count($apps)) {
                                        $x = 1;
                                        foreach ($apps as $item) { ?>
                                            <tr>
                                                <td><?php echo $x; ?></td>
                                                <td>
                                                    Name:
                                                    <b><?php echo $item['user_f_name'] ?><?php echo $item['user_l_name'] ?></b>
                                                    <br>
                                                    Mobile: <b><?php echo $item['phone'] ?></b>

                                                </td>
                                                <td><b><?php echo $item['subject'] ?></b></td>
                                                <td><b><?php echo $item['date'] ?></b></td>
                                                <td><b><?php echo $item['time_slot'] ?></b></td>
                                                <td><b><?php echo $item['matter'] ?></b></td>
                                                <td>
                                                    <?php echo ($item['is_followed_up']) ? '<i class="fa fa-check-circle text-success" title="Followed Up"></i> Followed up' : '<i class="fa fa-info-circle text-danger" title="Not Followed Up"></i> Not Followed Up' ?>
                                                </td>
                                                <td>
                                                    <?php if(!$item['is_followed_up']){ ?>
                                                        <form action="<?php echo base_url(); ?>admin/appointments/mark" method="post">
                                                            <input type="text" name="id" value="<?php echo $item['id']?>">
                                                        <button class="btn btn-sm btn-success"> Mark as Followed up</button>

                                                        </form>
                                                    <?php }?>
                                                </td>
                                            </tr>
                                            <?php
                                            $x++;
                                        }
                                    } ?>
                                </tboady>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <?php $this->view('backend/includes/footer') ?>

