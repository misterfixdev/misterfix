-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2020 at 11:52 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mrfix`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) NOT NULL DEFAULT 'system'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `code`, `icon`, `status`, `created_date`, `created_by`) VALUES
(1, 'Masons', 'Masons', '1', 1, '2020-02-19 11:35:14', 'system'),
(2, 'Tile', 'Tile', '2', 1, '2020-02-19 11:35:14', 'system'),
(3, 'Carpenters', 'Carpenters', '3', 1, '2020-02-19 11:35:15', 'system'),
(4, 'Plumbers', 'Plumbers', '4', 1, '2020-02-19 11:35:15', 'system'),
(5, 'Electricians', 'Electricians', '5', 1, '2020-02-19 11:35:15', 'system'),
(6, 'Painters', 'Painters', '6', 1, '2020-02-19 11:35:15', 'system'),
(7, 'Contractors', 'Contractors', '7', 1, '2020-02-19 11:35:16', 'system'),
(8, 'Concrete Slab', 'Concrete Slab', '8', 1, '2020-02-19 11:35:16', 'system'),
(9, 'Landscaping', 'Landscaping', '9', 1, '2020-02-19 11:35:16', 'system'),
(10, 'Ceiling', 'Ceiling', '10', 1, '2020-02-19 11:35:17', 'system'),
(11, 'Stones/ Sand/ Soil', 'Stones/ Sand/ Soil', '11', 1, '2020-02-19 11:35:17', 'system'),
(12, 'Aluminium', 'Aluminium', '12', 1, '2020-02-19 11:35:17', 'system'),
(13, 'Welding', 'Welding', '13', 1, '2020-02-19 11:35:17', 'system'),
(14, 'Gully Bowser', 'Gully Bowser', '14', 1, '2020-02-19 11:35:17', 'system'),
(15, 'Equipment Repairing', 'Equipment Repairing', '15', 1, '2020-02-19 11:35:17', 'system'),
(16, 'Professionals', 'Professionals', '16', 1, '2020-02-19 11:35:17', 'system'),
(17, 'A/C', 'A/C', '17', 1, '2020-02-19 11:35:18', 'system'),
(18, 'Well', 'Well', '18', 1, '2020-02-19 11:35:18', 'system'),
(19, 'Vehicle Repairs', 'Vehicle Repairs', '19', 1, '2020-02-19 11:35:18', 'system'),
(20, 'Cleaners', 'Cleaners', '20', 1, '2020-02-19 11:35:18', 'system'),
(21, 'CCTV', 'CCTV', '21', 1, '2020-02-19 11:35:18', 'system'),
(22, 'Cushion Works', 'Cushion Works', '22', 1, '2020-02-19 11:35:18', 'system'),
(23, 'Rent Tools', 'Rent Tools', '23', 1, '2020-02-19 11:35:18', 'system'),
(24, 'Chair Weavers', 'Chair Weavers', '24', 1, '2020-02-19 11:35:19', 'system'),
(25, 'Solar Panel Fixing', 'Solar Panel Fixing', '25', 1, '2020-02-19 11:35:19', 'system'),
(26, 'Curtains', 'Curtains', '26', 1, '2020-02-19 11:35:19', 'system'),
(27, 'House Demolishers', 'House Demolishers', '27', 1, '2020-02-19 11:35:19', 'system'),
(28, 'Movers', 'Movers', '28', 1, '2020-02-19 11:35:19', 'system'),
(29, 'Pest Control', 'Pest Control', '29', 1, '2020-02-19 11:35:19', 'system'),
(30, 'Repairs & Others', 'Repairs & Others', '30', 1, '2020-02-19 11:35:19', 'system');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'service_provider', 'Service Provider');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(100) NOT NULL,
  `main_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) NOT NULL DEFAULT 'system'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `code`, `main_id`, `status`, `created_date`, `created_by`) VALUES
(1, 'Ampara', 'ampa', NULL, 1, '2020-02-21 09:35:36', 'system'),
(2, 'Anuradhapura', 'anura', NULL, 1, '2020-02-21 09:35:36', 'system'),
(3, 'Badulla', 'badu', NULL, 1, '2020-02-21 09:35:36', 'system'),
(4, 'Batticaloa', 'batt', NULL, 1, '2020-02-21 09:35:36', 'system'),
(5, 'Colombo', 'Colo', NULL, 1, '2020-02-21 09:35:36', 'system'),
(6, 'Galle', 'Galle', NULL, 1, '2020-02-21 09:35:36', 'system'),
(7, 'Gampaha', 'Gamp', NULL, 1, '2020-02-21 09:35:37', 'system'),
(8, 'Hambantota', 'Hamb', NULL, 1, '2020-02-21 09:35:37', 'system'),
(9, 'Jaffna', 'jaff', NULL, 1, '2020-02-21 09:35:37', 'system'),
(10, 'Kalutara', 'kalu', NULL, 1, '2020-02-21 09:35:37', 'system'),
(11, 'Kandy', 'kand', NULL, 1, '2020-02-21 09:35:37', 'system'),
(12, 'Kegalle', 'kega', NULL, 1, '2020-02-21 09:35:37', 'system'),
(13, 'Kilinochchi', 'kili', NULL, 1, '2020-02-21 09:35:37', 'system'),
(14, 'Kurunegala', 'kuru', NULL, 1, '2020-02-21 09:35:37', 'system'),
(15, 'Mannar', 'mann', NULL, 1, '2020-02-21 09:35:37', 'system'),
(16, 'Matale', 'mtle', NULL, 1, '2020-02-21 09:35:37', 'system'),
(17, 'Matara', 'mata', NULL, 1, '2020-02-21 09:35:37', 'system'),
(18, 'Moneragala', 'mona', NULL, 1, '2020-02-21 09:35:37', 'system'),
(19, 'Mullaitivu', 'mull', NULL, 1, '2020-02-21 09:35:37', 'system'),
(20, 'Nuwara Eliya', 'nuwa', NULL, 1, '2020-02-21 09:35:37', 'system'),
(21, 'Polonnaruwa', 'polo', NULL, 1, '2020-02-21 09:35:37', 'system'),
(22, 'Puttalam', 'putt', NULL, 1, '2020-02-21 09:35:37', 'system'),
(23, 'Ratnapura', 'ratn', NULL, 1, '2020-02-21 09:35:37', 'system'),
(24, 'Trincomalee', 'trin', NULL, 1, '2020-02-21 09:35:37', 'system'),
(25, 'Vavuniya', 'vavu', NULL, 1, '2020-02-21 09:35:37', 'system'),
(26, 'Nugegoda', 'nuge', 5, 1, '2020-02-21 09:35:37', 'system'),
(27, 'Dehiwala', 'dehi', 5, 1, '2020-02-21 09:35:37', 'system'),
(28, 'Piliyandala', 'pili', 5, 1, '2020-02-21 09:35:38', 'system'),
(29, 'Maharagama', 'maha', 5, 1, '2020-02-21 09:35:38', 'system'),
(30, 'Angoda', 'ango', 5, 1, '2020-02-21 09:35:38', 'system'),
(31, 'Athurugiriya', 'athu', 5, 1, '2020-02-21 09:35:38', 'system'),
(32, 'Avissawella', 'avis', 5, 1, '2020-02-21 09:35:38', 'system'),
(33, 'Battaramulla', 'bath', 5, 1, '2020-02-21 09:35:38', 'system'),
(34, 'Boralesgamuwa', 'bora', 5, 1, '2020-02-21 09:35:38', 'system'),
(35, 'Colombo 1', 'col1', 5, 1, '2020-02-21 09:35:38', 'system'),
(36, 'Colombo 2', 'col2', 5, 1, '2020-02-21 09:35:38', 'system'),
(37, 'Colombo 3', 'col3', 5, 1, '2020-02-21 09:35:38', 'system'),
(38, 'Colombo 4', 'col4', 5, 1, '2020-02-21 09:35:38', 'system'),
(39, 'Colombo 5', 'col5', 5, 1, '2020-02-21 09:35:38', 'system'),
(40, 'Colombo 6', 'col6', 5, 1, '2020-02-21 09:35:38', 'system'),
(41, 'Colombo 7', 'col7', 5, 1, '2020-02-21 09:35:38', 'system'),
(42, 'Colombo 8', 'col8', 5, 1, '2020-02-21 09:35:38', 'system'),
(43, 'Colombo 9', 'col9', 5, 1, '2020-02-21 09:35:38', 'system'),
(44, 'Colombo 10', 'col10', 5, 1, '2020-02-21 09:35:38', 'system'),
(45, 'Colombo 11', 'col11', 5, 1, '2020-02-21 09:35:38', 'system'),
(46, 'Colombo 12', 'col12', 5, 1, '2020-02-21 09:35:38', 'system'),
(47, 'Colombo 13', 'col13', 5, 1, '2020-02-21 09:35:38', 'system'),
(48, 'Colombo 14', 'col14', 5, 1, '2020-02-21 09:35:38', 'system'),
(49, 'Colombo 15', 'col15', 5, 1, '2020-02-21 09:35:39', 'system'),
(50, 'Hanwella', 'han', 5, 1, '2020-02-21 09:35:39', 'system'),
(51, 'Homagama', 'homa', 5, 1, '2020-02-21 09:35:39', 'system'),
(52, 'Kaduwela', 'kadu', 5, 1, '2020-02-21 09:35:39', 'system'),
(53, 'Kesbewa', 'kesb', 5, 1, '2020-02-21 09:35:39', 'system'),
(54, 'Kohuwala', 'kohu', 5, 1, '2020-02-21 09:35:39', 'system'),
(55, 'Kolonnawa', 'kolo', 5, 1, '2020-02-21 09:35:39', 'system'),
(56, 'Kottawa', 'kotta', 5, 1, '2020-02-21 09:35:39', 'system'),
(57, 'Kotte', 'kotte', 5, 1, '2020-02-21 09:35:39', 'system'),
(58, 'Malabe', 'mala', 5, 1, '2020-02-21 09:35:39', 'system'),
(59, 'Moratuwa', 'mora', 5, 1, '2020-02-21 09:35:39', 'system'),
(60, 'Mount Lavinia', 'mtlav', 5, 1, '2020-02-21 09:35:39', 'system'),
(61, 'Nawala', 'naw', 5, 1, '2020-02-21 09:35:39', 'system'),
(62, 'Padukka', 'paduk', 5, 1, '2020-02-21 09:35:39', 'system'),
(63, 'Pannipitiya', 'panni', 5, 1, '2020-02-21 09:35:39', 'system'),
(64, 'Rajagiriya', 'raja', 5, 1, '2020-02-21 09:35:39', 'system'),
(65, 'Ratmalana', 'ratma', 5, 1, '2020-02-21 09:35:39', 'system'),
(66, 'Talawatugoda', 'talaw', 5, 1, '2020-02-21 09:35:39', 'system'),
(67, 'Wellampitiya', 'wella', 5, 1, '2020-02-21 09:35:39', 'system'),
(68, 'Kandy', 'kandy', 11, 1, '2020-02-21 09:35:39', 'system'),
(69, 'Katugastota', 'katug', 11, 1, '2020-02-21 09:35:39', 'system'),
(70, 'Gampola', 'gampa', 11, 1, '2020-02-21 09:35:39', 'system'),
(71, 'Kundasale', 'kund', 11, 1, '2020-02-21 09:35:39', 'system'),
(72, 'Peradeniya', 'pera', 11, 1, '2020-02-21 09:35:39', 'system'),
(73, 'Akurana', 'aku', 11, 1, '2020-02-21 09:35:39', 'system'),
(74, 'Ampitiya', 'ampi', 11, 1, '2020-02-21 09:35:40', 'system'),
(75, 'Digana', 'digan', 11, 1, '2020-02-21 09:35:40', 'system'),
(76, 'Galagedara', 'galag', 11, 1, '2020-02-21 09:35:40', 'system'),
(77, 'Gelioya', 'geli', 11, 1, '2020-02-21 09:35:40', 'system'),
(78, 'Kadugannawa', 'kadug', 11, 1, '2020-02-21 09:35:40', 'system'),
(79, 'Madawala Bazaar', 'madaw', 11, 1, '2020-02-21 09:35:40', 'system'),
(80, 'Nawalapitiya', 'nawa', 11, 1, '2020-02-21 09:35:40', 'system'),
(81, 'Pilimatalawa', 'pilim', 11, 1, '2020-02-21 09:35:40', 'system'),
(82, 'Wattegama', 'watt', 11, 1, '2020-02-21 09:35:40', 'system'),
(83, 'Galle', 'gall', 6, 1, '2020-02-21 09:35:40', 'system'),
(84, 'Ambalangoda', 'amba', 6, 1, '2020-02-21 09:35:40', 'system'),
(85, 'Elpitiya', 'elpi', 6, 1, '2020-02-21 09:35:40', 'system'),
(86, 'Hikkaduwa', 'hikk', 6, 1, '2020-02-21 09:35:40', 'system'),
(87, 'Baddegama', 'badd', 6, 1, '2020-02-21 09:35:40', 'system'),
(88, 'Ahangama', 'ahan', 6, 1, '2020-02-21 09:35:40', 'system'),
(89, 'Balapitiya', 'balap', 6, 1, '2020-02-21 09:35:41', 'system'),
(90, 'Benthota', 'bent', 6, 1, '2020-02-21 09:35:41', 'system'),
(91, 'Karapitiya', 'karap', 6, 1, '2020-02-21 09:35:41', 'system'),
(92, 'Ampara', 'ampar', 1, 1, '2020-02-21 09:35:41', 'system'),
(93, 'Akkarepattu', 'akkar', 1, 1, '2020-02-21 09:35:41', 'system'),
(94, 'Kalmunai', 'kalm', 1, 1, '2020-02-21 09:35:41', 'system'),
(95, 'Sainthamaruthu', 'sain', 1, 1, '2020-02-21 09:35:41', 'system'),
(96, 'Anuradhapura', 'anur', 2, 1, '2020-02-21 09:35:41', 'system'),
(97, 'Kekirawa', 'keki', 2, 1, '2020-02-21 09:35:41', 'system'),
(98, 'Tambuttegama', 'tamb', 2, 1, '2020-02-21 09:35:41', 'system'),
(99, 'Medawachchiya', 'medaw', 2, 1, '2020-02-21 09:35:41', 'system'),
(100, 'Eppawala', 'eppa', 2, 1, '2020-02-21 09:35:41', 'system'),
(101, 'Galenbindunuwewa', 'galen', 2, 1, '2020-02-21 09:35:41', 'system'),
(102, 'Galnewa', 'galn', 2, 1, '2020-02-21 09:35:41', 'system'),
(103, 'Habarana', 'haba', 2, 1, '2020-02-21 09:35:41', 'system'),
(104, 'Mihintale', 'mihi', 2, 1, '2020-02-21 09:35:41', 'system'),
(105, 'Nochchiyagama', 'noch', 2, 1, '2020-02-21 09:35:41', 'system'),
(106, 'Talawa', 'tala', 2, 1, '2020-02-21 09:35:41', 'system'),
(107, 'Badulla', 'badul', 3, 1, '2020-02-21 09:35:41', 'system'),
(108, 'Bandarawela', 'banda', 3, 1, '2020-02-21 09:35:41', 'system'),
(109, 'Welimada', 'weli', 3, 1, '2020-02-21 09:35:41', 'system'),
(110, 'Mahiyanganaya', 'mahi', 3, 1, '2020-02-21 09:35:41', 'system'),
(111, 'Hali Ela', 'hali', 3, 1, '2020-02-21 09:35:42', 'system'),
(112, 'Diyathalawa', 'diya', 3, 1, '2020-02-21 09:35:42', 'system'),
(113, 'Ella', 'ella', 3, 1, '2020-02-21 09:35:42', 'system'),
(114, 'Haputhale', 'hapu', 3, 1, '2020-02-21 09:35:42', 'system'),
(115, 'Passara', 'pass', 3, 1, '2020-02-21 09:35:42', 'system'),
(116, 'Batticaloa', 'bat', 4, 1, '2020-02-21 09:35:42', 'system'),
(117, 'Gampaha', 'gam', 7, 1, '2020-02-21 09:35:42', 'system'),
(118, 'Negombo', 'nego', 7, 1, '2020-02-21 09:35:42', 'system'),
(119, 'Kelaniya', 'kela', 7, 1, '2020-02-21 09:35:42', 'system'),
(120, 'Kadawatha', 'kadaw', 7, 1, '2020-02-21 09:35:42', 'system'),
(121, 'Ja-Ela', 'jaela', 7, 1, '2020-02-21 09:35:42', 'system'),
(122, 'Delgoda', 'delg', 7, 1, '2020-02-21 09:35:42', 'system'),
(123, 'Divulapitiya', 'divu', 7, 1, '2020-02-21 09:35:42', 'system'),
(124, 'Ganemulla', 'gane', 7, 1, '2020-02-21 09:35:42', 'system'),
(125, 'Kandana', 'kada', 7, 1, '2020-02-21 09:35:42', 'system'),
(126, 'Katunayake', 'katun', 7, 1, '2020-02-21 09:35:42', 'system'),
(127, 'Kiribathgoda', 'kirib', 7, 1, '2020-02-21 09:35:42', 'system'),
(128, 'Minuwangoda', 'minuw', 7, 1, '2020-02-21 09:35:42', 'system'),
(129, 'Mirigama', 'miri', 7, 1, '2020-02-21 09:35:42', 'system'),
(130, 'Nittambuwa', 'nitt', 7, 1, '2020-02-21 09:35:42', 'system'),
(131, 'Ragama', 'raga', 7, 1, '2020-02-21 09:35:43', 'system'),
(132, 'Veyangoda', 'veya', 7, 1, '2020-02-21 09:35:43', 'system'),
(133, 'Wattala', 'watta', 7, 1, '2020-02-21 09:35:43', 'system'),
(134, 'Tangalla', 'tang', 8, 1, '2020-02-21 09:35:43', 'system'),
(135, 'Beliatta', 'beli', 8, 1, '2020-02-21 09:35:43', 'system'),
(136, 'Tissamaharama', 'tiss', 8, 1, '2020-02-21 09:35:43', 'system'),
(137, 'Hambantota', 'hamba', 8, 1, '2020-02-21 09:35:43', 'system'),
(138, 'Ambalantota', 'amb', 8, 1, '2020-02-21 09:35:43', 'system'),
(139, 'Jaffna', 'jaffn', 9, 1, '2020-02-21 09:35:43', 'system'),
(140, 'Nallur', 'nall', 9, 1, '2020-02-21 09:35:43', 'system'),
(141, 'Chavakachcheri', 'chav', 9, 1, '2020-02-21 09:35:43', 'system'),
(142, 'Horana', 'hora', 10, 1, '2020-02-21 09:35:43', 'system'),
(143, 'Kalutara', 'kalut', 10, 1, '2020-02-21 09:35:43', 'system'),
(144, 'Panadura', 'pana', 10, 1, '2020-02-21 09:35:43', 'system'),
(145, 'Bandaragama', 'band', 10, 1, '2020-02-21 09:35:43', 'system'),
(146, 'Matugama', 'matu', 10, 1, '2020-02-21 09:35:43', 'system'),
(147, 'Aluthgama', 'alut', 10, 1, '2020-02-21 09:35:43', 'system'),
(148, 'Beruwala', 'beru', 10, 1, '2020-02-21 09:35:43', 'system'),
(149, 'Ingiriya', 'ingi', 10, 1, '2020-02-21 09:35:43', 'system'),
(150, 'Wadduwa', 'wadd', 10, 1, '2020-02-21 09:35:43', 'system'),
(151, 'Kegalle', 'kegal', 12, 1, '2020-02-21 09:35:43', 'system'),
(152, 'Mawanella', 'mawa', 12, 1, '2020-02-21 09:35:44', 'system'),
(153, 'Warakapola', 'wara', 12, 1, '2020-02-21 09:35:44', 'system'),
(154, 'Rambukkana', 'ramb', 12, 1, '2020-02-21 09:35:44', 'system'),
(155, 'Ruwanwella', 'ruwan', 12, 1, '2020-02-21 09:35:44', 'system'),
(156, 'Dehiowita', 'dehio', 12, 1, '2020-02-21 09:35:44', 'system'),
(157, 'Deraniyagala', 'dera', 12, 1, '2020-02-21 09:35:44', 'system'),
(158, 'Galigamuwa', 'galig', 12, 1, '2020-02-21 09:35:44', 'system'),
(159, 'Kithulgala', 'kitu', 12, 1, '2020-02-21 09:35:44', 'system'),
(160, 'Yatiyanthota', 'yatiy', 12, 1, '2020-02-21 09:35:44', 'system'),
(161, 'Kilinochchi', 'kilin', 13, 1, '2020-02-21 09:35:44', 'system'),
(162, 'Kurunegala', 'kurun', 14, 1, '2020-02-21 09:35:44', 'system'),
(163, 'Kuliyapitiya', 'kuli', 14, 1, '2020-02-21 09:35:44', 'system'),
(164, 'Pannala', 'pann', 14, 1, '2020-02-21 09:35:44', 'system'),
(165, 'Narammala', 'naram', 14, 1, '2020-02-21 09:35:44', 'system'),
(166, 'Wariyapola', 'wariy', 14, 1, '2020-02-21 09:35:44', 'system'),
(167, 'Alawwa', 'alaw', 14, 1, '2020-02-21 09:35:44', 'system'),
(168, 'Bingiriya', 'bing', 14, 1, '2020-02-21 09:35:44', 'system'),
(169, 'Galgamuwa', 'galg', 14, 1, '2020-02-21 09:35:44', 'system'),
(170, 'Giriulla', 'giriu', 14, 1, '2020-02-21 09:35:44', 'system'),
(171, 'Hettipola', 'hett', 14, 1, '2020-02-21 09:35:44', 'system'),
(172, 'Ibbagamuwa', 'ibba', 14, 1, '2020-02-21 09:35:44', 'system'),
(173, 'Mawathagama', 'mawat', 14, 1, '2020-02-21 09:35:44', 'system'),
(174, 'Nikaweratiya', 'nika', 14, 1, '2020-02-21 09:35:45', 'system'),
(175, 'Polgahawela', 'polg', 14, 1, '2020-02-21 09:35:45', 'system'),
(176, 'Mannar', 'manne', 15, 1, '2020-02-21 09:35:45', 'system'),
(177, 'Matale', 'matal', 16, 1, '2020-02-21 09:35:45', 'system'),
(178, 'Dambulla', 'damb', 16, 1, '2020-02-21 09:35:45', 'system'),
(179, 'Galewela', 'galew', 16, 1, '2020-02-21 09:35:45', 'system'),
(180, 'Ukuwela', 'ukuw', 16, 1, '2020-02-21 09:35:45', 'system'),
(181, 'Palapathwela', 'palap', 16, 1, '2020-02-21 09:35:45', 'system'),
(182, 'Rattota', 'ratt', 16, 1, '2020-02-21 09:35:45', 'system'),
(183, 'Sigiriya', 'sigir', 16, 1, '2020-02-21 09:35:45', 'system'),
(184, 'Yatawatta', 'yataw', 16, 1, '2020-02-21 09:35:45', 'system'),
(185, 'Matara', 'matar', 17, 1, '2020-02-21 09:35:45', 'system'),
(186, 'Weligama', 'welig', 17, 1, '2020-02-21 09:35:45', 'system'),
(187, 'Akuressa', 'akur', 17, 1, '2020-02-21 09:35:45', 'system'),
(188, 'Hakmana', 'hakm', 17, 1, '2020-02-21 09:35:45', 'system'),
(189, 'Dikwella', 'dikw', 17, 1, '2020-02-21 09:35:45', 'system'),
(190, 'Deniyaya', 'deniy', 17, 1, '2020-02-21 09:35:45', 'system'),
(191, 'Kamburugamuwa', 'kamb', 18, 1, '2020-02-21 09:35:45', 'system'),
(192, 'Kamburupitya', 'kambu', 18, 1, '2020-02-21 09:35:46', 'system'),
(193, 'Kekanadurra', 'kekan', 18, 1, '2020-02-21 09:35:46', 'system'),
(194, 'Moneragala', 'mone', 18, 1, '2020-02-21 09:35:46', 'system'),
(195, 'Wellawaya', 'well', 18, 1, '2020-02-21 09:35:46', 'system'),
(196, 'Bibile', 'bibi', 18, 1, '2020-02-21 09:35:46', 'system'),
(197, 'Buttala', 'butt', 18, 1, '2020-02-21 09:35:46', 'system'),
(198, 'Kataragama', 'katar', 18, 1, '2020-02-21 09:35:46', 'system'),
(199, 'Mullativu', 'mulla', 19, 1, '2020-02-21 09:35:46', 'system'),
(200, 'Nuwara Eliya', 'nuwel', 20, 1, '2020-02-21 09:35:46', 'system'),
(201, 'Hatton', 'hatt', 20, 1, '2020-02-21 09:35:46', 'system'),
(202, 'Ginigathena', 'gini', 20, 1, '2020-02-21 09:35:46', 'system'),
(203, 'Madulla', 'madul', 20, 1, '2020-02-21 09:35:46', 'system'),
(204, 'Polonnaruwa', 'polon', 21, 1, '2020-02-21 09:35:46', 'system'),
(205, 'Hingurakgoda', 'hing', 21, 1, '2020-02-21 09:35:47', 'system'),
(206, 'Kaduruwela', 'kaduw', 21, 1, '2020-02-21 09:35:47', 'system'),
(207, 'Medirigiriya', 'medir', 21, 1, '2020-02-21 09:35:47', 'system'),
(208, 'Chilaw', 'chil', 22, 1, '2020-02-21 09:35:47', 'system'),
(209, 'Wennappuwa', 'wenn', 22, 1, '2020-02-21 09:35:47', 'system'),
(210, 'Puttalam', 'putta', 22, 1, '2020-02-21 09:35:47', 'system'),
(211, 'Dankotuwa', 'dank', 22, 1, '2020-02-21 09:35:47', 'system'),
(212, 'Nattandiya', 'natt', 22, 1, '2020-02-21 09:35:47', 'system'),
(213, 'Marawila', 'mara', 22, 1, '2020-02-21 09:35:47', 'system'),
(214, 'Ratnapura', 'rathn', 23, 1, '2020-02-21 09:35:47', 'system'),
(215, 'Embilipitiya', 'embi', 23, 1, '2020-02-21 09:35:47', 'system'),
(216, 'Balangoda', 'balan', 23, 1, '2020-02-21 09:35:47', 'system'),
(217, 'Pelmadulla', 'pelm', 23, 1, '2020-02-21 09:35:47', 'system'),
(218, 'Eheliyagoda', 'ehel', 23, 1, '2020-02-21 09:35:47', 'system'),
(219, 'Kuruwita', 'kuruw', 23, 1, '2020-02-21 09:35:47', 'system'),
(220, 'Trincomalee', 'trinc', 24, 1, '2020-02-21 09:35:47', 'system'),
(221, 'Kinniya', 'kinn', 24, 1, '2020-02-21 09:35:47', 'system'),
(222, 'Vavuniya', 'vavun', 25, 1, '2020-02-21 09:35:47', 'system'),
(223, '', '', 0, 1, '2020-02-21 09:35:48', 'system'),
(224, '', '', 0, 1, '2020-02-21 09:35:48', 'system');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email_verified` int(11) NOT NULL DEFAULT '0',
  `phone_verified` int(11) NOT NULL DEFAULT '0',
  `user_type` varchar(100) NOT NULL DEFAULT 'member'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `email_verified`, `phone_verified`, `user_type`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$dzC6O7UJAI4zMoS3mC2JeuQ9gLXMKU2IGZ8CrZDgo1PkicCUitszW', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1582266919, 1, 'Admin', 'istrator', 'ADMIN', '0', 0, 0, 'administrator'),
(9, '::1', 'ww@ww.cc', '$2y$10$HZqvDWcSnlnGN5eBcjfCWOJ5/3uq6l4AWiIEOVb8.kxpos.dI7LD2', 'ww@ww.cc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1582008575, NULL, 1, 'www', 'www', 'www', '11111111111', 0, 0, 'member');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(14, 1, 3),
(12, 9, 2),
(11, 9, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
